package CaptureScreenLib;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

public class CaptureScreen 
{
	
	public static void CaptureScreenShotVedio(WebDriver driver,String screenshotName) {
		
		
		try {
			TakesScreenshot ts=(TakesScreenshot)driver;
			
  File source =     ts.getScreenshotAs(OutputType.FILE);
			
			FileUtils.copyDirectory(source, new File("./Screenshot_vedios/"+screenshotName+".png"));
			
			System.out.println("Screenshot taken for vedio ");
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

}
