package dashboard;

import static org.junit.Assert.*;

import org.ini4j.Ini;
import org.junit.After;
import org.junit.Test;

import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.read.biff.BiffException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.sl.usermodel.Background;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;

import java.io.*;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Scanner;//added 11/03/2013 by @Shahid 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.lang.System;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart; 

public class Framework {


	//*****************Run Manager Path****************************************************
	static String RunManagerPath="C:/Users/ABC/eclipse-workspace/LaunchAnra/src/dashboard/";
	static String EnvSelected="";
	//*************************************************************************************	
	static BufferedWriter bw = null;
	static BufferedWriter bw1 = null;
	static int reportFlag = 0;
	static WebDriver D8;
	static String htmlname;
	static String objType;
	static String objName;
	static String TestData;
	static String rootPath = "";
	static int report;
	static Date cur_dt = null;
	static String filenamer;
	static String dataIdentifier;
	static String TestSuite;
	static String TestScript;
	static String scriptPath;
	static String ObjectRepository;
	String ReportsPath;
	String strResultPath;
	String[] cCellDataVal = null;
	String[] dCellDataVal = null;
	static String TestReport;
	int rowcnt;
	static String exeStatus = "True";
	static int dtrownum;
	int ORrowcount = 0;
	static String ORvalname = "";
	String ORvalue = "";
	static String Action = "";
	static String cCellData = "";
	static String dCellData = "";
	static Sheet DTsheet = null;
	static Sheet sheet2 = null;
	Sheet ORsheet;
	String Searchtext;
	static String ObjectSet;
	String ObjectSetVal = "";
	static int iflag = 0;
	static int j = 0;
	boolean captureperform = false;
	boolean capturecheckvalue = false;
	boolean capturestorevalue = false;
	static Sheet TScsheet;
	static Workbook TScworkbook;
	static int TScrowcount = 0;
	Workbook w2 = null;
	static int loopnum = 1;
	static String TScname;
	String dateval = null;
	String ActionVal;
	static int DTcolumncount = 0;
	static WebElement elem = null;
	static WebElement elem1= null;
	static String result;
	static int count;
	static String IEWebDriverExePath="";
	static String BaseFolder="";
	static String LoginData="";
	static Actions action; // = new Actions(D8); 
	


	private static Map<String, String> map = new HashMap<String, String>();

	/*
	 * This function reads the selenium utility file and identifies where Object
	 * Repository, Test Suite & Test Scripts are located
	 */
	@Test
	public void ReadUtilFile() throws Exception {

		String iniSettingFile=RunManagerPath+"/Settings.ini";	

		BaseFolder= "";
		EnvSelected=GetINIOptionValue(iniSettingFile, "Environment", "env");
		TestSuite = RunManagerPath+"/TestSuite.xls";


		rootPath="C:/Users/domestic/Desktop/path/path/";		

		PrintWriter pw = null;
		try {
			pw = new PrintWriter(new FileWriter(
					rootPath+"/test.html"));
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		//pw.println("<TABLE BORDER><TR><TH>Execute<TH>Keyword<TH>Object<TH>Action<Result></TR>");
		Workbook w1 = null;
		try {
			w1 = Workbook
					.getWorkbook(new File(rootPath+"/Selenium_Utility.xls"));
			
		} catch (BiffException e) { // TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) { // TODO Auto-generated catch block
			e.printStackTrace();
		}
		Sheet sheet = w1.getSheet(0);

		TestScript = sheet.getCell(1, 2).getContents();
		ObjectRepository = sheet.getCell(1, 3).getContents();
		ReportsPath = sheet.getCell(1, 4).getContents();
		TestReport = sheet.getCell(1, 5).getContents();
		TestData = sheet.getCell(1, 6).getContents();
		LoginData = sheet.getCell(1, 7).getContents();

		FindExecTestscript(TestSuite, TestScript, ObjectRepository);

	}

	public void FindExecTestscript(String TestSuite, String TestScript,String ObjectRepository) throws Exception {

		FileInputStream fs = null;
		WorkbookSettings ws = null;
		fs = new FileInputStream(new File(TestSuite));
		ws = new WorkbookSettings();
		ws.setLocale(new Locale("en", "EN"));
		Workbook TSworkbook = Workbook.getWorkbook(fs, ws);
		Sheet TSsheet = TSworkbook.getSheet(0);
		TSsheet.getRows();

		//Global Datasheet

		//		w2 = Workbook.getWorkbook(new File(TestData));
		//		sheet2 = w2.getSheet("RunTimeGTD");
		//		DTcolumncount = sheet2.getColumns();

		for (int i = 1; i < TSsheet.getRows(); i++) {
			//dtrownum = 1;
			String TSvalidate = "r";
			if (((TSsheet.getCell(0, i).getContents())
					.equalsIgnoreCase(TSvalidate) == true)) {
				// String TCStatus = "Pass";
				String ScriptName = TSsheet.getCell(1, i).getContents();
				//dataIdentifier = TSsheet.getCell(2, i).getContents();
				ExecKeywordScript(ScriptName, TestScript, ObjectRepository);
				count = i;
				//D8.quit();
				writeReport(count, 2);
			}else{
				count = i;
				report = 2;
				writeReport(count, 2);
				report = 0;
			}

			/*
			 * else { System.out.println(TSvalidate); }
			 */
		}
	}

	public void ExecKeywordScript(String scriptName, String TestScript,
			String ObjectRepository) throws Exception {
		report = 0;
		//System.setProperty("webdriver.ie.driver",IEWebDriverExePath);
		//$$$System.setProperty("webdriver.ie.driver","C:\\GuidewireAutomation\\Selenium\\IEWebDriver\\IEDriverServer.exe");


		DesiredCapabilities capability=DesiredCapabilities.internetExplorer();
		capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		capability.setCapability("useLegacyInternalServer",true);
		//D8 = new FirefoxDriver();
		D8=new FirefoxDriver();
		D8.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		D8.manage().window().maximize();
		//Actions action = new Actions(D8);
		// Report header
//		D8.switchTo().frame("CossScreenFrame");
//		System.out.println(D8.findElement(By.xpath("//*[@id='GridView1']/tbody/tr[2]/td[2]")).getText());
//		
//		List <WebElement> allRows1 = D8.findElement(By.xpath("//*[@id='GridView1']/tbody")).findElements(By.tagName("tr"));
//		
//		
//		for(int i=1; i<allRows1.size(); i++){
//			List <WebElement> allColumns1 = allRows1.get(i).findElements(By.tagName("td"));
//			
//			for(int j =0; j<allColumns1.size();j++){
//				
//				System.out.println("Row = " + i + ";  Column = "+ j + "  " + allColumns1.get(j).getText());
//				
//			}
//		}
		
		
		cur_dt = new Date(); 
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		String strTimeStamp = dateFormat.format(cur_dt);
		WebDriverWait wait1=new WebDriverWait(D8,20);


		if (ReportsPath == "") { // if results path is passed as null, by
			// default 'C:\' drive is used
			ReportsPath = "C:\\";
		}

		if (ReportsPath.endsWith("\\")) { // checks whether the path ends with
			// '/'
			ReportsPath = ReportsPath + "\\";
		}
		String[] TCNm = scriptName.split("\\.");
		strResultPath = ReportsPath + "Log" + "/" + EnvSelected + "/" +TCNm[0] + "/"; //@Updated by shahid 10/08/2013
		htmlname = ReportsPath + "Log" + "/" +  EnvSelected + "/" + TCNm[0] + "/" + scriptName + "_" + strTimeStamp //@Updated by shahid 10/08/2013
				+ ".html";
		File f = new File(strResultPath);
		f.mkdirs();
		bw = new BufferedWriter(new FileWriter(htmlname));
		bw.write("<HTML><BODY><TABLE BORDER=0 CELLPADDING=3 CELLSPACING=1 WIDTH=100%>");
		bw.write("<TABLE BORDER=0 BGCOLOR=BLACK CELLPADDING=3 CELLSPACING=1 WIDTH=100%>");
		bw.write("<TR><TD BGCOLOR=#66699 WIDTH=27%><FONT FACE=VERDANA COLOR=WHITE SIZE=2><B>Test Case Name:</B></FONT></TD><TD COLSPAN=6 BGCOLOR=#66699><FONT FACE=VERDANA COLOR=WHITE SIZE=2><B>"
				+ TCNm[0] + "</B></FONT></TD></TR>");
		bw.write("<TR><TD BGCOLOR=#66699 WIDTH=27%><FONT FACE=VERDANA COLOR=WHITE SIZE=2><B>Baltimore Automation Framework :</B></FONT></TD><TD COLSPAN=6 BGCOLOR=#66699><FONT FACE=VERDANA COLOR=WHITE SIZE=2><B>"
				+ "Environment : "  + "  " +  EnvSelected + "</B></FONT></TD></TR>");
		bw.write("<HTML><BODY><TABLE BORDER=1 CELLPADDING=3 CELLSPACING=1 WIDTH=100%>");
		bw.write("<TR COLS=7><TD BGCOLOR=#FFCC99 WIDTH=3%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>Row</B></FONT></TD>"
				+ "<TD BGCOLOR=#FFCC99 WIDTH=10%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>Keyword</B></FONT></TD>"
				+ "<TD BGCOLOR=#FFCC99 WIDTH=10%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>Object</B></FONT></TD>"
				+ "<TD BGCOLOR=#FFCC99 WIDTH=10%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>Action</B></FONT></TD>"
				+ "<TD BGCOLOR=#FFCC99 WIDTH=10%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>Execution Time</B></FONT></TD> "
				+ "<TD BGCOLOR=#FFCC99 WIDTH=10%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>Status </B></FONT></TD>"
				+ "<TD BGCOLOR=#FFCC99 WIDTH=47%><FONT FACE=VERDANA COLOR=BLACK SIZE=2><B>Action Comments</B></FONT></TD></TR>");
		scriptPath = TestScript + scriptName +".xls";
		TScname = scriptName;
		int loopcount = 0;
		int startrow = 0;
		FileInputStream fs1 = null;
		WorkbookSettings ws1 = null;
		fs1 = new FileInputStream(new File(scriptPath));
		ws1 = new WorkbookSettings();
		ws1.setLocale(new Locale("en", "EN"));
		Workbook TScworkbook = Workbook.getWorkbook(fs1, ws1);

		Sheet TScsheet = TScworkbook.getSheet(0);
		TScrowcount = TScsheet.getRows();


		String xcelpath = scriptPath;
		FileInputStream fs3 = null;
		WorkbookSettings ws3 = null;
		fs3 = new FileInputStream(new File(xcelpath));
		ws3 = new WorkbookSettings();
		ws3.setLocale(new Locale("en", "EN"));
		Workbook DTworkbook = Workbook.getWorkbook(fs3, ws3);
		// DTsheet = DTworkbook.getSheet(0);
		DTsheet = DTworkbook.getSheet(1);

		DTsheet.getRows();
		// *This is the Data Table Sheet
		rowcnt = 0;

		dtrownum=1;
		for (j = 1; j < TScrowcount; j++) {
			Thread.sleep(1000);

			rowcnt = rowcnt + 1;
			String TSvalidate = "r";
			if (((TScsheet.getCell(0, j).getContents())
					.equalsIgnoreCase(TSvalidate) == true)) {
				Action = TScsheet.getCell(1, j).getContents();
				cCellData = TScsheet.getCell(2, j).getContents();
				dCellData = TScsheet.getCell(3, j).getContents();
				String ORPath = ObjectRepository;
				FileInputStream fs2 = null;
				WorkbookSettings ws2 = null;
				fs2 = new FileInputStream(new File(ORPath));
				ws2 = new WorkbookSettings();
				ws2.setLocale(new Locale("en", "EN"));
				try {
					Workbook ORworkbook = Workbook.getWorkbook(fs2, ws2);
					ORsheet = ORworkbook.getSheet(0);
					ORrowcount = ORsheet.getRows();
					ActionVal = Action.toLowerCase();
					iflag = 0;

				} catch (Exception e) {
					System.out.println(e);
					fail("Excel file of test is not correct.");
				}
				try {
					switch (ActionVal) {
					
					case "validatetable":
						
						String dataTablePath =  cCellData;
						String sheetName = dCellData;
						String[][] recData = readSheet(dataTablePath, sheetName);
						
//						D8.switchTo().frame("CossScreenFrame");
						
						List <WebElement> allRows = D8.findElement(By.xpath("//*[@id='GridView1']/tbody")).findElements(By.tagName("tr"));
//						CossScreenFrame
					
						for(int i=1; i<allRows.size(); i++){
							List <WebElement> allColumns = allRows.get(i).findElements(By.tagName("td"));
							
							if(allColumns.get(1).getText().trim().equals(recData[i][0].trim())){
								System.out.println("Pass");
							}else{

								report = 1;
								result = "Expected value '" + recData[i][0].trim() + "' did not match with actual value '"+  allColumns.get(1).getText().trim() + "' ";
								System.out.println("Fail");
								Update_Report("failed");
							}
							
							
							System.out.println(allColumns.get(1).getText());
							System.out.println(recData[i][0]);	
							System.out.println();
							
						}
						
						if(report == 0){
							
							result = "Insurance product validation is Matching for "+ cCellData + " -- "+ dCellData+ ". ";
							Update_Report("Executed");
							
						}
							
						
						break;

					case "loop":
						loopnum = 1;
						loopcount = Integer.parseInt(cCellData);
						startrow = j;
						Update_Report("loop" + " Start of loop number"+ loopnum);
						//Update_Report("executed");
						break;
					case "endloop":
						dtrownum++;
						int loopnumber = loopnum;
						loopcount = loopcount - 1;
						loopnum = loopnum + 1;
						if (loopcount == 0) {
							System.out.println("Reached End of Loop");
						} else

						{
							j = startrow;
						}
						rowcnt = 1;
						//Update_Report("loop" + " End of loop number"+ loopnumber);
						break;


					case "context":

						String[] dCellDataVal1 = dCellData.split("::");
						if (dCellDataVal1[1].contains("frame")) {
							String splitforframe[] = dCellDataVal1[1]
									.split(";");
							D8.switchTo().frame(splitforframe[1]);
						} else {
							D8.switchTo().defaultContent();
						}

						break;							


					case "callfunction":
						String[] cCellDataVal2 = cCellData.split(";");
						String ObjectVal1 = cCellData.substring(cCellDataVal2[0].length() + 1,cCellData.length());
						//String[] ObjectVal2=ObjectVal1.split(";");
						for (int k = 1; k < ORrowcount; k++) {
							String ORName = ORsheet.getCell(0, k).getContents();

							if (((ORsheet.getCell(0, k).getContents())
									.equalsIgnoreCase(ObjectVal1) == true)) {
								String[] ORcellData = new String[3];
								ORcellData = (ORsheet.getCell(3, k)
										.getContents()).split("=");
								ORvalname = ORcellData[0];
								ORvalue = ORcellData[1];

								if (ORcellData.length == 2)
									ORvalue = ORcellData[1];
								else 
								{
									ORvalue = ORcellData[1]+"="+ORcellData[2];		                                
								}

								k = ORrowcount + 1;
							}
						} 
						Func_FindObj(ORvalname, ORvalue);
						String ActionVal = cCellData;
						String value2 = dCellData;
//						UserDefinedFunction.Func_FunctionCall(ActionVal, value2);
						break;


					case "launchapp":
						D8.get(cCellData);
						result = "Application Launched";					
						break;

					case "launchapp123": //shahid and alam 11/22/2013
						try {
							D8.get(cCellData);
							//D8.switchTo().frame("top_frame");
							Thread.sleep(5000);
							result = " Application Launch Successful ";
							Update_Report("executed");	
						}

						catch(Exception e)
						{
							Update_Report("failed", e);
						}

						break;


					case "verifysuspenseaccountreport"://@ Shahid 

						D8.switchTo().frame(D8.findElement(By.name("inetframe")));
						D8.switchTo().frame(D8.findElement(By.id("reportiframe")));

						//driver.findElements(By.tagName(""))
						D8.findElement(By.xpath("//input[@type='button' and @value='OK']")).click();
						Thread.sleep(5000);
						Thread.sleep(2000);
						System.out.println("$$$$$$$$$$"+D8.findElement(By.xpath("//*[@id='divArea0_4']/font/b")).getText());
						String sCapturedReportTitle=D8.findElement(By.xpath("//*[@id='divArea0_4']/font/b")).getText();

						if (sCapturedReportTitle.equalsIgnoreCase("Suspense Account Report"))
						{
							Framework.result = "Successfully verified Report Title - Suspense Account Report";
							Update_Report("executed");	
						}else
						{
							Framework.result = "Verification Report Title failed- Suspense Account Report";
							Update_Report("failed");	
						}
						//System.out.println("*****Payment  : "  + driver.findElement(By.xpath("//*[@id='divArea0_4']/font/b")).getText());
						//D8.findElement(By.xpath("//*[@id='toolbarTbl']/tbody/tr/td[17]/a/img")).click();


						D8.switchTo().defaultContent();

						D8.switchTo().frame("top_frame");
						D8.switchTo().frame(D8.findElement(By.name("inetframe")));
						Thread.sleep(2000);
						D8.switchTo().frame(D8.findElement(By.id("iframeId")));

						D8.findElement(By.xpath("//*[@id='export_exportButton']")).click();
						Thread.sleep(2000);
						for(String winHandle : D8.getWindowHandles()){
							D8.switchTo().window(winHandle);
						}
						Thread.sleep(2000);
						System.out.println(D8.findElement(By.xpath("//div[@data-canvas-width='240.0193004864657']")).getText());
						D8.close();

						break;

					case "wait":

						Thread.sleep(Long.parseLong(cCellData));
						break;

					case "launchandlogin1":

						String	URL;
						int columncount = 0;
						String Expectedvalue= cCellData;

						Workbook w2 = null;

						w2 = Workbook.getWorkbook(new File(TestData));
						Sheet sheet = w2.getSheet(0);

						DTcolumncount = sheet.getColumns();
						for (columncount = 0; columncount < DTcolumncount; columncount++) {
							if (Expectedvalue.equalsIgnoreCase(sheet.getCell(columncount, 0).getContents()) == true) {
								URL = sheet.getCell(columncount,dtrownum).getContents();
								D8.get(URL);

							}

						}
						String UN = sheet.getCell(4, 1).getContents();
						String PWD = sheet.getCell(5, 1).getContents();

						Thread.sleep(3000);

						w2 = Workbook.getWorkbook(new File(ObjectRepository));
						sheet = w2.getSheet(0);
						String userNameField = sheet.getCell(3, 1)
								.getContents();
						String passWordField = sheet.getCell(3, 2)
								.getContents();
						String loginButton = sheet.getCell(3, 3).getContents();
						D8.switchTo().frame("top_frame");
						Thread.sleep(500);
						D8.findElement(By.xpath(userNameField)).sendKeys(UN);
						Thread.sleep(500);
						D8.findElement(By.xpath(passWordField)).sendKeys(PWD);
						Thread.sleep(1000);
						D8.findElement(By.xpath(loginButton)).click();
						Thread.sleep(5000);
						result = "Launch and Login Successful";
						Update_Report("executed");


						String xcelpath01 = scriptPath;
						FileInputStream fs4 = null;
						WorkbookSettings ws4 = null;
						fs4 = new FileInputStream(new File(xcelpath01));
						ws4 = new WorkbookSettings();
						ws4.setLocale(new Locale("en", "EN"));
						Workbook DTworkbook01 = Workbook.getWorkbook(fs4, ws4);
						// DTsheet = DTworkbook.getSheet(0);
						DTsheet = DTworkbook01.getSheet(1);
						DTsheet.getRows();
						break;

					case "switchtoframe":
						String frameName = cCellData;
						D8.switchTo().frame(frameName);
						break;
					case "launchandlogin":

						String	URL1;
						int columncount1 = 0;
						String Expectedvalue1= cCellData;

						Workbook w21 = null;

						w21 = Workbook.getWorkbook(new File(LoginData));
						Sheet sheet1 = w21.getSheet(0);						

						Integer RowNum=GetXLIdentifierRowNum(LoginData, "Login", EnvSelected+"_"+cCellData);											

						URL1 = sheet1.getCell(1,RowNum).getContents();
						String UN1 = sheet1.getCell(2,RowNum).getContents();
						String PWD1 = sheet1.getCell(3,RowNum).getContents();

						D8.get(URL1);

						w21 = Workbook.getWorkbook(new File(ObjectRepository));
						sheet1 = w21.getSheet(0);
						String userNameField1="";
						String passWordField1 ="";
						String loginButton1="";

						if (D8.getPageSource().contains("iNet Logout"))


						{
							D8.findElement(By.xpath("//span[contains(text(),'iNet Logout')]")).click();
							Thread.sleep(2000);
							D8.get(URL1);
							Thread.sleep(2000);
						}


						if (cCellData.equalsIgnoreCase("PC")||cCellData.equalsIgnoreCase("BC")||cCellData.equalsIgnoreCase("CC"))
						{
							userNameField1 = sheet1.getCell(3, 1)
									.getContents();
							passWordField1 = sheet1.getCell(3, 2)
									.getContents();
							loginButton1 = sheet1.getCell(3, 3).getContents();
							D8.switchTo().frame("top_frame");
							Thread.sleep(2000);
						}else
						{

							//							passWordField  = "//*[@name='Password']";; 
							//							userNameField= "//*[@name='Username']"; 
							//							loginButton = "//*[@name='login']"; 

							userNameField1 = sheet1.getCell(3, 4)
									.getContents();
							passWordField1 = sheet1.getCell(3, 5)
									.getContents();
							loginButton1 = sheet1.getCell(3, 6).getContents();
						}


						D8.findElement(By.xpath(userNameField1)).sendKeys(UN1);
						Thread.sleep(500);
						D8.findElement(By.xpath(passWordField1)).sendKeys(PWD1);
						Thread.sleep(1000);
						D8.findElement(By.xpath(loginButton1)).click();
						Thread.sleep(5000);
						result = cCellData+"Application Launch and Login Successful ";
						Update_Report("executed");						

						String xcelpath011 = scriptPath;
						FileInputStream fs41 = null;
						WorkbookSettings ws41 = null;
						fs41 = new FileInputStream(new File(xcelpath011));
						ws41 = new WorkbookSettings();
						ws41.setLocale(new Locale("en", "EN"));
						Workbook DTworkbook011 = Workbook.getWorkbook(fs41, ws41);
						// Get the Row number where data needs to be picked for re usable scripts
						Integer RowNumber ;
						if(!(dataIdentifier.equals("") ||dataIdentifier.equalsIgnoreCase("NA"))){
							dtrownum =GetXLIdentifierRowNum(xcelpath011, "TestData", dataIdentifier);
							if(dtrownum == 0){
								result = "Data Identifier '" + dataIdentifier + "' is not available in the Test Data sheet, Please check Identifier provided in TestSuite file and TestData sheet.";
								Update_Report("failed");
								//								D8.quit();
							}
						}
						//						}else{
						//							dtrownum = 1;
						//						}

						// DTsheet = DTworkbook.getSheet(0);
						DTsheet = DTworkbook011.getSheet(1);
						DTsheet.getRows();

						break;										

					case "writeruntimedata":

						String[] cIniValues = cCellData.split(";");

						String ObjectSetValIni= cIniValues[1];
						if (ObjectSetValIni.substring(0, 1).equalsIgnoreCase("#")) {
							ObjectSetValIni = map.get(ObjectSetValIni.substring(1,(ObjectSetValIni.length())));
						}
						if (ObjectSetValIni.contains("dt_")) 
						{
							String ObjectSetValtableColName[] = ObjectSetValIni.split("_");
							int column = 0;
							String Searchtext = ObjectSetValtableColName[1];
							for (column = 0; column < DTcolumncount; column++)
							{
								if (Searchtext.equalsIgnoreCase(DTsheet.getCell(column, 0).getContents()) == true)
								{
									ObjectSetValIni = DTsheet.getCell(column,dtrownum).getContents();
								}

							}							
						}

						WriteRunTimeINI(cIniValues[0],ObjectSetValIni);

						break;	

					case "globaldata":
						String expectedvalue= cCellData;
						String finalvalue=expectedvalue.substring(1,(expectedvalue.length()));
						String ObjectSetValCh= cCellData;
						if (ObjectSetValCh.substring(0, 1).equalsIgnoreCase("#")) {
							ObjectSetValCh = map.get(ObjectSetValCh.substring(1,(ObjectSetValCh.length())));
						}
						//	Workbook w3 = null;
						HSSFCell cell = null;

						File xlFile = new File(TestData);
						FileInputStream xlDoc = new FileInputStream(xlFile);
						HSSFWorkbook WB = new HSSFWorkbook(xlDoc);
						HSSFSheet Sheet = WB.getSheet("RunTimeGTD");
						java.util.Iterator<Row> rowIterator=Sheet.iterator();
						Row row=rowIterator.next();
						java.util.Iterator<Cell> cellIterator=row.cellIterator();
						while(cellIterator.hasNext()){
							Cell cellvalue=cellIterator.next();
							if (finalvalue.equalsIgnoreCase(cellvalue.getStringCellValue())){
								int colm=	cellvalue.getColumnIndex();
								cell = Sheet.getRow(1).getCell(colm);
								cell.setCellValue(ObjectSetValCh);
							}

						}

						FileOutputStream fout = new FileOutputStream(TestData);
						WB.write(fout);
						fout.flush();
						fout.close();
						break;	

					case "globaldatart"://added by Alam 01/10/2014
						String expectedvalue1= cCellData;
						String finalvalue1=expectedvalue1.substring(1,(expectedvalue1.length()));
						String ObjectSetValCh1= cCellData;
						if (ObjectSetValCh1.substring(0, 1).equalsIgnoreCase("#")) {
							ObjectSetValCh1 = map.get(ObjectSetValCh1.substring(1,(ObjectSetValCh1.length())));
						}
						//	Workbook w3 = null;
						HSSFCell cell1 = null;

						if (ObjectSetValCh1.contains("MEM")) {


							String[] ccellIniValues = ObjectSetValCh1.split("-");

							ObjectSetValCh1 = ccellIniValues[0];
							String[] ccellIniValues1 = ObjectSetValCh1.split(" ");
							ObjectSetValCh1 = ccellIniValues1[1];

						}


						File xlFile1 = new File(TestData);
						FileInputStream xlDoc1 = new FileInputStream(xlFile1);
						HSSFWorkbook WB1 = new HSSFWorkbook(xlDoc1);
						HSSFSheet Sheet1 = WB1.getSheet("RunTimeGTD");
						java.util.Iterator<Row> rowIterator1=Sheet1.iterator();
						Row row1=rowIterator1.next();
						java.util.Iterator<Cell> cellIterator1=row1.cellIterator();
						while(cellIterator1.hasNext()){
							Cell cellvalue=cellIterator1.next();
							if (finalvalue1.equalsIgnoreCase(cellvalue.getStringCellValue())){
								int colm=	cellvalue.getColumnIndex();
								//cell1 = Sheet1.getRow(1).getCell(colm);
								cell1 = Sheet1.getRow(loopnum).getCell(colm);
								cell1.setCellValue(ObjectSetValCh1);
							}

						}

						FileOutputStream fout1 = new FileOutputStream(TestData);
						WB1.write(fout1);
						fout1.flush();
						fout1.close();
						break;

					case "localdata":
						String expectedvalue11= cCellData;
						String finalvalue11=expectedvalue11.substring(1,(expectedvalue11.length()));
						String ObjectSetValCh11= cCellData;
						if (ObjectSetValCh11.substring(0, 1).equalsIgnoreCase("#")) {
							ObjectSetValCh11 = map.get(ObjectSetValCh11.substring(1,(ObjectSetValCh11.length())));
						}
						//	Workbook w3 = null;
						HSSFCell cell11 = null;


						if (ObjectSetValCh11.contains("MEM")) {


							String[] ccellIniValues = ObjectSetValCh11.split("-");

							ObjectSetValCh11 = ccellIniValues[0];
							String[] ccellIniValues1 = ObjectSetValCh11.split(" ");
							ObjectSetValCh11 = ccellIniValues1[1];

						}

						File xlFile11 = new File(scriptPath);
						FileInputStream xlDoc11 = new FileInputStream(xlFile11);
						HSSFWorkbook WB11 = new HSSFWorkbook(xlDoc11);
						HSSFSheet Sheet11 = WB11.getSheet("TestData");
						java.util.Iterator<Row> rowIterator11=Sheet11.iterator();
						Row row11=rowIterator11.next();
						java.util.Iterator<Cell> cellIterator11=row11.cellIterator();
						while(cellIterator11.hasNext()){
							Cell cellvalue=cellIterator11.next();
							if (finalvalue11.equalsIgnoreCase(cellvalue.getStringCellValue())){
								int colm=	cellvalue.getColumnIndex();
								cell11 = Sheet11.getRow(loopnum).getCell(colm);
								cell11.setCellValue(ObjectSetValCh11);

							}

						}
						FileOutputStream fout11 = new FileOutputStream(scriptPath);
						WB11.write(fout11);					
						fout11.flush();
						fout11.close();
						break;


					case "condition":
						String strConditionStatus = Func_IfCondition(cCellData);
						if (strConditionStatus.equalsIgnoreCase("false")) {
							// j = ifContidionSkipper(strConditionStatus);
							int temp = 0;
							temp = Integer.parseInt(dCellData);
							j = j + temp;
							System.out.println(j);
							//result = "Condition Failed";
							//Update_Report("failed");
						} else {
							//result = "Condition Failed";
							Update_Report("executed");
						}
						break;

					case "endcondition":
						result = "Condition Ended";
						Update_Report("executed");
						break;

					case "screencaptureoption":
						String[] sco = cCellData.split(";");

						for (int s = 0; s < sco.length; s++) {
							if (sco[s].equalsIgnoreCase("perform")) {
								captureperform = true;
							} else

								if (sco[s].equalsIgnoreCase("storevalue")) {
									capturestorevalue = true;
								}
							if (sco[s].equalsIgnoreCase("check")) {
								capturecheckvalue = true;
							}

						}
						Update_Report("executed");
						break;
					case "report":
						System.out.println(cCellData);
						screenshot(loopnum, TScrowcount, TScname);
						result = dCellData ;
						Update_Report(cCellData);
						break;
						/*case "importdata":

						  //Runtime rt = Runtime.getRuntime(); //Process p =
						  rt.exec("D://AutoITScript/FileDown.exe"); // String
						  xcelpath = cCellData; String xcelpath = scriptPath;
						  FileInputStream fs3 = null; WorkbookSettings ws3 =
						  null; 
						  fs3 = new FileInputStream(new File(xcelpath));
						  ws3 = new WorkbookSettings();
						  ws3.setLocale(new Locale("en", "EN"));
						  Workbook DTworkbook =Workbook.getWorkbook(fs3, ws3); // DTsheet =
						 DTworkbook.getSheet(0); DTsheet =
						  DTworkbook.getSheet(1); DTsheet.getRows(); // result
						  = "Data Imported"; // Update_Report("executed");


						break;*/

					case "savedialog":
						Runtime rt = Runtime.getRuntime();
						rt.exec("D://AutoITScript/FileDown.exe");
						break;

					case "screencapture":
						screenshot(loopnum, TScrowcount, TScname);
						Update_Report("executed");
						break;
					case "check":
						String[] cCellDataVal1 = cCellData.split(";");
						objType = cCellDataVal1[0];
						objName = cCellDataVal1[1];
						Func_StoreCheck();
						break;
					case "storevalue":
						Func_StoreCheck();
						break;

					case "pastdate":     	//@added by Shahid
						try{
							DateFormat PastFormat;															//return date in 11212013 format
							cur_dt = new Date();
							String PastDate = null;
							Calendar LastCalendar= Calendar.getInstance();
							String[]  pdateval = cCellData.split(":");
							String cCellDataValue01= pdateval[1];
							int objValue1 = Integer.parseInt(cCellDataValue01);
							int Lastyear = LastCalendar.get(Calendar.YEAR)-objValue1;								
							if (pdateval.length==2){
								PastFormat = new SimpleDateFormat("MMdd"+Lastyear);
							}else{																		//return date in 11/21/2013 format
								PastFormat = new SimpleDateFormat("MM/dd/yyyy");	
							}
							String PTimeStamp = PastFormat .format(cur_dt);
							map.put(pdateval[0],PTimeStamp.toString());

							WriteRunTimeINI("PastEffectiveDate",PTimeStamp);

							result = "'" + "PastEffectiveDate file with value" + "'  " + PTimeStamp
									+ "  has been created.";
							Update_Report("executed");
						}
						catch(Exception e)
						{
							Update_Report("failed", e);
						}
						break;

					case "currentdate":
						DateFormat Format;															//return date in 08212013 format
						cur_dt = new Date();
						String [] dateval=cCellData.split(":");
						if (dateval.length==2){
							Format = new SimpleDateFormat("MMddyyyy");
						}else{																		//return date in 08/21/2013 format
							Format = new SimpleDateFormat("MM/dd/yyyy");	
						}
						String TimeStamp = Format.format(cur_dt);
						map.put(dateval[0],TimeStamp.toString());
						break;


					case "futuredate":	
						//Adds 30 days to the current date
						DateFormat Futuredate;
						String[] futdateval=cCellData.split(":");								//@added by Damodar on 09.26.2013
						Calendar c= Calendar.getInstance();
						c.add(Calendar.DATE,30);
						Date d=c.getTime();
						if (futdateval.length==2){;
						Futuredate = new SimpleDateFormat("MMddyyyy");
						}else{																		
							Futuredate = new SimpleDateFormat("MM/dd/yyyy");	
						}
						String Fut_Timestamp=Futuredate.format(d);
						map.put(futdateval[0],Fut_Timestamp.toString());

						break;

					case "futuredate1":	
						//Adds 30 days to the current date
						//Adds 365 days to the current date//
						//Substract 300 days from the current date
						DateFormat Futuredate1;
						String[] futdateval1=cCellData.split(":");								//@added by Damodar on 09.26.2013
						Calendar c1= Calendar.getInstance();
						//c.add(Calendar.DATE,365);												//@added by Alam on 11.14.2013
						//c.add(Calendar.DATE,30);
						String cCellDataValue001= futdateval1[1];
						int objValue1 = Integer.parseInt(cCellDataValue001);
						c1.add(Calendar.YEAR,+objValue1);
						Date d1=c1.getTime();
						if (futdateval1.length==2){;
						Futuredate1 = new SimpleDateFormat("MMddyyyy");
						}else{																		
							Futuredate1 = new SimpleDateFormat("MM/dd/yyyy");	
						}
						String Fut_Timestamp1=Futuredate1.format(d1);
						map.put(futdateval1[0],Fut_Timestamp1.toString());

						WriteRunTimeINI("PreviousMonth", Fut_Timestamp1);

						break;

					case "backdate":	
						//Adds 30 days to the current date
						DateFormat Backdate;
						String[] backdateval=cCellData.split(":");								//@added by Damodar on 09.26.2013
						Calendar x= Calendar.getInstance();
						x.add(Calendar.DAY_OF_MONTH,-20);
						Date y=x.getTime();
						if (backdateval.length==2){;
						Backdate = new SimpleDateFormat("MMddyyyy");
						}else{																		
							Backdate = new SimpleDateFormat("MM/dd/yyyy");	
						}
						String Back_Timestamp=Backdate.format(y);

						map.put(backdateval[0],Back_Timestamp.toString());

						WriteRunTimeINI("Prevoius Date", Back_Timestamp);

						break;	



					case "handlealert": // Added by Chandra on 09.20.2013
						String alertvalue=cCellData;

						Alert altObj=D8.switchTo().alert();
						String altwindowtext=altObj.getText();
						result="Message" + "  "  +   altwindowtext + "     "+"is displayed";

						if (alertvalue.equalsIgnoreCase("accept")){
							altObj.accept();	
						}else{
							altObj.dismiss();
						}

						Update_Report("executed");
						break;

					case "alert":
						Alert alt=D8.switchTo().alert();
						String windowtext=alt.getText();
						result="Message" + "  "  +   windowtext + "     "+"is displayed";
						alt.accept();
						Update_Report("executed");
						break;

					case "cancel":
						Alert cancel=D8.switchTo().alert();
						String windowtext1=cancel.getText();
						result="Message" + "  "  +   windowtext1 + "     "+"is displayed & Cancel button is clicked";
						cancel.dismiss();
						Update_Report("executed");
						break;

					case "selectcolumn":

						String Query = dCellData;
						String Column = cCellData;		

						Connection Conn = null;

						Conn = DriverManager.getConnection("jdbc:oracle:thin:@" + Query +"_db:1755:"+Query,"query_dude","query");
						System.out.println("connected.."+Conn);

						Conn.createStatement();

						if (cCellData.contains("AccountNumber"))
						{
							String accountnumber ="select accountnumber from pcuser.pc_Account WHERE ROWNUM <= 1";
							Statement stmt = Conn.createStatement();
							ResultSet rs1 = stmt.executeQuery(accountnumber);

							while (rs1.next()) {

								String ResultColvalue2 = rs1.getString("accountnumber");
								//String ResultColvalue2 = rs1.getString("select" + schema2 + "from pcuser.pc_Account");


								System.out.println("*****Account Number::"+ResultColvalue2);

								WriteRunTimeINI(Column,ResultColvalue2);

							}

						}

						else

						{     	
							String PolicyNumber ="SELECT PolicyNumber FROM PCUSER.PC_POLICYPERIOD  WHERE ROWNUM <= 1";

							Statement stmt = Conn.createStatement();

							ResultSet rs1 = stmt.executeQuery(PolicyNumber);

							while (rs1.next()) {

								String ResultColvalue2 = rs1.getString("PolicyNumber");		        			            

								System.out.println("*****Policy Number::"+ResultColvalue2);

								WriteRunTimeINI(Column,ResultColvalue2);

							}	
						}

						//break;
						//@Shahid


					case "closeapp":
						D8.quit();
						//D8.close();
//						if(	reportFlag == 0){
//							writeSheet(TestSuite, "", irow, 2, "Pass");
//							
//						}
						result = "Application Closed";
						Update_Report("executed");
						break;
					case "strops":
						cCellDataVal = cCellData.split(";");
						dCellDataVal = null;
						dCellData.toString();
						if (dCellData.contains(":")) {
							dCellDataVal = dCellData.split(":");
							ObjectSet = dCellDataVal[0].toLowerCase();
							ObjectSetVal = dCellDataVal[1];
						} else {
							ObjectSet = dCellData.toString();
						}
						// System.out.println(ObjectSet);
						str_operations();
						break;

					case "arithops":
						cCellDataVal = cCellData.split(";");
						dCellDataVal = null;
						dCellData.toString();
						if (dCellData.contains(":")) {
							dCellDataVal = dCellData.split(":");
							ObjectSet = dCellDataVal[0].toLowerCase();
							ObjectSetVal = dCellDataVal[1];
						} else {
							ObjectSet = dCellData.toString();
						}
						// System.out.println(ObjectSet);
						arith_operations();
						break;

					case "switchreportframe": //@ Shahid 
						System.out.println("Switching to iframe");
						try{
							//String dateval01=cCellData;	
							String dateval01=cCellData;	
							//D8.switchTo().frame(D8.findElement(iframe));
							//D8.switchTo().frame(D8.findElement(By.name(dateval01)));
							Update_Report("executed");
						}
						catch(Exception e)
						{
							Update_Report("failed", e);
						}
						break;
					case "switchframeOld": //@ Shahid 
						System.out.println("Switching frame");
						try{
							String dateval01=cCellData;	

							D8.switchTo().frame(D8.findElement(By.name("dateval01")));


							D8.switchTo().frame(dateval01);
							Update_Report("executed");
						}
						catch(Exception e)
						{
							Update_Report("failed", e);
						}
						break;
					case "switchparentwindow": //@ Shahid 
						System.out.println("Switching to parent window");
						try{														
							D8.switchTo().defaultContent();
							Update_Report("executed");
						}
						catch(Exception e)
						{
							Update_Report("failed", e);
						}
						break;


					case "switchframe":

						if (ObjectSetVal.contains("dt_")) {
							String ObjectSetValtableheader[] = ObjectSetVal
									.split("_");
							int column = 0;
							String Searchtext = ObjectSetValtableheader[1];

							for (column = 0; column < DTcolumncount; column++) {
								if (Searchtext.equalsIgnoreCase(DTsheet
										.getCell(column, 0).getContents()) == true) {
									ObjectSetVal = DTsheet.getCell(column,
											dtrownum).getContents();
									iflag = 1;
								}
							}
							if (iflag == 0) {
								ORvalname = "exit";
							}
						}

						//@ Shahid 
						switch (ORvalname) {
						case "id":
							//							D8.switchTo().defaultContent();
							//							D8.switchTo().frame(D8.findElement(By.id("ORvalue")));
							D8.switchTo().frame(D8.findElement(By.id(ORvalue)));
							break;
						case "name":
							D8.switchTo().frame(D8.findElement(By.name(ORvalue)));
							break;
						case "xpath":
							D8.switchTo().frame(D8.findElement(By.xpath(ORvalue)));
							break;
						case "cssselector":
							D8.switchTo().frame(D8.findElement(By.cssSelector(ORvalue)));
							break; //@ Shahid 												

						}

						//						Func_FindObj(ORvalname, ORvalue);
						//						D8.switchTo().frame(elem);
						result = "Expected frame" + ObjectSetVal
								+ " has been Switched";
						Update_Report("executed");
						if (captureperform == true) {
							screenshot(loopnum, TScrowcount, TScname);
						}

						break;

					case "switchwindow":
						try{
							String parentWindowHandle = D8.getWindowHandle(); // save the current window handle.
							WebDriver popup = null;
							Set<String> al = new HashSet<String>();
							al = D8.getWindowHandles();
							java.util.Iterator<String> windowIterator = al.iterator();
							while (windowIterator.hasNext()) {
								String windowHandle = windowIterator.next();
								popup = D8.switchTo().window(windowHandle);
								if (popup.getTitle().equalsIgnoreCase(cCellData)) {
									//System.out.println(popup.getTitle() + " : " + cCellData);
									break;
								}
							}
							if(cCellData.equalsIgnoreCase("parent window"))
								D8.switchTo().window(parentWindowHandle);
							Update_Report("executed");
						}
						catch(Exception e)
						{
							Update_Report("failed", e);
						}
						break;

					case "randomruntimedata":

						String cCellDataValue011 = cCellData;
						String ObjectValue011 = cCellDataValue011;
						Integer Name01=10;


						if (cCellDataValue011.equalsIgnoreCase("EntityName"))
						{
							//String ObjectValue011 = cCellDataValue011[0];
							//WriteRunTimeINI(ObjectValue011,  NumberGen(ObjectValue011, Name01)); 
							NumberGen(ObjectValue011, 5);
							result = "'" + ObjectValue011 + "'  "  +"  has been created and stored as Runtime value. ";
							Update_Report("executed");
							break;

						}

						else if (cCellDataValue011.equalsIgnoreCase("FEINNumber"))

						{

							int Name02 = 9;
							NumberGen(ObjectValue011,  Name02);
							result = "'" +    Name02 +" digit" + ObjectValue011 + "'  " +"  has been created and stored as Runtime value. ";
							Update_Report("executed");
							break;

						}

						else if (cCellDataValue011.equalsIgnoreCase("PhoneNumber"))

						{

							int Name02 = 10;
							NumberGen(ObjectValue011, Name02);
							result = "'" +   Name02 +" digit" + ObjectValue011 + "'  " +"  has been created and stored as Runtime value. ";
							Update_Report("executed");
							break;

						}




					case "scannumber":  //@ 11/04/2013 Shahid 

						String[] cCellDataValue0 = cCellData.split(";");
						objType = cCellDataValue0[0];
						objName = cCellDataValue0[1];

						String ObjectValue0 = cCellData.substring(
								cCellDataValue0[0].length() + 1,
								cCellData.length());

						String[] dCellDataVa = dCellData.split(":");
						String ObjectSetValue0 = dCellDataVa[0];
						String ObjectSetValue01 = " ";
						DTcolumncount = 0;
						//System.out.println(DTsheet.getName());
						DTcolumncount = DTsheet.getColumns();

						if (dCellDataVa.length == 2) {

							ObjectSetValue01 = dCellDataVa[1];
						}

						for (int k = 1; k < ORrowcount; k++) {
							ORsheet.getCell(0, k).getContents();
							System.out.println(ORsheet.getCell(0, k)
									.getContents());
							if (((ORsheet.getCell(0, k).getContents())
									.equalsIgnoreCase(ObjectValue0) == true)) {
								String[] ORcellData = new String[3];
								ORcellData = (ORsheet.getCell(3, k)
										.getContents()).split("=");
								ORvalname = ORcellData[0];
								ORvalue = ORcellData[1];

								if (ORcellData.length == 2)
									ORvalue = ORcellData[1];
								else {
									ORvalue = ORcellData[1] + "="
											+ ORcellData[2];
								}

								k = ORrowcount + 1;
							}
						}

						Func_FindObj(ORvalname, ORvalue);						
						String stringvalue = elem.findElement(By.xpath(ORvalue)).getText();
						Scanner digitscanner = new Scanner(stringvalue);
						String [] ccellDataValue100 = stringvalue.split(" ");
						String Number = null;
						digitscanner.useDelimiter("[^\\p{Alnum},\\.-]");

						while  (stringvalue.isEmpty() != true)
						{

							for (int i = 0 ; i < ccellDataValue100.length; i++)
							{


								if(ScanDigit(ccellDataValue100[i]) == true)

								{
									System.out.println("contains digit " + ccellDataValue100[i] );
									Number = ccellDataValue100[i];


								}


							}

							WriteRunTimeINI(ObjectSetValue0, Number); 
							result = "'" + ObjectSetValue0 + "'  " +   Number +"  has been retrieved and stored as Runtime value. ";
							Update_Report("executed");

							break; //Exit loop 

						}

						break;	//End scannumber

						//@ Shahid 

						// @Abhi -- Added  Oct 8th 2013	

					case "sql":
						String[] cCellDataValue = cCellData.split(";");
						String[] dCellDataValue = dCellData.split(";");
						String columnName = cCellDataValue [0];
						String DB_ENV = dCellDataValue [1];

						int columnIndex = Integer.parseInt(cCellDataValue [1]);
						//String ObjectSetValue = dCellData;
						String ObjectSetValue = dCellDataValue [0];
						// DTsheet = DTworkbook.getSheet(0);
						Sheet DTsheetNew = DTworkbook.getSheet(1);

						int DTcolumncountNew = DTsheetNew.getColumns();
						if (ObjectSetValue.substring(0,1).equalsIgnoreCase("#")) 
						{
							ObjectSetValue = map.get(ObjectSetValue.substring(1,(ObjectSetValue.length())));
						}
						if (ObjectSetValue.contains("dt_")) 
						{
							String ObjectSetValuetableheader[] = ObjectSetValue
									.split("_");
							int column = 0;
							String Searchtext = ObjectSetValuetableheader[1].trim();
							for (column = 0; column < DTcolumncountNew; column++)
							{
								System.out.println(DTsheetNew.getCell(column, 0).getContents());
								if (Searchtext.equalsIgnoreCase(DTsheet
										.getCell(column, 0).getContents().trim()) == true)
								{
									ObjectSetValue = DTsheetNew.getCell(column,
											dtrownum).getContents();
									iflag = 1;
								}

							}
							if (iflag == 0) {
								ORvalname = "exit";
							}
						}

						try{
							sqlData(ObjectSetValue, columnName, columnIndex,DB_ENV);
							result = "SQL Query Executed with Column name : " + columnName + ", and selected Row number " +  columnIndex + " content.";
							Update_Report("executed");
						} catch(Exception e)
						{
							Update_Report("failed", e);
						}
						break;


					case "perform":
						String[] cCellDataVal = cCellData.split(";");
						objType = cCellDataVal[0];
						objName = cCellDataVal[1];

						String ObjectVal = cCellData.substring(
								cCellDataVal[0].length() + 1,
								cCellData.length());

						String[] dCellDataVal = dCellData.split(":");
						String ObjectSet = dCellDataVal[0];
						String ObjectSetVal = "";
						DTcolumncount = 0;
						//System.out.println(DTsheet.getName());
						DTcolumncount = DTsheet.getColumns();

						if (dCellDataVal.length == 2) {

							ObjectSetVal = dCellDataVal[1];
						}

						for (int k = 1; k < ORrowcount; k++) {
							ORsheet.getCell(0, k).getContents();
							System.out.println(ORsheet.getCell(0, k)
									.getContents());
							if (((ORsheet.getCell(0, k).getContents())
									.equalsIgnoreCase(ObjectVal) == true)) {
								String[] ORcellData = new String[3];
								ORcellData = (ORsheet.getCell(3, k)
										.getContents()).split("=");
								ORvalname = ORcellData[0];
								ORvalue = ORcellData[1];

								if (ORcellData.length == 2)
									ORvalue = ORcellData[1];
								else {
									ORvalue = ORcellData[1] + "="
											+ ORcellData[2];
								}

								k = ORrowcount + 1;
							}
						}

						switch (ObjectSet) {

						case "set":

							if (ObjectSetVal.substring(0,1).equalsIgnoreCase("#")) 
							{
								ObjectSetVal = map.get(ObjectSetVal.substring(1,(ObjectSetVal.length())));
							}
							if (ObjectSetVal.contains("dt_")) 
							{
								String ObjectSetValtableheader[] = ObjectSetVal
										.split("_");
								int column = 0;
								String Searchtext = ObjectSetValtableheader[1];
								for (column = 0; column < DTcolumncount; column++)
								{
									if (Searchtext.equalsIgnoreCase(DTsheet
											.getCell(column, 0).getContents()) == true)
									{
										ObjectSetVal = DTsheet.getCell(column,
												dtrownum).getContents();
										iflag = 1;
									}

								}
								if (iflag == 0) {
									ORvalname = "exit";
								}
							}

							if (ObjectSetVal.contains("rt_")) //@Chandra - Added on 09.19.2013
							{
								String ObjectSetValtableheader[] = ObjectSetVal
										.split("_");

								String SearchFile = ObjectSetValtableheader[1];

								ObjectSetVal=ReadRuntimeINI(SearchFile);								

								if (iflag == 0)
								{
									ORvalname = "exit";
								}
							}

							Func_FindObj(ORvalname, ORvalue);
							elem.clear();
							elem.sendKeys(ObjectSetVal);
							if(!(ObjectSetVal.equalsIgnoreCase("")))
							{
								result = "Input value " + ObjectSetVal
										+ " has been entered";
								Update_Report("executed");
							}
							if (captureperform == true) 
							{
								screenshot(loopnum, TScrowcount, TScname);
							}

							break;
						case "switchframe":

							if (ObjectSetVal.contains("dt_")) {
								String ObjectSetValtableheader[] = ObjectSetVal
										.split("_");
								int column = 0;
								String Searchtext = ObjectSetValtableheader[1];

								for (column = 0; column < DTcolumncount; column++) {
									if (Searchtext.equalsIgnoreCase(DTsheet
											.getCell(column, 0).getContents()) == true) {
										ObjectSetVal = DTsheet.getCell(column,
												dtrownum).getContents();
										iflag = 1;
									}
								}
								if (iflag == 0) {
									ORvalname = "exit";
								}
							}

							//@ Shahid 
							switch (ORvalname) {
							case "id":
								//							D8.switchTo().defaultContent();
								//							D8.switchTo().frame(D8.findElement(By.id("ORvalue")));
								D8.switchTo().frame(D8.findElement(By.id(ORvalue)));
								break;
							case "name":
								D8.switchTo().frame(D8.findElement(By.name(ORvalue)));
								break;
							case "xpath":
								D8.switchTo().frame(D8.findElement(By.xpath(ORvalue)));
								break;
							case "cssselector":
								D8.switchTo().frame(D8.findElement(By.cssSelector(ORvalue)));
								break; //@ Shahid 												

							}

							//						Func_FindObj(ORvalname, ORvalue);
							//						D8.switchTo().frame(elem);
							result = "Expected frame" + ObjectSetVal
									+ " has been Switched";
							Update_Report("executed");
							if (captureperform == true) {
								screenshot(loopnum, TScrowcount, TScname);
							}

							break;


						case "keys":
							Func_FindObj(ORvalname, ORvalue);
							if (ObjectSetVal.contains("dt_")) {
								String ObjectSetValtableheader[] = ObjectSetVal
										.split("_");
								int column = 0;
								String Searchtext = ObjectSetValtableheader[1];
								w21 = Workbook.getWorkbook(new File(TestData));
								Sheet sheet2 = w21.getSheet("RunTimeGTD");
								DTcolumncount = sheet2.getColumns();
								for (column = 0; column < DTcolumncount; column++) {
									if (Searchtext.equalsIgnoreCase(sheet2
											.getCell(column, 0).getContents()) == true) {
										ObjectSetVal = sheet2.getCell(column,
												dtrownum).getContents();
										iflag = 1;
									}

								}
								if (iflag == 0) {
									ORvalname = "exit";
								}
							}
							elem.clear();
							elem.sendKeys(ObjectSetVal); // send the key presses into the text box
							break;



						case "backspace":
							Func_FindObj(ORvalname, ORvalue);
							String dateval01=ObjectSetVal;
							Keys allKeys =Keys.BACK_SPACE;
							for(int k=0;k<Integer.parseInt(dateval01);k++){
								elem.sendKeys(allKeys);
							}
							break;


						case "select":
							if (ObjectSetVal.contains("dt_")) {
								String ObjectSetValtableheader[] = ObjectSetVal
										.split("_");
								int column = 0;
								String Searchtext = ObjectSetValtableheader[1];

								for (column = 0; column < DTcolumncount; column++) {
									if (Searchtext.equalsIgnoreCase(DTsheet
											.getCell(column, 0).getContents()) == true) {
										ObjectSetVal = DTsheet.getCell(column,
												dtrownum).getContents();
										iflag = 1;
									}

								}
								if (iflag == 0) {
									ORvalname = "exit";
								}
							}
							Func_FindObj(ORvalname, ORvalue);
							new Select(elem).selectByVisibleText(ObjectSetVal);
							result = "Expected value " + ObjectSetVal
									+ " has been Selected";
							Update_Report("executed");
							if (captureperform == true) {
								screenshot(loopnum, TScrowcount, TScname);
							}

							break;

						case "selectbyindex":

							Func_FindObj(ORvalname, ORvalue);

							Integer	indexval= Integer.parseInt(ObjectSetVal);

							new Select(elem).selectByIndex(indexval);
							result = "Expected value " + ObjectSetVal
									+ " has been Selected";
							Update_Report("executed");
							if (captureperform == true) {
								screenshot(loopnum, TScrowcount, TScname);
							}

							break;
							
						case "validatetable":
							
//							String dataTablePath =  cCellData;
//							String sheetName = cCellData;
//							String[][] recData = readSheet(dataTablePath, sheetName);
//							
//							
//							
//							List <WebElement> allRows = D8.findElement(By.xpath("//*[@id='GridView1']/tbody")).findElements(By.tagName("tr"));
//							
//							
//							for(int i=1; i<allRows.size(); i++){
//								List <WebElement> allColumns = allRows.get(i).findElements(By.tagName("td"));
//								
//								if(allColumns.get(1).getText().trim().equals(recData[i][0].trim())){
//									System.out.println("Pass");
//								}else
//									System.out.println("Fail");
//								
//								System.out.println(allColumns.get(1).getText());
//								System.out.println(recData[i][0]);	
//								System.out.println();
//								
//							}
											

						case "check":
							if (ObjectSetVal.contains("dt_")) {
								String ObjectSetValtableheader[] = ObjectSetVal
										.split("_");
								int column = 0;
								String Searchtext = ObjectSetValtableheader[1];
								for (column = 0; column < DTcolumncount; column++) {
									if (Searchtext.equalsIgnoreCase(DTsheet
											.getCell(column, 0).getContents()) == true) {
										ObjectSetVal = DTsheet.getCell(column,
												dtrownum).getContents();
										iflag = 1;
									}
								}
								if (iflag == 0) {
									ORvalname = "exit";
								}
							}
							Func_FindObj(ORvalname, ORvalue);
							if (elem.isSelected()
									&& dCellDataVal[1].equalsIgnoreCase("On")) {

							} else if (elem.isSelected()
									|| dCellDataVal[1].equalsIgnoreCase("On")) {
								elem.click();
							}
							Update_Report("executed");
							if (captureperform == true) {
								screenshot(loopnum, TScrowcount, TScname);
							}
							break;

						case "click":
							Func_FindObj(ORvalname, ORvalue);

							elem.click();
							//	elem1.click();
							result = "'" + objName + "'  " + objType
									+ "  has been clicked.";
							Update_Report("executed");
							break;

						case "mouseover":

							Actions action = new Actions(D8); 

							//D8.findElement(By.xpath("//*[@id='TabBar:DesktopTab']")).click();

							//D8.findElement(By.xpath("//*[@class='menu_arrow_menubutton']")).click();

							//action.moveToElement(D8.findElement(By.xpath("//*[@class='menu_arrow_menubutton']"))).build().perform();
							//action.moveToElement(D8.findElement(By.xpath("//*[@id='DesktopGroup:DesktopMenuActions:DesktopMenuActions_NewPayment']"))).build().perform();
							//							action.moveToElement(D8.findElement(By.xpath("//*[@class='menu_item_link']"))).build().perform();
							//							action.moveToElement(D8.findElement(By.xpath("//*[@id='DesktopGroup:DesktopMenuActions:DesktopMenuActions_NewPayment:DesktopMenuActions_MultiPaymentEntryWizard']"))).build().perform();


							Thread.sleep(2000);

							//					        D8.findElement(By.xpath("//*[@id='DesktopGroup:DesktopMenuActions:DesktopMenuActions_NewPayment:DesktopMenuActions_MultiPaymentEntryWizard']")).click();

							Func_FindObj(ORvalname, ORvalue);


							//							WebElement  elem3 = D8.findElement(By.xpath(objName));

							action.moveToElement(elem).build().perform();
							Thread.sleep(3000);


							//elem.click();
							//	elem1.click();
							result = "'" + objName + "'  " + objType
									+ "  has been switched.";
							Update_Report("executed");


							// for (WebElement we : hoverer) {
							//Action hovering = actions.moveToElement(we).build();
							//hovering.perform();
							// }

							//element.click();
							//}


							break;
						case "mouseoverclick": //@Shahid 
							action = new Actions(D8); 
							Thread.sleep(2000);
							Func_FindObj(ORvalname, ORvalue);
							action.moveToElement(elem).build().perform();
							Thread.sleep(3000);
							elem.click();

							result = "'" + objName + "'  " + objType
									+ "  has been switched.";
							Update_Report("executed");
							break;



						case "presstab":   
							Func_FindObj(ORvalname, ORvalue);

							String KeysTab=""+ Keys.TAB;
							elem.sendKeys(KeysTab);
							//elem.sendKeys(Keys.chord(Keys.CONTROL,"a"));

							//	elem1.click();
							result = "'" + objName + "'  " + objType
									+ "  has been clicked.";
							Update_Report("executed");
							break;
						case "pressenter":
							Func_FindObj(ORvalname, ORvalue); //@ Shahid 

							String KeysTab1=""+ Keys.ENTER;
							elem.sendKeys(KeysTab1);
							//elem.sendKeys(Keys.chord(Keys.CONTROL,"a"));

							//	elem1.click();
							result = "'" + objName + "'  " + objType
									+ "  has been clicked.";
							Update_Report("executed");
							break;
							//@ Shahid 
						case "cleartext":
							Func_FindObj(ORvalname, ORvalue);

							//String AllKeys=""+ Keys.SHIFT + Keys.HOME + Keys.DELETE;
							String AllKeys=""+ Keys.chord(Keys.CONTROL,"a") + Keys.DELETE;
							elem.sendKeys(AllKeys);
							//elem.sendKeys(Keys.chord(Keys.CONTROL,"a"));

							//	elem1.click();
							result = "'" + objName + "'  " + objType
									+ "  has been clicked.";
							Update_Report("executed");
							break; //@ Shahid 
						case "rightclick":

							Actions actionright=new Actions(D8);
							Func_FindObj(ORvalname, ORvalue);

							actionright.moveToElement(elem1).build().perform();
							for (int s = 0; s < Integer.parseInt(ObjectSetVal); s++) {
								actionright.contextClick(elem1)
								.sendKeys(Keys.ARROW_DOWN).build()
								.perform();
							}
							actionright.contextClick(elem1).click();
							actionright.contextClick(elem1).sendKeys(Keys.ENTER)
							.build().perform();

							result = "Right Click operation Performed";
							Update_Report("executed");
							if (captureperform == true) {
								screenshot(loopnum, TScrowcount, TScname);
							}
							break;
						}// End of Objectset Switch
						// Update_Report("executed");
					}// End of Actval Switch

				} catch (UnhandledAlertException e) {

					for (String id : D8.getWindowHandles()) {
						System.out.println("WindowHandle: "
								+ D8.switchTo().window(id));

					}
					System.out.println(e);
					System.out
					.println("Because of specification of SeleniumWebDriver, downloading may be failed.");
					System.out
					.println("Please confirm the report file and screenshot about test result.");
				} catch (Exception ex) {
					Update_Report("failed");
					System.out.println(ex);
					System.out
					.println("------Error Information : Test-------");
					System.out.println("Current Script:" + scriptName);
					System.out.println("Current ScriptPath:" + TestScript);
					System.out.println("Using ObjectRepositoryPath:"
							+ ObjectRepository);
					System.out.println("Current Keyword:" + Action);
					System.out.println("Current ObjectDetails:" + cCellData);
					System.out.println("Current ObjectDetailsPath:" + ORvalue);
					System.out.println("Current Action:" + dCellData);
					System.out.println("------Error Information : Test-------");
					//fail("Cannot test normally by Test.");

					j=TScrowcount+1;
				}

			}// End of Execution

		}// End of If that get all rows in Test Script
		bw.close();
	}// End of For that get all rows in Test Script

	private Object length(String objectSetVal2) {
		// TODO Auto-generated method stub
		return null;
	}

	public void readAttributeforPerform() throws Exception {
		try {
			if (ObjectSetVal.length() > 0) {
				if (ObjectSetVal.substring(0, 1).equalsIgnoreCase("#")) {
					ObjectSetVal = map.get(ObjectSetVal.substring(1,
							(ObjectSetVal.length())));
				} else if (ObjectSetVal.contains("dt_")) {
					String ObjectSetValtableheader[] = ObjectSetVal.split("_");
					int column = 0;
					String Searchtext = ObjectSetValtableheader[1];
					for (column = 0; column < DTcolumncount; column++) {
						if (Searchtext.equalsIgnoreCase(DTsheet.getCell(column,
								0).getContents()) == true) {
							ObjectSetVal = DTsheet.getCell(column, dtrownum)
									.getContents();
							iflag = 1;
						}

					}
					if (iflag == 0) {
						ORvalname = "exit";
					}
				}
			}
		} catch (Exception e) {
			Update_Report("failed", e);
		}

	}

	public void str_operations() throws Exception {
		readAttributeforPerform();
		String dummy = "";
		Boolean result;
		String[] dCellDataVal = dCellData.split(":");
		String varname = dCellDataVal[1];
		// dCellAction();
		switch (ObjectSet) {
		case "strconcat":
			cCellDataVal = cCellData.split(";");
			for (int cnt = 0; cnt < cCellDataVal.length; cnt++) {
				dummy = dummy.concat(readAttributeforStrops(cnt));
				// System.out.println(dummy);
			}
			map.put(varname, dummy);
			Update_Report("executed");
			break;
		case "strcompare":
			String str[] = cCellData.split(";");
			String value1 = str[0];
			String value2 = str[1];
			if (value1.substring(0, 1).equalsIgnoreCase("#")) {
				value1 = map.get(value1.substring(1, (value1.length())));
			}
			if (value2.substring(0, 1).equalsIgnoreCase("#")) {
				value2 = map.get(value2.substring(1, (value2.length())));
			}
			// cCellDataVal = cCellData.split(";");
			result = value1.equals(value2);
			map.put(varname, result.toString());

			Update_Report("executed");
			break;
		case "strsearch":
			cCellDataVal = cCellData.split(";");
			result = cCellDataVal[0].contains(cCellDataVal[1]);
			map.put(varname, result.toString());
			Update_Report("executed");
			break;
		case "strsearch1":
			cCellDataVal = cCellData.split(";");
			result = cCellDataVal[0].contains(cCellDataVal[1]);
			map.put(varname, result.toString());
			Update_Report("executed");
			break;
		}
	}

	public void arith_operations() throws Exception {
		readAttributeforPerform();
		float dummy = 0;
		String result = "";
		// Boolean result;
		String[] dCellDataVal = dCellData.split(":");
		String varname = dCellDataVal[1];
		float x = (float) readAttributeforArithops(0);
		float y = (float) readAttributeforArithops(1);
		// System.out.println("x : " + x + " -- Y : " + y);
		switch (ObjectSet) {
		case "add":
			// System.out.println(dummy);
			dummy = x + y;
			result = "" + dummy;
			map.put(varname, result);
			// System.out.println(map.get("con"));
			Update_Report("executed");
			break;
		case "sub":
			dummy = x - y;
			result = "" + dummy;
			map.put(varname, result);
			// System.out.println(map.get("con1"));
			Update_Report("executed");
			break;
		case "multiply":
			dummy = x * y;
			result = "" + dummy;
			map.put(varname, result);
			// System.out.println(map.get("con2"));
			Update_Report("executed");
			break;
		case "divide":
			dummy = x / y;
			result = "" + dummy;
			map.put(varname, result);
			// System.out.println(map.get("con3"));
			Update_Report("executed");
			break;
		case "mod":
			dummy = x % y;
			result = "" + dummy;
			map.put(varname, result);
			// System.out.println(map.get("con4"));
			Update_Report("executed");
			break;
		}
	}

	public String readAttributeforStrops(int cnt) throws Exception {
		String[] val = cCellData.split(";");
		try {
			// System.out.println(val[cnt].substring(1, (val[cnt].length())));
			if (val[cnt].substring(0, 1).equalsIgnoreCase("#")) {
				// System.out.println(map.get("var1"));
				return map.get(val[cnt].substring(1, (val[cnt].length())));
			} else if (val[cnt].contains("dt_")) {
				String ObjectSetValtableheader[] = val[cnt].split("_");
				int column = 0;
				String Searchtext = ObjectSetValtableheader[1];
				for (column = 0; column < DTcolumncount; column++) {
					if (Searchtext.equalsIgnoreCase(DTsheet.getCell(column, 0)
							.getContents()) == true) {
						return DTsheet.getCell(column, dtrownum).getContents();
						// iflag = 1;
					}

				}
			}

		} catch (Exception e) {
			Update_Report("failed", e);
		}
		System.out.println(val[cnt]);
		return val[cnt];

	}

	public int readAttributeforArithops(int cnt) throws Exception {
		String[] val = cCellData.split(";");
		// System.out.println(val.length);
		try {
			if (val[cnt].substring(0, 1).equalsIgnoreCase("#")) {
				return Integer.parseInt(map.get(val[cnt].substring(1,
						(val[cnt].length()))));
			} else if (val[cnt].contains("dt_")) {
				String ObjectSetValtableheader[] = val[cnt].split("_");
				int column = 0;
				String Searchtext = ObjectSetValtableheader[1];
				for (column = 0; column < DTcolumncount; column++) {
					if (Searchtext.equalsIgnoreCase(DTsheet.getCell(column, 0)
							.getContents()) == true) {
						return Integer.parseInt(DTsheet.getCell(column,
								dtrownum).getContents());
						// iflag = 1;
					}

				}
			}

		} catch (Exception e) {
			System.out.println(e);
			Update_Report("failed", e);
		}
		// System.out.println(val[cnt]);
		return Integer.parseInt(val[cnt]);

	}

	public static void screenshot(int loopn, int rown, String Sname) throws Exception {
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
			Date date = new Date();
			String strTime = dateFormat.format(date);
			File screenshot = ((TakesScreenshot) D8)
					.getScreenshotAs(OutputType.FILE);
			filenamer = TestReport + Sname + "_" + loopn + "_" + (j + 1) + "_"
					+ strTime + ".png";
			FileUtils.copyFile(screenshot, new File(filenamer));
		} catch (Exception e) {
			System.out.println(e);
			// System.out.println("Getting Screenshot is failed. Please confirm the test report whether the operation is executed or not.");
			// System.out.println("This message may be displayed when closing the dialog.");
		}
	}

	public static void Update_Report(String Res_type) throws Exception {
		String str_time;
		Date exec_time = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		str_time = dateFormat.format(exec_time);
		if (Res_type.startsWith("executed")) {
			bw.write("<TR COLS=7><TD BGCOLOR=#EEEEEE WIDTH=3%><FONT FACE=VERDANA SIZE=2>"
					+ (j + 1)
					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=10%><FONT FACE=VERDANA SIZE=2>"
					+ Action
					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=10%><FONT FACE=VERDANA SIZE=2>"
					+ cCellData
					+ "</FONT></TD></TD><TD BGCOLOR=#EEEEEE WIDTH=10%><FONT FACE=VERDANA SIZE=2>"
					+ dCellData
					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=10%><FONT FACE=VERDANA SIZE=2>"
					+ str_time
					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=10%><FONT FACE=VERDANA SIZE=2 COLOR = GREEN>"
					+ "Passed"
					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=30%><FONT FACE=VERDANA SIZE=2 COLOR = GREEN>"
					+ result + "</FONT></TD></TR>");
		} else if (Res_type.startsWith("failed")) {
			exeStatus = "Failed";
			screenshot(loopnum, TScrowcount, TScname);
			report = 1;
			bw.write("<TR COLS=7><TD BGCOLOR=#EEEEEE WIDTH=3%><FONT FACE=VERDANA SIZE=2>"
					+ (j + 1)
					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=10%><FONT FACE=VERDANA SIZE=2>"
					+ Action
					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=10%><FONT FACE=VERDANA SIZE=2>"
					+ cCellData
					+ "</FONT></TD></TD><TD BGCOLOR=#EEEEEE WIDTH=10%><FONT FACE=VERDANA SIZE=2>"
					+ dCellData
					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=10%><FONT FACE=VERDANA SIZE=2>"
					+ str_time
					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=10%><FONT FACE=VERDANA SIZE=2 COLOR = RED>"
					+ "<a href= "
					+ filenamer
					+ "  style=\"color: #FF0000\"> Failed </a>"

					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=30%><FONT FACE=VERDANA SIZE=2 COLOR = RED>"
					+ result + "</FONT></TD></TR>");
		} else if (Res_type.startsWith("loop")) {
			bw.write("<TR COLS=7><th colspan= 6 BGCOLOR=#EEEEEE WIDTH=5%><FONT FACE='WINGDINGS 2' SIZE=3 COLOR=BLUE><div align=left></FONT><FONT FACE=VERDANA SIZE=2 COLOR = BLUE>"
					+ Res_type + "</div></th></FONT></TR>");
		} else if (Res_type.startsWith("missing")) {
			exeStatus = "Failed";
			report = 1;
			bw.write("<TR COLS=6><TD BGCOLOR=#EEEEEE WIDTH=5%><FONT FACE=VERDANA SIZE=2>"
					+ (j + 1)
					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=17%><FONT FACE=VERDANA SIZE=2>"
					+ Action
					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=30%><FONT FACE=VERDANA SIZE=2>"
					+ cCellData
					+ "</FONT></TD></TD><TD BGCOLOR=#EEEEEE WIDTH=30%><FONT FACE=VERDANA SIZE=2>"
					+ dCellData
					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=30%><FONT FACE=VERDANA SIZE=2>"
					+ str_time
					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=30%><FONT FACE=VERDANA SIZE=2 COLOR = ORANGE>"
					+ "Failed" + "</FONT></TD></TR>");

			bw.write("<TR COLS=7><th colspan= 6 BGCOLOR=#EEEEEE WIDTH=5%><FONT FACE='WINGDINGS 2' SIZE=3 COLOR=RED><div align=left>X </FONT><FONT FACE=VERDANA SIZE=2 COLOR = RED>Error Occurred in Keyword test step number "
					+ (j + 1)
					+ ".Description: The Datatable column name not found</div></th></FONT></TR>");
		} else if (Res_type.startsWith("ObjectLocator")) {
			report = 1;
			bw.write("<TR COLS=6><TD BGCOLOR=#EEEEEE WIDTH=5%><FONT FACE=VERDANA SIZE=2>"
					+ (j + 1)
					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=17%><FONT FACE=VERDANA SIZE=2>"
					+ Action
					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=30%><FONT FACE=VERDANA SIZE=2>"
					+ cCellData
					+ "</FONT></TD></TD><TD BGCOLOR=#EEEEEE WIDTH=30%><FONT FACE=VERDANA SIZE=2>"
					+ dCellData
					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=30%><FONT FACE=VERDANA SIZE=2>"
					+ str_time
					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=30%><FONT FACE=VERDANA SIZE=2 COLOR = ORANGE>"
					+ "Failed" + "</FONT></TD></TR>");
			bw.write("<TR COLS=7><th colspan= 6 BGCOLOR=#EEEEEE WIDTH=5%><FONT FACE='WINGDINGS 2' SIZE=3 COLOR=RED><div align=left>X </FONT><FONT FACE=VERDANA SIZE=2 COLOR = RED>Error Occurred in Keyword test step number "
					+ (j + 1)
					+ ".Description: Object Locator is wrong or not supported. Supported Locators are Id,Name,Xpath& CSS</div></th></FONT></TR>");
		}
	}

	public static void Update_Report(String Res_type, Exception e)
			throws Exception {
		String str_time;
		Date exec_time = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		str_time = dateFormat.format(exec_time);
		exeStatus = "Failed";
		if (Res_type.startsWith("failed")) {
			report = 1;
			screenshot(loopnum, TScrowcount, TScname);
			bw.write("<TR COLS=7><TD BGCOLOR=#EEEEEE WIDTH=5%><FONT FACE=VERDANA SIZE=2>"
					+ (j + 1)
					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=17%><FONT FACE=VERDANA SIZE=2>"
					+ Action
					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=30%><FONT FACE=VERDANA SIZE=2>"
					+ cCellData
					+ "</FONT></TD></TD><TD BGCOLOR=#EEEEEE WIDTH=30%><FONT FACE=VERDANA SIZE=2>"
					+ dCellData
					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=30%><FONT FACE=VERDANA SIZE=2>"
					+ str_time
					+ "</FONT></TD><TD BGCOLOR=#EEEEEE WIDTH=30%><FONT FACE=VERDANA SIZE=2 COLOR = RED>"
					+ "Failed" + "</FONT></TD></TR>");
			bw.write("<TR COLS=6><th colspan= 6 BGCOLOR=#EEEEEE WIDTH=5%><FONT FACE='WINGDINGS 2' SIZE=3 COLOR=RED><div align=left></FONT><FONT FACE=VERDANA SIZE=2 COLOR = RED>"
					+ e.toString().substring(
							e.toString().indexOf(":") + 1,
							e.toString().indexOf(".",
									e.toString().indexOf(":") + 1) + 1)
									+ "</div></th></FONT></TR>");
		}
	}

	public static void writeReport(int iRow, int iColumn) throws IOException {

		File xlFile = new File(TestSuite);
		FileInputStream xlDoc = new FileInputStream(xlFile);
		HSSFWorkbook WB = new HSSFWorkbook(xlDoc);
		HSSFSheet Sheet = WB.getSheet("Test_Suite");

		CreationHelper createHelper = WB.getCreationHelper();
		CellStyle hlink_style = WB.createCellStyle();
		HSSFFont hlink_font = WB.createFont();
		hlink_font.setUnderline(Font.U_SINGLE);
		hlink_font.setColor(IndexedColors.BLUE.getIndex());
		hlink_style.setFont(hlink_font);

		HSSFCell cell;

		cell = Sheet.getRow(iRow).getCell(iColumn);
		if (report == 0){

			HSSFCellStyle cellStyle = WB.createCellStyle();
			HSSFFont font = WB.createFont();
			font.setBold(true);
			cellStyle.setFont(font);
			cell.setCellStyle(cellStyle);
			cell.setCellValue("PASS");
/*			Hyperlink link = createHelper.createHyperlink(Hyperlink.LINK_URL);
			String htmlname1 = "file:///" + htmlname;
			link.setAddress(htmlname1);
			cell.setHyperlink(link);*/
			cell.setCellStyle(hlink_style);
		}
		else if(report == 1){
			cell.setCellValue("FAIL");
			HSSFCellStyle cellStyle = WB.createCellStyle();
			HSSFFont font = WB.createFont();
			font.setBold(true);
			cellStyle.setFont(font);
			cell.setCellStyle(cellStyle);
/*			Hyperlink link = createHelper.createHyperlink(Hyperlink.LINK_URL);
			String htmlname1 = "file:///" + htmlname;
			link.setAddress(htmlname1);
			cell.setHyperlink(link);*/
			cell.setCellStyle(hlink_style);
		}

		else{
			cell.setCellValue("NA");
			HSSFCellStyle cellStyle = WB.createCellStyle();
			HSSFFont font = WB.createFont();
			font.setBold(true);
			cellStyle.setFont(font);
			cell.setCellStyle(cellStyle);

		}

		/*Hyperlink link = createHelper.createHyperlink(Hyperlink.LINK_URL);
		String htmlname1 = "file:///" + htmlname;
		link.setAddress(htmlname1);
		cell.setHyperlink(link);
		cell.setCellStyle(hlink_style);*/ //@Removed by Shahid


		// Access the row
		HSSFRow row = Sheet.getRow(iRow);
		// Access the column
		HSSFCell Cell = row.getCell(iColumn);
		// Set the String
		Cell.setCellType(HSSFCell.CELL_TYPE_STRING);
		// Set the Color
		HSSFCellStyle titleStyle = WB.createCellStyle();
		if (report == 0)
			titleStyle.setFillForegroundColor(new HSSFColor.BRIGHT_GREEN().getIndex());
		else if((report == 1))
			titleStyle.setFillForegroundColor(new HSSFColor.RED().getIndex());
		else
			titleStyle.setFillForegroundColor(new HSSFColor.WHITE().getIndex());
	
		Cell.setCellStyle(titleStyle);

		FileOutputStream fout = new FileOutputStream(TestSuite);
		WB.write(fout);
		fout.flush();
		fout.close();

	}

	private void Func_StoreCheck() throws Exception {
		String actval = null;
		String expval;
		Boolean boolval = null;
		String varname;
		String[] cCellDataValCh = cCellData.split(";");
		String ObjectValCh = cCellDataValCh[1];
		String[] dCellDataValCh = dCellData.split(":");
		String ObjectSetCh = dCellDataValCh[0];
		String ObjectSetValCh = "";
		DTsheet.getColumns();
		if (dCellDataValCh.length == 2) {
			ObjectSetValCh = dCellDataValCh[1];
		}


		for (int k = 1; k < ORrowcount; k++) {
			ORsheet.getCell(0, k).getContents();

			if (((ORsheet.getCell(0, k).getContents())
					.equalsIgnoreCase(ObjectValCh) == true)) {
				String[] ORcellData = new String[3];
				ORcellData = (ORsheet.getCell(3, k).getContents()).split("=");
				ORvalname = ORcellData[0];
				ORvalue = ORcellData[1];
				if (ORcellData.length == 2)
					ORvalue = ORcellData[1];
				else {
					ORvalue = ORcellData[1] + "=" + ORcellData[2];
				}
				k = ORrowcount + 1;
			}

		}


		if (ObjectSetValCh.substring(0, 1).equalsIgnoreCase("#"))
		{
			ObjectSetValCh = map.get(ObjectSetValCh.substring(1,(ObjectSetValCh.length())));

		} else if (ObjectSetValCh.contains("dt_"))
		{
			String ObjectSetValtableheader[] = ObjectSetValCh.split("_");
			int column = 0;
			String Searchtext = ObjectSetValtableheader[1];
			if (dCellDataValCh.length == 3)
			{
				for (column = 0; column < DTcolumncount; column++) 
				{
					if (Searchtext.equalsIgnoreCase(sheet2.getCell(column, 0).getContents()) == true) 
					{
						ObjectSetValCh = sheet2.getCell(column,dtrownum).getContents();
						iflag = 1;
					}

				}

			}else
			{
				for (column = 0; column < DTcolumncount; column++) 
				{
					if (Searchtext.equalsIgnoreCase(DTsheet.getCell(column,0).getContents()) == true) 
					{
						ObjectSetValCh = DTsheet.getCell(column, dtrownum).getContents();
						iflag = 1;
					}			
				}								
			}				
		}else if (ObjectSetValCh.contains("rt_"))
		{
			String ObjectSetValIni[] = ObjectSetValCh.split("_");
			String SearchFile = ObjectSetValIni[1];

			ObjectSetValCh=ReadRuntimeINI(SearchFile);	

		}



		Func_FindObj(ORvalname, ORvalue);

		switch (ObjectSetCh) {
		case "enabled":
			boolval = elem.isEnabled();
			actval = boolval.toString();
			break;
		case "text":
			actval = elem.getAttribute("text");
			break;
		case "gettext":
			actval = elem.getText();
			break;
		case "value":
			actval = elem.getAttribute("value");
			break;
		case "innerText":
			actval = elem.getAttribute("innerText"); //Changed by Shahid 
			break;

		case "innerHTML":
			actval = elem.getAttribute("innerHTML");//@ Shahid 
			break;
		case "displayed":
			boolval = elem.isDisplayed();
			actval = boolval.toString();
			break;
		case "checked":
			boolval = elem.isSelected();
			actval = boolval.toString();
			break;
		case "selection":
			Select select=new Select(elem);
			WebElement value=select.getFirstSelectedOption();
			actval=value.getText();
		}

		if ((ActionVal).equalsIgnoreCase("check")) {
			expval = ObjectSetValCh;
			if (expval.equalsIgnoreCase(actval)) {
				System.out
				.println("Actual value matches with expected value. Actual value is "
						+ actval);

				result = "Actual "   + " value " + actval + " matches with the expected value.";
				Update_Report("executed");
			} else {
				System.out
				.println("Actual " + objName + " value differs with expected value. Actual value is "
						+ actval);
				if (ORvalname == "exit") {
					Update_Report("missing");
				} else {
					result = "Actual " + objName + " value " + actval
							+ " does not match with the expected " + objName + "  value "
							+ expval;
					screenshot(loopnum, TScrowcount, TScname);
					Update_Report("failed");

				}
			}
			if (capturecheckvalue == true) {
				screenshot(loopnum, TScrowcount, TScname);
			}
		} else if ((ActionVal).equalsIgnoreCase("storevalue")) {
			varname = ObjectSetValCh;
			if (map.containsKey(varname)) {
				System.out
				.println("Overwriting the value of the variable "
						+ varname
						+ " to store the value as mentioned in the test case row number "
						+ rowcnt);
				map.remove(varname);
			}
			map.put(varname, actval);

			Update_Report("executed");
		}
		if (capturestorevalue == true) {
			screenshot(loopnum, TScrowcount, TScname);
		}
	}

	/*	public static void mailing1(String Mailadd, String Subject, String Body)
			throws AddressException, MessagingException {
		String host = "smtp.gmail.com";// host name
		String from = "";// sender id
		String to = Mailadd;// reciever id
		String pass = "Automation1";// sender's password
		String fileAttachment = "test.txt";// file name for attachment
		// system properties
		Properties prop = System.getProperties();
		// Setup mail server properties
		prop.put("mail.smtp.gmail", host);
		prop.put("mail.smtp.starttls.enable", "true");
		prop.put("mail.smtp.host", host);
		prop.put("mail.smtp.user", from);
		prop.put("mail.smtp.password", pass);
		prop.put("mail.smtp.port", "587");
		prop.put("mail.smtp.auth", "true");
		// session
		Session session = Session.getInstance(prop, null);
		// Define message
		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(from));
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
		message.setSubject(Subject);
		// create the message part
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		// message body
		messageBodyPart.setText(Body);
		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);
		// attachment
		messageBodyPart = new MimeBodyPart();
		DataSource source = new FileDataSource(
				"C:/Users/DPalacha/Desktop/Test/path/Configuration.xls");
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName("ExecutionReport.xls");
		multipart.addBodyPart(messageBodyPart);
		message.setContent(multipart);
		// send message to reciever
		Transport transport = session.getTransport("smtp");
		transport.connect(host, from, pass);
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();
	}*/

	@After
	public void close() throws Exception {
		try {
			System.out.println("Test end.");
//			Send_Email(emailID);
			Runtime.getRuntime().exec("cmd /c start " + TestSuite);
		} catch (UnhandledAlertException e) {
			System.out.println(e);
			System.out
			.println("Because of specification of SeleniumWebDriver, downloading may be failed.");
			System.out
			.println("Please confirm the report file and screenshot about test result.");
		}
	}

	private static void Func_FindObj(String strObjtype, String strObjpath) {

		if (strObjtype.equalsIgnoreCase("id")) {
			elem = D8.findElement(By.id(strObjpath));
		} else if (strObjtype.trim().equalsIgnoreCase("name")) {
			elem = D8.findElement(By.name(strObjpath));
		} else if (strObjtype.trim().equalsIgnoreCase("xpath")) {
			elem = D8.findElement(By.xpath(strObjpath));
		} else if (strObjtype.trim().equalsIgnoreCase("linkText")) {
			elem = D8.findElement(By.linkText(strObjpath.toString()));
		} else if (strObjtype.trim().equalsIgnoreCase("css")) {
			elem = D8.findElement(By.cssSelector(strObjpath));
		}
	}

	public static int ifContidionSkipper(String strifConditionStatus)
			throws Exception {
		try {

			String strKeyword;
			int intIfEndConditionCount, intIfConditionCount;
			intIfConditionCount = 1;
			intIfEndConditionCount = 0;
			if (strifConditionStatus.equalsIgnoreCase("false")) {
				do {
					j = j + 1;
					strKeyword = TScsheet.getCell(1, j).getContents();
					System.out.println(strKeyword);
					if (strKeyword.equalsIgnoreCase("Condition")) {
						intIfConditionCount = intIfConditionCount + 1;
					}
					if (strKeyword.equalsIgnoreCase("Endcondition")) {
						intIfEndConditionCount = intIfEndConditionCount + 1;
						if (intIfConditionCount == intIfEndConditionCount) {
							j = j + 1;
							break;
						}
					}

				} while (true);
			}
		} catch (Exception e) {

		}
		return j;
	}

	public String Func_IfCondition(String strConditionArgs) throws Exception {
		int iFlag = 1;
		String str[] = strConditionArgs.split(";");
		String value1 = str[0];
		String value2 = str[2];
		String strOperation = str[1];
		strOperation = strOperation.toLowerCase().trim();

		switch (strOperation) {
		case "equals":
			if (value1.substring(0, 1).equalsIgnoreCase("#")) {
				value1 = map.get(value1.substring(1, (value1.length())));
				System.out
				.println("Variable used in condition statement has value: "
						+ value1);
				if (value1.trim().equalsIgnoreCase(value2.trim())) {
					iFlag = 0;
					result = " Expected value " + "Condition passed";
					Update_Report("executed");

				}
			} else if (!value1.trim().equals(value2.trim())) {
				//else if (value1 !=value2) {
				iFlag = 0;
				result = " Expected value " + "Condition Failed";
				Update_Report("executed");
			}
			break;

		case "notequals":
			if (value1.substring(0, 1).equalsIgnoreCase("#")) {
				value1 = map.get(value1.substring(1, (value1.length())));
				System.out
				.println("Variable used in condition statement has values: "
						+ value1);
				if (value1.trim().equalsIgnoreCase(value2.trim())) {
					iFlag = 0;
				}
			} else if (!value1.trim().equals(value2.trim())) {
				iFlag = 0;
			}
			break;

		case "greaterthan":
			if (isInteger(value1) && isInteger(value2)) {
				if (Integer.parseInt(value1) > Integer.parseInt(value2)) {
					iFlag = 0;
				}
			} else {
				// Report
			}
			break;
		case "lessthan":
			if (isInteger(value1) && isInteger(value2)) {
				if (Integer.parseInt(value1) < Integer.parseInt(value2)) {
					iFlag = 0;
				}
			} else {
				// Report
			}
			break;
		default:
			Update_Report("missing");

		}
		if (iFlag == 0) {
			return "true";
		} else {
			return "false";
		}

	}

	public static boolean isInteger(String input) {
		try {
			Integer.parseInt(input);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	//@Chandra - added on 09.19.2013
	/*#######################################################################################################################################################################
	Method Name: ReadRuntimeINI
	Arguments: Runtime INI File Name
	Return Value: Value of the Ini file (option value). If file does not exist, then "" is returned.
	Description: This method connects to the ini file and reads the option value. Here Section Name, Option Name and File Name are maintained same for ease and simplicity	
	#######################################################################################################################################################################*/
	public static String ReadRuntimeINI(String sReadINI) throws IOException, IOException
	{

		String iniReadfileName = sReadINI;
		String iniReadSectionName = sReadINI;
		String iniReadOptionname = sReadINI;

		File file = new File(RunManagerPath+"/RunTimeData/"+iniReadfileName+".ini");

		if (file.exists())
		{
			Ini iniRead = new Ini(new File(file.getAbsolutePath()));
			String sReadINIFileVariableValue=iniRead.get(iniReadSectionName, iniReadOptionname);
			iflag=1;

			return sReadINIFileVariableValue;
		}else
		{
			System.out.println("Runtime data file "+iniReadfileName+".ini doesnot exist. Please check the variable and file name.");

			return 	"";
		}
	}

	//@Chandra - added on 09.19.2013
	/*#######################################################################################################################################################################
	Method Name: GetINIOptionValue
	Arguments: INI File Name with full path, Section Name and Option Name
	Return Value: Value of the option in the Ini file. If file does not exist, then "" is returned.
	Description: This method connects to the ini file and reads the option value from the given section name. 	
	#######################################################################################################################################################################*/
	public static String GetINIOptionValue(String iniReadfilePathName, String iniSectionName, String iniOptionName) throws IOException, IOException
	{	
		File inifile = new File(iniReadfilePathName);			

		if (inifile.exists())
		{
			//	Ini iniRead = new Ini(new File("C:/GWAutomationRunManager/Settings.ini"));
			Ini iniRead = new Ini(new File(inifile.getAbsolutePath()));
			String sReadINIFileVariableValue=iniRead.get(iniSectionName, iniOptionName);			
			return sReadINIFileVariableValue;
		}else
		{			
			String IniFileName=inifile.getName();		
			System.out.println("The file "+IniFileName+" doesnot exist. Please check the variable and file name.");		
			return 	"";
		}
	}

	//@Chandra - added on 09.19.2013
	/*#######################################################################################################################################################################
	Method Name: WriteRunTimeINI
	Arguments: INI File Name with full path, Section Name and Option Name
	Return Value: Value of the option in the Ini file. If file does not exist, then "" is returned.
	Description: This method connects to the ini file and reads the option value from the given section name. 	
	#######################################################################################################################################################################*/
	public static void WriteRunTimeINI(String inifileName, String iniValue) throws IOException
	{

		String iniSectioneName=inifileName;
		String iniOptionName=inifileName;

		File file = new File(RunManagerPath+"/RunTimeData/"+inifileName+".ini");

		if (!file.exists())
		{
			CreateINI(inifileName);												
		}

		Ini writeini = new Ini(new File(RunManagerPath+"/RunTimeData/"+inifileName+".ini"));													
		writeini.put(iniSectioneName, iniOptionName,iniValue);
		writeini.store();											
	}
	//@Chandra - added on 09.19.2013
	/*#######################################################################################################################################################################
	Method Name: CreateINI
	Arguments: INI File Name
	Return Value: No return value
	Description: This method creates an ini file in the RunTimeData folder. 	
	#######################################################################################################################################################################*/
	public static void CreateINI(String sINIFile) throws IOException, IOException
	{		
		File file = new File(RunManagerPath+"/RunTimeData/"+sINIFile+".ini");
		file.createNewFile();
		System.out.println("Runtime data file created-"+sINIFile+".ini");

	}
	//@Chandra - added on 09.20.2013
	/*#######################################################################################################################################################################
		Method Name: GetXLIdentifierRowNum
		Arguments: Excel File Name with path, Sheet Name and Identifier (search) value 
		Return Value: Row Number (Integer type)
		Description: This method searches all the first column cell values and when the macth is found returns the row number.
		#######################################################################################################################################################################*/

	public static Integer GetXLIdentifierRowNum(String FileName, String SheetName, String Identifiervalue) throws IOException
	{
		File xlFile = new File(FileName);
		FileInputStream xlDoc = new FileInputStream(xlFile);
		HSSFWorkbook WB = new HSSFWorkbook(xlDoc);
		HSSFSheet Sheet = WB.getSheet(SheetName);
		for (Row row : Sheet) {
			for (Cell cell : row) {
				if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
					if (cell.getRichStringCellValue().getString().trim().equals(Identifiervalue)) {

						System.out.println("This is the row num"+row.getRowNum());

						return row.getRowNum();  
					}
				}
			}
		}               
		return 0;



	}



	//@Abhi -- Added  Oct 8th 2013
	/*#######################################################################################################################################################################
	Method Name: sqlData
	Arguments: SQL Quere, database column name, datatable index value start with 1 
	Return Value: Required value from the data table with column name and index value will be saved as Runtime data ini file, file name will be column name of the data table 
	Description: Get data from data base
	#######################################################################################################################################################################*/
	public static void sqlData(String SQLQuery, String columnName, int indexValue, String DB_ENV) throws SQLException, IOException{
		int i = 0;

		Connection Conn0 = null;
		Connection Conn1 = null;
		Connection Conn2 = null;

		Conn0 = DriverManager.getConnection("jdbc:oracle:thin:@GPCABE5_db:1755:GPCABE5","query_dude","query"); //ABE5 DB
		Conn1 = DriverManager.getConnection("jdbc:oracle:thin:@GPCTST_db:1792:GPCTST","query_dude","query"); //TST DB
		//Conn2 = DriverManager.getConnection("jdbc:oracle:thin:@GPCABE5_db:1792:GPCABE5","query_dude","query"); //ACC DB

		Conn0.createStatement();


		String query = SQLQuery;

		Statement stmt0 = Conn0.createStatement();
		Statement stmt1 = Conn1.createStatement();
		ResultSet rs0 = stmt0.executeQuery(query);
		ResultSet rs1 = stmt1.executeQuery(query);
		// Get Count

		if (DB_ENV.equalsIgnoreCase("GPCABE5"))
		{
			while (rs0.next()) {

				if (cCellData.contains("AccountNumber"))
				{
					i++;
					String ResultColvalue0 = rs0.getString("AccountNumber");
					if(indexValue == i){
						System.out.println(ResultColvalue0);
						WriteRunTimeINI("AccountNumber", ResultColvalue0);
						break;
					}
				}


				else
				{

					i++;
					String ResultColvalue1 = rs0.getString("PolicyNumber");
					if(indexValue == i){
						System.out.println(ResultColvalue1);
						WriteRunTimeINI("PolicyNumber", ResultColvalue1);
						break;
					}

				}

			}

		}

		if  (DB_ENV.equalsIgnoreCase("GPCTST"))
		{
			while (rs1.next()) {

				if (cCellData.contains("AccountNumber"))
				{
					i++;
					String ResultColvalue2 = rs1.getString("AccountNumber");
					if(indexValue == i){
						System.out.println(ResultColvalue2);
						WriteRunTimeINI("AccountNumber", ResultColvalue2);
						break;
					}
				}

				else
				{

					i++;
					String ResultColvalue2 = rs1.getString("PolicyNumber");
					if(indexValue == i){
						System.out.println(ResultColvalue2);
						WriteRunTimeINI("PolicyNumber", ResultColvalue2);
						break;
					}
				}

			}


		}



	}

	public static void Send_Email(String emailOwner) throws MessagingException, IOException{
		String host = "172.16.1.86";
		String from = emailOwner;
		String slNo;
		String emailID;
		String status;
		String[] emailIdArray;
		int availableEmail = 0;


		File xlFile = new File(TestData);
		FileInputStream xlDoc = new FileInputStream(xlFile);
		HSSFWorkbook WB = new HSSFWorkbook(xlDoc);
		HSSFSheet Sheet = WB.getSheet("EmailID");
		int mailRowCount = Sheet.getLastRowNum();
		System.out.println(mailRowCount);
		HSSFCell cell;
		cell = Sheet.getRow(0).getCell(2);
		status = cell.getStringCellValue();
		System.out.println(status);

		emailIdArray = new String[mailRowCount];

		if(status.equalsIgnoreCase("ON")){

			for(int i = 1; i <= mailRowCount; i++ ){


				cell = Sheet.getRow(i).getCell(0);
				slNo = cell.getStringCellValue();
				System.out.println(slNo );


				cell = Sheet.getRow(i).getCell(1);
				emailID = cell.getStringCellValue();
				System.out.println(emailID);

				cell = Sheet.getRow(i).getCell(2);
				status = cell.getStringCellValue();
				System.out.println(status);
				if(status.equalsIgnoreCase("ON")){
					emailIdArray[availableEmail] = emailID ;
					availableEmail++;
				}

			}

			if(availableEmail > 0){
				// Get system properties
				Properties properties = System.getProperties();

				// Setup mail server
				properties.setProperty("mail.smtp.host", host);

				// Get the default Session object.
				Session session = Session.getDefaultInstance(properties);

				// Define message
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(from));

				InternetAddress[] mailAddress_TO = new InternetAddress [availableEmail] ;
				for(int i=0;i<availableEmail;i++){
					mailAddress_TO[i] = new InternetAddress(emailIdArray[i]);
				}
				message.addRecipients(Message.RecipientType.TO, mailAddress_TO);

				message.setSubject("Test Automation Report, Env Selected:  " + EnvSelected );

				// Create the message part 
				BodyPart messageBodyPart = new MimeBodyPart();

				// Fill the message
				messageBodyPart.setText("Please find the attached Automation Execution Report, If you have any Questions Please contact Automation team at UserName@ins.com " );


				//messageBodyPart.setText("//mem-co-stor1/is/Quality Engineering/GuidewireAutomation/Test/path/ScreenshotReportLog");

				Multipart multipart = new MimeMultipart();
				multipart.addBodyPart(messageBodyPart);

				// Part two is attachment
				messageBodyPart = new MimeBodyPart();
				String filename = TestSuite ;
				DataSource source = new FileDataSource(filename);
				messageBodyPart.setDataHandler(new DataHandler(source));
				messageBodyPart.setFileName("TestSuite.xls");
				multipart.addBodyPart(messageBodyPart);

				// Put parts in message
				message.setContent(multipart);

				// Send the message
				try{
					Transport.send(message);
				}
				catch(Exception e){
					System.out.println(e);
				}
				System.out.println("Execution Suit Emailed.");
			}else{
				System.out.println("No Email Sent");
			}


		}else
		{
			System.out.println("Email Option is Turned OFF");
		}
	}

	//@added by shahid 11/13/2013
	/*#######################################################################################################################################################################
			Method Name: ScanDigit
			Arguments: String Param.
			Return Value: True OR False
			Description: This method will check if a string contains digit values. 
			#######################################################################################################################################################################*/

	public final static boolean ScanDigit (String s){  
		boolean containsDigit = false;

		if(s != null){
			for(char c : s.toCharArray()){
				if(containsDigit = Character.isDigit(c)){
					break;
				}
			}
		}

		return containsDigit;
	}



	//@shahid 11/07/2013
	/*#######################################################################################################################################################################
					Method Name: NumberGen
					Arguments:  File Name and Number Length 
					Return Value: No return value String and Number
					Description: This method creates a a String or 10/9 digit Number 
					#######################################################################################################################################################################*/

	public static void NumberGen(String inifileName, int name100) throws IOException {

		long timeSeed = System.nanoTime(); // to get the current date time value

		double randSeed = Math.random() * 1000; // random number generation

		long midSeed = (long) (timeSeed * randSeed); // mixing up the time and
		// rand number.

		// variable timeSeed
		// will be unique


		// variable rand will 
		// ensure no relation 
		// between the numbers
		if (inifileName.equalsIgnoreCase("EntityName"))

		{
			String s = midSeed + "";
			String subStr = s.substring(0, name100)+" CORP QAE@QAE-ins.com";

			//String finalSeed = Integer.parseInt(subStr);    // integer value

			WriteRunTimeINI(inifileName,subStr);

			System.out.println(subStr);

		}

		else

		{
			String s = midSeed + "";
			String subStr = s.substring(0, name100);

			//String finalSeed = Integer.parseInt(subStr);    // integer value

			WriteRunTimeINI(inifileName,subStr);

			System.out.println(subStr);
		}


	}

	
	
	
	public static String[][] readSheet(String dataTablePath, String sheetName) throws IOException{
		String xlData[][] = null;
		dataTablePath = "C:/Users/domestic/Desktop/Framework/path/TestScript/" + dataTablePath+".xls";

		/*Step 1: Access Data Table Path */
		File xlFile = new File(dataTablePath);

		/*step2: Access the Xl File*/
		FileInputStream xlDoc = new FileInputStream(xlFile);

		/*Step3: Access the work book */
		HSSFWorkbook wb = new HSSFWorkbook(xlDoc);

		/*Step4: Access the Sheet */
		HSSFSheet sheet = wb.getSheet(sheetName);

		int iRowCount = sheet.getLastRowNum() + 1;
		int iColCount =sheet.getRow(0).getLastCellNum();

		xlData = new String[iRowCount][iColCount];

		for(int i = 0; i < iRowCount; i ++){
			for(int j = 0; j < iColCount; j ++){
				HSSFCell cell = sheet.getRow(i).getCell(j);
				xlData[i][j] = cell.getStringCellValue();
			}
		}
		return xlData;
	}
	
	
	
	public static void writeSheet(String dataTablePath,String sheetName, int irow, int icolumn, String xlData) throws IOException{

		/* Access The Path of Xl File*/
		File xlFile = new File(dataTablePath );
		/* Access the File */
		FileInputStream xlDoc = new FileInputStream(xlFile);  
		/* Access The Work Book */
		HSSFWorkbook wb = new HSSFWorkbook(xlDoc);
		/* Access the Sheet */
		HSSFSheet sheet = wb.getSheet(sheetName);
		/*Access Row*/
		HSSFRow row = sheet.getRow(irow);
		/*Access Column*/
		HSSFCell cell = row.getCell(icolumn);
		/*Set cell for String value*/
		cell.setCellType(HSSFCell.CELL_TYPE_STRING);
		/*Enter the value*/
		cell.setCellValue(xlData);

		/* Set Color */
		HSSFCellStyle titleStyle = wb.createCellStyle();
		if(xlData.equalsIgnoreCase("Present")){
			titleStyle.setFillForegroundColor(new HSSFColor.GREEN().getIndex());

//			titleStyle.setFillPattern();;
			cell.setCellStyle(titleStyle);
		}else if(xlData.equalsIgnoreCase("Not Present")){
			titleStyle.setFillForegroundColor(new HSSFColor.RED().getIndex());
//			titleStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
			cell.setCellStyle(titleStyle);
		}else{
			titleStyle.setFillForegroundColor(new HSSFColor.WHITE().getIndex());
//			titleStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
			cell.setCellStyle(titleStyle);
		}



		FileOutputStream fout = new FileOutputStream(dataTablePath);
		wb.write(fout);
		fout.flush();
		fout.close();

	}

	

	//@shahid 11/07/2013
	/*#######################################################################################################################################################################
				Method Name: NumberGen
				Arguments:  File Name and Number Length 
				Return Value: No return value String and Number
				Description: This method creates a a String or 10/9 digit Number 
				#######################################################################################################################################################################*/


}

