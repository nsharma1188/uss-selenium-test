package dashboard;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import ReadExcelData.ReadExcelConfig;
import ReadExcelData.WriteExcelData;

public class NewTestng {
	
	static WebDriver driver;
	private static String excelWorkSheetpath = "C:\\Nikhil\\Book.xlsx";
	
	
	
	
	
	private static  WebDriver CreateDriver() {

		System.setProperty("webdriver.chrome.driver", "C:\\selenium driver\\chromedriver.exe");
		driver = new ChromeDriver();
		return driver;
	}
	
	
	public static  WebElement findElementByXPath(WebDriver driver, String xpath) {
		return driver.findElement(By.xpath(xpath));
	}
	
	
  @Test
  public void loginfunctionality() {
	  logindashboard.loginFunctionality(CreateDriver());
	  
	  String CurrentUrl = driver.getCurrentUrl();
	  
	 Assert.assertEquals(CurrentUrl, "https://dev-oss.flyanra.net/#/login","Failed Url Did not mateched");
	 System.out.println("Passed");
	 WriteExcelData.writedata(0, 0, 0,"Passed",excelWorkSheetpath );
  }

	


 // @Test (dependsOnMethods= {"loginfunctionality"})
  public void Dashboard_Latest_flightlog() {
	  
	 
	  logindashboard.Dashboard_LatestFlight_log(driver);
	  
	  WebElement flight_log_telemetry = findElementByXPath(driver,"//*[@class='leaflet-clickable']");
	  String flight_telemetry = flight_log_telemetry.getAttribute("class");
	  Assert.assertEquals(flight_telemetry, "leaflet-clickable", "Flight log not generated");
	  System.out.println("Dashboard flight geometry Working fine");
	  
  }
  
  //@Test (priority=2,dependsOnMethods= {"Dashboard_Latest_flightlog"})
  
  @Test (priority=2,dependsOnMethods= {"loginfunctionality"})
  public void  Compare_DroneCount_NetworkView_ActualActiveDrone() {
	  
	  logindashboard.DashboardDroneCount_ActiveDrone(driver);
  WebElement GetListingTable = findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr/td[6]");
	  List <WebElement> Total_Drone_Count = GetListingTable.findElements(By.xpath("//*[contains(text(),'Active')]"));
	  int RequiredDroneCount=0;
		 for(WebElement currentDrone : Total_Drone_Count){
			  System.out.println("Current Drone is  "+currentDrone.getText()); 
			  if( currentDrone.getText().trim().equalsIgnoreCase("Active")) {
				  RequiredDroneCount= RequiredDroneCount+1;
			  }
		 }
		 System.out.println("Active Drone Count :"+RequiredDroneCount);	 
	  
		 
		 logindashboard.DroneCount_networkView(driver); 
		 
		   WebElement droneImageLink = findElementByXPath(driver,"//img[@src='./image/quad-copter-small.png']");
		   List <WebElement> findLink = droneImageLink.findElements(By.xpath("//img[@src='./image/quad-copter-small.png']"));
           int Count_total_link =findLink.size();
           System.out.println("Total Active Drone Count :"+Count_total_link);
		   
		   
	Assert.assertEquals(RequiredDroneCount, Count_total_link, "Drone Count And Drone On Network View Not Equal");   
	
	
 
  }
  
  
//  
  @Test (priority=3,dependsOnMethods= {"Compare_DroneCount_NetworkView_ActualActiveDrone"})
  public void  workspace_sketchfab() {
	  
	  logindashboard.workspace_sketchfab(driver);
	 
	  findElementByXPath(driver,"//a[@href='#/flight-data-listing']").click();
	  
	  WebDriverWait wait_for_searchtext= new WebDriverWait(driver,100);
		
	  wait_for_searchtext.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@class='searchTable au-target']")));
	  
	 
		
		
		findElementByXPath(driver,"//*[@class='searchTable au-target']").sendKeys(logindashboard.Mission_Name);
	    
		
		WebDriverWait wait_for_workspaceIcon = new WebDriverWait(driver,100);
		
		wait_for_workspaceIcon.until(ExpectedConditions.elementToBeClickable(By.xpath("//i[@class='anra-ico_positionupdates_dark']")));
	  
	  findElementByXPath(driver,"//i[@class='anra-ico_positionupdates_dark']").click();
	  
	  System.out.println("clicked on workspace icon");
	  
	  
	  WebDriverWait wait_for_loder = new WebDriverWait(driver,100);
		
	  wait_for_loder.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"body\"]/main/router-view/compose[1]/section/div/ul/li[3]/a/div/span[1]")));
	  
	  
	  try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	 
      findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/compose[1]/section/div/ul/li[3]/a/div/span[1]").click();
	  
	  findElementByXPath(driver,"//*[@id='btn3Dold']").click();
	  
	  try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	  
	  int size = driver.findElements(By.tagName("iframe")).size();
	  System.out.println("size>>>>> is"+size);
	  int size_1 = ((List<WebElement>) driver.switchTo().frame("3dViewer")).size();
	  
	  System.out.println("size_1>>>>> is"+size_1);
	  
	  WebElement sketchfab_canvas= findElementByXPath(driver,"//canvas[@class='canvas']");
       String canvas = sketchfab_canvas.getAttribute("class");
	  System.out.println(canvas);
	
	  Assert.assertEquals(size, size_1, "Sketch fab model not loaded");   
		
	  
	  
  } 
//  
//  @Test (priority=5,dependsOnMethods= {"workspace_sketchfab"})
//  public void Three_D_Inspection_model() {
//	  logindashboard.Three_D_Inspection_model(driver);
//  }
//  
//  
//  
//  
//  @Test (priority=6,dependsOnMethods= {"Three_D_Inspection_model"})
//  public void workspace_Orthomosaic() {
//	  logindashboard.Orthomosaic(driver);
//  }
  
  
  
  
  
  
  
  
}
