package dashboard;

import java.io.FileOutputStream;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.graphbuilder.curve.Point;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntries;

import CaptureScreenLib.CaptureScreen;
import ReadExcelData.ReadExcelConfig;
import ReadExcelData.WriteExcelData;

import javax.sql.RowSet;
import javax.swing.*;
import java.awt.MouseInfo;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.GridLayout;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;



public class logindashboard {

	private static final WebElement True = null;
	private static final WebElement Check2 = null;
	static WebDriver driver;
	private static ReadExcelConfig excel ;
	private static String excelWorkSheetpath = "C:\\Nikhil\\Book.xlsx";
	
	public static String Mission_Name;
	
	public static void main(String[] args) {
		CreateDriver();
//		Check_Services(driver);
		loginFunctionality(driver);
		//CreatingOrganisation();
		//CreateAUser();

//TODO----------------********Dashboard functionality Implementation******------------------0	
		
//		Dashboard_LatestFlight_log(driver);        //Dashboard under latest flight checks weather the telemetry log has been saved or not		
		DashboardDroneCount_ActiveDrone(driver);    //Counts active drone count on listing and compares it with network view
//		DroneCount_networkView(driver);
		

//TODO ---------------****** Plan mission Funtionality********-------------------------------
		
//		Polygrid_mission(driver);                    // Creates the mission 
//		Freehand_Mission(driver);                    // Creates the mission
//		LineString_Mission(driver);
//		Tornado_Mission(driver);
//		Tower_Circular_Mission(driver);
//		Facade_mission(driver); 
//		Guyed_wire_mission(driver);	
		
		
		
//TODO----------------******** Edit Mission Functionality*****-----------------------------
		
//		Edit_waypoint_Polygird(driver);              // Edit the mission that has been created
//		Edit_waypoint_Freehand(driver);               // Edit the mission that has been created
//		Edit_LineString_waypoint(driver);
//		Edit_Tornado_mission(driver);
//		Edit_Cricular_mission(driver);
//		Edit_Facade_waypointScreen(driver);
				
		

//TODO -----------------**************Generate MAnulally Flight Data On the Basis of Mission Planned.*****-----------------
		
//		Create_Flight_Data(driver);     // creation of flight data from polygrid mission. using mission name saved in excel.l
		
		
		
		
//TODO  -----------------------*******Workspace_upload Functionality Done*******-------------------------------
		
		WorkspaceUpload_image(driver);        //TODO implemenetation of Checks weather new Flight data Or Old for Processing data
//		Add_Annotation_Filter(driver);
//		Map_view(driver);                          // Map View is Enable or not and if we can see the tiles on the map view.
//		Orthomosaic(driver);                       // TODO Implementation of New Method
//	    Three_D_Inspection_model(driver);          // test case checks weather the modal has loaded on the canvas or not 	   
//		Workpsace_vedio(driver);

//	     workspace_sketchfab(driver);               // test case checks weather the modal has been loaded or not checks the size of the iframe
		
		
//TODO Implementation of loadin model from UI Currently checking For default Loaded model.

		
//        Process_Queued(driver);		


//		Orthomosaic(driver);                       // test the orthomosaic hase been loaded on the UI or not	
//		Live_drone_view(driver);                   		  
//		Network_view_Live_drone(driver);
		
		


		
	}

	private static void CreateDriver() {

		System.setProperty("webdriver.chrome.driver", "C:\\selenium driver\\chromedriver.exe");
		driver = new ChromeDriver();
		if(null!=driver){
		System.out.println("Driver is Null");
	
		}else {
			System.out.println("Driver is not Null");
			
		}
	}

	public static void ExplicitWait(WebDriver driver, String text) {

		(new WebDriverWait(driver, 10)).until(ExpectedConditions
				.elementToBeClickable(By.xpath("//div[@class='right-botton pull-right']/button[@type='button']")));
	}
	
	
	
	
	public static void Check_Services(WebDriver driver) {
	
	}
	
	
	
	
	public static void loginFunctionality(WebDriver driver) {
		//Below code for login to Anra Dashboard
		if(driver!=null){
		System.out.println("Driver is not Null");
		}else {
			System.out.println("Driver is Null");
		}
		
		driver.get("https://dev-oss.flyanra.net/#/login");
		
		// https://dashboard.flyanra.com/web/#/login
		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
		
		// thread.sleep applied for 
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		findElementByXPath(driver, "//*[@id='chkTermsCondition']").click();
		findElementByXPath(driver, "//*[@id='chkPrivacyPolicy']").click();

		findElementByXPath(driver, "//*[@id='btnAgree']").click();
		
		
		excel= new ReadExcelConfig(excelWorkSheetpath);
		String username1 = excel.getData(0,0,0);
		String password1 = excel.getData(0,0,1);
		
		System.out.println("User : "+username1);
		System.out.println("Pass : "+password1);

		findElementByXPath(driver, "//*[@id='email']").sendKeys(username1);
		findElementByXPath(driver, "//*[@id='password']").sendKeys(password1);
		findElementByXPath(driver, "//button[@type='submit']").click();
		
		String CurrentUrl = driver.getCurrentUrl();
		
		
		if(CurrentUrl.equals("https://dev-oss.flyanra.net/#/login"))
		{  
			// https://dashboard.flyanra.com/web/#/login
			
			System.out.println("Passed");
	
//			Write EXcel PAssed 
			
			WriteExcelData.writedata(0, 0, 0,"Passed",excelWorkSheetpath );
		
		}
		else
	        {
			System.out.println("Failed");
			
//			Write EXcel Failed 
			WriteExcelData.writedata(0, 0, 0, "Failed",excelWorkSheetpath );
		}
		
		}
	//}
	
	
	public static void CreatingOrganisation(WebDriver driver) {
		// Below code for opening Admin Screen
		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
		findElementByXPath(driver, "//a[@href='#/clients']").click();

		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
		System.out.println("finding organisation");
		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);

		findElementByXPath(driver, 
				"//div[@class='container']/ul[@class='row bashboard-block nav nav-pills']/li[@class='col-md-2 bas-bx-3 au-target']/a")
				.click();

		// command to check weather Search is working on organization listing page.

		// Test case condition to be implemented to Pass or fail

		WebElement searchbox = driver.findElement(By.className("searchTable"));
		searchbox.sendKeys("SNtechnologioes pvt ltd");

		// driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);

		searchbox.clear();

		// Search section ends

		// create organisation Section

		findElementByXPath(driver, "//button[@type='button']").click();

		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		ExplicitWait(driver, "//div[@class='right-botton pull-right']/button[@type='button']");

		System.out.println("found button to create organisation");
		findElementByXPath(driver, "//div[@class='right-botton pull-right']/button[@type='button']").click();

		findElementByXPath(driver, "//div[@class='input-group']/input[@type='text']").sendKeys("Amazon India");
		findElementByXPath(driver, "//div[@class='input-group']/input[@type='email']")
				.sendKeys("nsharma@flyanra.com");
		findElementByXPath(driver, 
				"//*[@id=\"body\"]/main/router-view/organization/section/div/div/div/form/div/div/div[1]/div[2]/div[3]/fieldset/div/input")
				.sendKeys("7503663518");
		findElementByXPath(driver, "//div[@class='input-group']/input[@type='url']").sendKeys("amit@Flyanra.com");
		findElementByXPath(driver, "//div[@class='input-group']/input[@placeholder='Government License']")
				.sendKeys("124578964-AH-4421-B");
		findElementByXPath(driver, "//div[@class='input-group']/input[@placeholder='Federal Tax ID/EIN']")
				.sendKeys("FTID-20166161");
		findElementByXPath(driver, "//div[@class='input-group']/input[@placeholder='Federal Tax ID/EIN']")
				.sendKeys("amit@Flyanra.com");
		findElementByXPath(driver, "//*[@class='ui-switch au-target ui-switch-info']").click();

		Select select = new Select(
				findElementByXPath(driver, "//div[@class='col-md-4']//select[@class='form-control au-target']"));
		select.selectByVisibleText("USD");

		Select select1 = new Select(findElementByXPath(driver, 
				"//*[@id=\"body\"]/main/router-view/organization/section/div/div/div/form/div/div/div[1]/div[3]/div[3]/fieldset/select"));
		select1.selectByVisibleText("Imperial");

		findElementByXPath(driver, "//*[@id=\"notes\"]").sendKeys("szfdxgchvjbk");
		findElementByXPath(driver, "//*[@id=\"email\"]").sendKeys("nsharma@flyanra.com");

		Select select2 = new Select(findElementByXPath(driver, 
				"//*[@id=\"body\"]/main/router-view/organization/section/div/div/div/form/div/div/div[2]/div[1]/div[2]/fieldset/select"));
		select2.selectByVisibleText("Free");

		findElementByXPath(driver, "//*[@id=\"password\"]").sendKeys("Password1");
		findElementByXPath(driver, "//*[@id=\"confirmpassword\"]").sendKeys("Password1");
		findElementByXPath(driver, "//*[@id=\"firstname\"]").sendKeys("Arick");
		findElementByXPath(driver, "//*[@id=\"lastname\"]").sendKeys("Williams");
		findElementByXPath(driver, 
				"//*[@id=\"body\"]/main/router-view/organization/section/div/div/div/form/div/div/div[3]/button[1]")
				.click();
		// findElementByXPath(driver, "//input[@class='searchTable
		// au-target']")).sendKeys("SNtechnologioes pvt ltd");

		// driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);

		// findElementByXPath(driver, "//input[@class='searchTable
		// au-target']")).sendKeys("");
		// command to create a new organisation

		// findElementByXPath(driver, "//button[@class='btn btn-primary btn-lg
		// au-target']")).click();

		// findElementByXPath(driver, "//*[@class='col-md-2 bas-bx-3 active
		// au-target']")).click();
		// findElementByXPath(driver, "//*[@href='#/organizations']")).click();
		System.out.println("Organisation Searched");

		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
		findElementByXPath(driver, "//*[@id=\"body\"]/nav-bar/header/div[1]/div[2]/div/div/ul/li[4]/ul/li[2]/a")
				.click();
		System.out.println("created organisation");
	}

	
	public static void CreateAUser(WebDriver driver) {
		findElementByXPath(driver, "//*[@id=\"body\"]/main/router-view/section[1]/div/ul/li[4]/a/div").click();
		findElementByXPath(driver, "//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[1]/div/div/button")
				.click();

		findElementByXPath(driver, "//input[@placeholder='First Name']").sendKeys("");
		findElementByXPath(driver, 
				"//*[@id=\"body\"]/main/router-view/user/section/div/div/form/div/div/div[2]/div[1]/div[2]/fieldset/div/input")
				.sendKeys("");
	}
	
	
	
	public static void Create_Flight_Data(WebDriver driver) {
		
		WebDriverWait hold= new WebDriverWait(driver,100);	
		hold.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@href='#/flight-data-listing']")));
		
		System.out.println("Found path");
		findElementByXPath(driver,"//a[@href='#/flight-data-listing']").click();
		
		WebDriverWait wait= new WebDriverWait(driver,100);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='btn btn-primary btn-lg au-target']")));
		
		findElementByXPath(driver,"//button[@class='btn btn-primary btn-lg au-target']").click();
		
		
		excel= new ReadExcelConfig(excelWorkSheetpath);
		String MissionName = excel.getData(1,0,0);
//		String password1 = excel.getData(0,0,1);
		
		System.out.println(MissionName);
		
//		TOdo selection of mission name on the basis of planned mission. Just Add MissionName to select feild.
		
		
		Select select = new Select(
				findElementByXPath(driver, "//div[@class='col-md-4']//select[@class='form-control au-target']"));
		select.selectByVisibleText(MissionName);
		
		
		findElementByXPath(driver,"//input[@id='startdt']").click();
		
//		findElementByXPath(driver,"//label[@for='pilot']").click();
//  ---------------------------------------Dynamic implementation of time to be done--------------------------------------
		
		
		
//	TODO Dynamic implementation of time after getting the start time and adding 30 min interval and setting it up in end time 	
//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//			
//		findElementByXPath(driver,"//label[@for='pilot']").click();
//		
//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//        String Start_Time= findElementByXPath(driver,"//input[@id='startdt']").getText();
//		
//		System.out.println(Start_Time);
//		System.out.println(">>>>>>>>>Backloloi kat gyi");
		
	
// ----------------------------------- End Of Dyanamic Part -----------------------		
		
// ------------------------------ Continued manual Process-------------------------------------		
		
		
//		try {
//			Thread.sleep(60000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
		
		
// Xpath for end time		
		findElementByXPath(driver,"//input[@id='enddt']").sendKeys("5:16 PM");
// Xpath for no of landing	
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/flight-data/section/div/div/form/div/div[2]/div[4]/div[2]/fieldset/input").clear();
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/flight-data/section/div/div/form/div/div[2]/div[4]/div[2]/fieldset/input").sendKeys("2");
//	Xpath for distance travelled	
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/flight-data/section/div/div/form/div/div[2]/div[4]/div[3]/fieldset/input").clear();
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/flight-data/section/div/div/form/div/div[2]/div[4]/div[3]/fieldset/input").sendKeys("350");
// Xpath for Altitude
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/flight-data/section/div/div/form/div/div[2]/div[5]/div[1]/fieldset/input").clear();
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/flight-data/section/div/div/form/div/div[2]/div[5]/div[1]/fieldset/input").sendKeys("200");
//  Xpath for Voltage begin	
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/flight-data/section/div/div/form/div/div[3]/div[1]/div[2]/fieldset/input").clear();
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/flight-data/section/div/div/form/div/div[3]/div[1]/div[2]/fieldset/input").sendKeys("5");
//	XPath for Voltage end	
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/flight-data/section/div/div/form/div/div[3]/div[1]/div[3]/fieldset/input").clear();
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/flight-data/section/div/div/form/div/div[3]/div[1]/div[3]/fieldset/input").sendKeys("2");
//	XPath for cloud cover
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/flight-data/section/div/div/form/div/div[4]/div[1]/div[1]/fieldset/input").sendKeys("32");
		
// Xpath for temprature 
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/flight-data/section/div/div/form/div/div[4]/div[1]/div[2]/fieldset/input").sendKeys("12");
// Xpath for wind sepeed
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/flight-data/section/div/div/form/div/div[4]/div[1]/div[3]/fieldset/input").sendKeys("34");
		
// Xpath for humidity	
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/flight-data/section/div/div/form/div/div[4]/div[2]/div/fieldset/input").sendKeys("10");
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/flight-data/section/div/div/form/div/div[5]/button[1]").click();
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static void WorkspaceUpload_image(WebDriver driver) {
		
		
//		//Xpath for Flight data
//				findElementByXPath(driver, "//*[@id=\"nav\"]/li[3]/a").click();
//				
//		
//	//Xpath for login Workspace icon
//				findElementByXPath(driver, "//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr/td[8]/a[1]").click();
//				
//				try {
//					Thread.sleep(10000);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//		
		
		// read operation from excel
		    
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	    
				String Mission_Name = excel.getData(1,0,0);
		
			findElementByXPath(driver,"//input[@class='searchTable au-target']").sendKeys(Mission_Name);	
		
			try {
				Thread.sleep(8000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			findElementByXPath(driver, "//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr/td[8]/a[1]").click();
			
		
    //	Xpath for upload button on header	
		
		findElementByXPath(driver,"//*[@id='icon-upload']").click();
		
//		xpath to select images upload section
		
//		WebDriverWait wait_for_Image_upload= new WebDriverWait(driver,180);
//		
//		wait_for_Image_upload.until(ExpectedConditions.elementToBeClickable(findElementByXPath(driver,"//*[@id='insp-main']/div[2]/ul/li[3]/ul/li[1]/a")));
				
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(">>>>>>>>>>>>finding Image Anchortag");
		
		findElementByXPath(driver,"//*[@id=\"insp-main\"]/div[2]/ul/li[4]/ul/li[1]/a").click();
		
		
		System.out.println(">>>>>>>>>>>>found Image Anchortag clicked to upload ");
		
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		Xpath for selecting the images section
		
//		findElementByXPath(driver,"//div[@class='qq-upload-button-selector qq-upload-button']").click();
		

		
		//		Xpath for uploading the complete file.
		
		
//		Working for single upload.
		

	    findElementByXPath(driver,"//input[@type='file']").sendKeys("D:\\Client Project\\BHU\\Testing\\DJI_0001.JPG");
	    
//	wait
	    
	    
	    
WebDriverWait wait= new WebDriverWait(driver,180);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='humane humane-original-info humane-animate']")));
		
		WebElement Upload_Notification  = findElementByXPath(driver,"//*[@id=\"body\"]/div");
		 String Notify = Upload_Notification.getText();
		 
		 
		 if ( Upload_Notification.isDisplayed()) {
			  
			  String Notify1 = Upload_Notification.getText();
			    
			    System.out.println("Got>>>>>>"+Notify);
		
			    if (Notify.equalsIgnoreCase("Media Uploaded Successfully!!")) {
					
			    	 try {
							Thread.sleep(10000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			    	
			    	 findElementByXPath(driver,"//button[@class='close au-target']").click();
			    	
			    	 
			    	 
			    }else {
			    	
			    	System.out.println("Upload notification did not showed up sucessfully");
			    }
	    
		 }
	    
	    
//	    Xpath for closing upload window by clicking X icon
	    
	    
	   
		

	    
//	 for multiple upload.	

//		findElementByXPath(driver,"//input[@type='file']").sendKeys(Keys.chord(Keys.CONTROL, "a"));
		
//		String selectAll = Keys.chord(Keys.CONTROL, "a");
//		findElementByXPath(driver,"//input[@type='file']").sendKeys(selectAll);  
	    

		 
		 
		 
		 
		 
		 
//TODO	Implementation   on Testng class 
//	    String CurrentUrl = driver.getCurrentUrl();
//	    
//	       System.out.println(CurrentUrl);
//	    
//	  String Abc = CurrentUrl.substring(CurrentUrl.lastIndexOf('/') + 1);
//	       
//	       System.out.println(Abc);
//	       
//	       
//		
//	WebElement Image_displayed=	findElementByXPath(driver,"//img[@id='annoImage']");
//		
//		String Is_Image_displayed=Image_displayed.getAttribute("src"); 
//		
//		
//		if (Is_Image_displayed.contains("https://s3.ap-south-1.amazonaws.com/in-oss-staging.flyanra.net/mission/"+Abc+"/gallery/DJI_0001.JPG")) {
//			
//			
//			System.out.println("Image Is being uploaded sucessfully and is displayed !");
//			
//			
//		}
//		
//		
//	   findElementByXPath(driver,"//button[@class='btn btn-primary btn-lg au-target']").click(); 
		
	}
	
	
	
	
	public static void Process_Queued(WebDriver driver) {
		
		
		
//		Xpath to visit Administration
			findElementByXPath(driver,"//*[@href='#/clients']").click();
			
			
			   try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
////			Xpath to visit Process Queue 
			findElementByXPath(driver,"//*[h3='Process Queue']").click();
			
			

			   try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		
			   WebElement sk_model =findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr[1]/td[6]/button");  
				sk_model.click();
				
				
////				Xpath For Browsing 
				 findElementByXPath(driver,"//input[@id='modelFile']").sendKeys("D:\\mesh obj\\mesh obj.zip");
				 

	}
	
	
	
	
	
	
	
	
	
	
	
	public static void Add_Annotation_Filter(WebDriver driver) {
		
//		This method is just picking up the first workspace that is in the list
//		We need to apply a check weather workspace is having images or not 
//		if workspace is not having images then upload and click on process data
//		again visit the same workspace and add annotation.
//		As of now it is adding annotation without any check for 
		
		
		//Xpath for login Workspace icon
		
//TODO When PRocess data button gets enabled.	
//		findElementByXPath(driver, "//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr/td[8]/a[1]").click();
//		
		System.out.println("Test Case For Annotating the Image Started ");
		
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		findElementByXPath(driver,"//img[@class='thumbnail image au-target']").click();
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		findElementByXPath(driver,"//div[@class='mapcaretshowhide au-target']").click();
		
		
		findElementByXPath(driver,"//*[@id=\"icon-annotate\"]").click();
		
		ClickforAnnotation();
		
		
		
		
		
		
		System.out.println("Annoattaion marking fineshedddddddd");
		
		
		findElementByXPath(driver,"//button[@class='btn btn-primary btn-md au-target']").click();
			
		findElementByXPath(driver,"//label[@class='radio-inline']/input[1]").click();
		
		findElementByXPath(driver,"//ul[@class='annotation-ul-inner']/li/a").click();
		
		findElementByXPath(driver,"//div[@class='annotation-ul ant-bx-2']//ul[@class='annotation-ul-inner']/li/a").click();
		
		findElementByXPath(driver,"//input[@id='comment']").sendKeys("Test_Annotation");
	
// TODO -------------------Dynamic Implementation of for adding an annotation and selecting it-------------------	
//		findElementByXPath(driver,"//div[@class='annotation-ul ant-bx-1']/a").click();
//	
//		
//		findElementByXPath(driver,"//div[@class='inp-add-input']/input").click();
//		
//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		findElementByXPath(driver,"//div[@class='inp-add-input']/input']").sendKeys("Test_Annotation");
//		
//		try {
//			Thread.sleep(20000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		
//		findElementByXPath(driver,"//*[@id=\"insp-main\"]/div[1]/div[2]/workspace-side-bar/div/div[2]/div[2]/div[2]/div[3]/ul/div/i").click();
//		
		
		
		
		
		
		
//		findElementByXPath(driver,"//div[@class='annotation-ul ant-bx-2']/a").click();
		
//		findElementByXPath(driver,"//div[@class='annotation-ul ant-bx-2']/a").sendKeys("Test_subAnnotation");
//		
//		findElementByXPath(driver,"//*[@id=\"insp-main\"]/div[1]/div[2]/workspace-side-bar/div/div[2]/div[2]/div[2]/div[3]/ul/div/i").click();
//		

//--------------------------		Dyanmic implementation to End here----------------------
		
		
//		XPath for Save button
		findElementByXPath(driver,"//*[@id=\"insp-main\"]/div[1]/div[2]/workspace-side-bar/div/div[2]/div[1]/div/div[3]/div/button").click();
		
		WebDriverWait wait= new WebDriverWait(driver,180);	
		
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='humane humane-original-info humane-animate']")));
		
		WebElement Upload_Notification  = findElementByXPath(driver,"//*[@id=\"body\"]/div");
		 String Notify = Upload_Notification.getText();
		 
		 
		 if ( Upload_Notification.isDisplayed()) {
			  
			  String Notify1 = Upload_Notification.getText();
			    
			    System.out.println("Got>>>>>>"+Notify);
		
		
	     if(Notify.equalsIgnoreCase("Annotation saved Sussesfully!")) {
	    	 
	    	 System.out.println(Notify); 
	    	 System.out.println("Test_Case_Passed");
	    	 
	     
	     }
	     
		 }
		
	}

	public static void Workpsace_vedio(WebDriver driver) {
		
//		//Xpath for Flight data
//		findElementByXPath(driver, "//*[@id=\"nav\"]/li[3]/a").click();
//		
//		
//		excel= new ReadExcelConfig(excelWorkSheetpath);
//		String MissionName = excel.getData(1,0,0);
////		String password1 = excel.getData(0,0,1);
//		
//		System.out.println(MissionName);
//		
//		
//		
//		//XPath for Serach box
//		findElementByXPath(driver, "//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[1]/div/div/input").sendKeys("Testing_polygrid");
//		
//		//Xpath for login Workspace icon
//		findElementByXPath(driver, "//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr/td[8]/a[1]").click();
//		
//		//Xpath For Sidebar menu
//
//		//WebDriverWait wait= new WebDriverWait(driver,400);
//		
//		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"body\"]/div[2]")));
//	
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		findElementByXPath(driver,"//*[@id=\"icon-upload\"]").click();
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//        WebDriverWait wait= new WebDriverWait(driver,40);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"insp-main\"]/div[2]/ul/li[4]/ul/li[2]/a")));

	   
//		WebDriverWait wait_for_vedio_upload= new WebDriverWait(driver,180);
//		
//		wait_for_vedio_upload.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\\\"insp-main\\\"]/div[2]/ul/li[4]/ul/li[2]/a")));
//				
		
		
		System.out.println(">>>>>>>>>finding Anchor tag For Vedio");
		
		
		
		findElementByXPath(driver,"//*[@id=\"insp-main\"]/div[2]/ul/li[4]/ul/li[2]/a").click();
		
		
		System.out.println(">>>>>>found and uploading vedio");
		
		try {
			Thread.sleep(9000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		findElementByXPath(driver,"//input[@type='file']").sendKeys("D:\\mesh obj\\Tower_vedio.mp4");
		
		System.out.println(">>>>>>>>>>>>> not done");
		
		
		WebDriverWait wait_for_notify= new WebDriverWait(driver,180);
		
		wait_for_notify.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='humane humane-original-info humane-animate']")));
		
		WebElement Upload_Notification  = findElementByXPath(driver,"//*[@id=\"body\"]/div");
		 String Notify = Upload_Notification.getText();
		 
		 
//		 if (Notify.equalsIgnoreCase("Media Uploaded Successfully!!")) {
//				
//	    	 try {
//					Thread.sleep(10000);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//	    	
//	    	 findElementByXPath(driver,"//button[@class='close au-target']").click();
//	    	 
//		 }
		 

		 if ( Upload_Notification.isDisplayed()) {
			  
			  String Notify1 = Upload_Notification.getText();
			    
			    System.out.println("Got>>>>>>"+Notify);
		
			    if (Notify.equalsIgnoreCase("Media Uploaded Successfully!!")) {
					
			    	 try {
							Thread.sleep(10000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			    	
			    	 findElementByXPath(driver,"//button[@class='close au-target']").click();
	}
// -------------------------If condition to get Exceute----------------------------			 		
		//------------------------------TRY---------------------------		
		 
//	 		findElementByXPath(driver, "//*[@id=\"menubutton\"]").click();
	 		
	 		//findElementByXPath(driver,"//*[@id='mySidenav']/div[2]/div[2]/div[6]/div/div/div/img").click(); 
	    	 
	    	 try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	 
	    	 System.out.println("finding the Vedio icon");
	 		
	 		 WebDriverWait wait1= new WebDriverWait(driver,120);
	 		
	 	     WebElement vedio_image =  wait1.until(ExpectedConditions.elementToBeClickable(findElementByXPath(driver, "//img[@src='image/video-icon.png']"))) ; 
	 		
	 	    
	 	      
	 		findElementByXPath(driver,"//img[@src='image/video-icon.png']").click();    
	 		
	 		 System.out.println("clicked and found the Vedio icon");
	 		
	 		//div[contains(@style,'display: block')]
	 		
	 		WebElement displaycond = findElementByXPath(driver, "//div[@class='playpause']");
	 		
	 		//WebElement check3 =findElementByXPath(driver, "//div[@class='playpause']");
	 		
	 		
	 		//System.out.println(check3);
	 		
	 		
	 		WebDriverWait cond= new WebDriverWait(driver,120);
	 		
	 		try {
	 			System.out.println("getTagName "+displaycond.getTagName());
	 			System.out.println("getCssValue "+displaycond.getCssValue("display: block"));
	 			System.out.println("getAttribute "+displaycond.getAttribute("style"));
	 			System.out.println("getSize "+displaycond.getSize());
	 			System.out.println("getLocation "+displaycond.getLocation());
	 			System.out.println("isDisplayed "+displaycond.isDisplayed());
	 			System.out.println("isDisplayed "+displaycond.isDisplayed());
	 			System.out.println("isSelected "+displaycond.isSelected());
	 		} catch (Exception e1) {
	 			//// TODO Auto-generated catch block
	 			e1.printStackTrace();
	 		}
	 		
	 		

	 		boolean isDisplayed = cond.until(ExpectedConditions.attributeContains(displaycond, "display", "block"));
	 		
	 		System.out.println(isDisplayed);
	 		
	 		System.out.println(displaycond);
	 		
	 		
	 		if(isDisplayed) {
	 			
	 			System.out.println("Vedio BuFfering!");
	 		}
	 		
	 		
	 		
	 		
	 		
	 		LogEntries logs = driver.manage().logs().get("browser");
	 		System.out.println(logs);
	 		
	 		
	 		//Condition to be implemented to check the psw swipe true or false on console.
	 		
	 		//Xpath for vedio 
	 		
	 		//findElementByXPath(driver, "//*[@id=\"page-content\"]/div/div[2]/div[1]/div[2]/div/div/div[2]").click();
	 		
	 		System.out.println("finding the play button");
	 		
	 		
	 		
	 	
	 		//CaptureScreen.CaptureScreenShotVedio(driver,"InitalVedioScreen");
	 		
	 		
	 		
	 		
	 		WebElement playbutton = findElementByXPath(driver, "//div[@class='playpause']");
	 		
	 		WebDriverWait wait_for_playIcon= new WebDriverWait(driver,40);
	 		
	 		wait_for_playIcon.until(ExpectedConditions.elementToBeClickable(playbutton));
	 			
	 		
	 		
	 		//JavascriptExecutor js=(JavascriptExecutor)driver;
	 		
	 		//js.executeScript("document.getElemetById(\"videoPlayer670\".click())");
	 		
	 			
	 			try {
	 				Thread.sleep(30000);
	 			} catch (InterruptedException e) {
	 				// TODO Auto-generated catch block
	 				e.printStackTrace();
	 			}
	 			
	 			
	 			
	 		
	 		playbutton.click();
	 		
	 		
	 		
	 		WebElement playdisplaycond =findElementByXPath(driver, "//div[@class='playpause']");
	 		
	 		
	 		
	 		WebDriverWait condplay= new WebDriverWait(driver,120);
	 		try {
	 			System.out.println("getTagName "+playdisplaycond.getTagName());
	 			System.out.println("getCssValue "+playdisplaycond.getCssValue("display: none"));
	 			System.out.println("getAttribute "+playdisplaycond.getAttribute("style"));
	 			System.out.println("getSize "+playdisplaycond.getSize());
	 			System.out.println("getLocation "+playdisplaycond.getLocation());
	 			System.out.println("isDisplayed "+playdisplaycond.isDisplayed());
	 			System.out.println("isDisplayed "+playdisplaycond.isDisplayed());
	 			System.out.println("isSelected "+playdisplaycond.isSelected());
	 		} catch (Exception e1) {
	 			//// TODO Auto-generated catch block
	 			e1.printStackTrace();
	 		}
	 		
	 		//boolean isdisplaycond = condplay.until(ExpectedConditions.attributeContains(playdisplaycond, "display", "none"));
	 		WebDriverWait wait = new WebDriverWait(driver, 100);

	 	    boolean waitUntil = wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='playpause']")));
	 		//String check2 =findElementByXPath(driver, "//*[@id=\"page-content\"]/div/div[2]/div[1]/div[2]/div/div/div[2]").getAttribute("style");
	 		
	 		//System.out.println(check2);
	 		
	 		if(waitUntil)
	 		{
	 			System.out.println("found button/Played");
	 			System.out.println("Vedio playing");
	 			
	 			try {
	 				Thread.sleep(4000);
	 			} catch (InterruptedException e) {
	 				// TODO Auto-generated catch block
	 				e.printStackTrace();
	 			}
	 			
	 			
	 			
	 			findElementByXPath(driver, "//button[@class='pswp__button pswp__button--close']").click();
	 		    System.out.println("Vedio Closed");
	 		    
	 		    
	 		   try {
	 				Thread.sleep(5000);
	 			} catch (InterruptedException e) {
	 				// TODO Auto-generated catch block
	 				e.printStackTrace();
	 			}
	 			
	 		   
	 		  findElementByXPath(driver,"//li[@class='add-filter-btn top-icons tooltip1']").click();
	 		}
		 }
	 		    
	 			
	 		
	 		//---------------------------------Try End--------------------------------		 
		 
//	----------------------------------- condition ends-----------------------------		    	 
			    	 
			    else {
			    	
			    	System.out.println("Upload notification did not showed up sucessfully");
			    }
		 }
		
		
	   
	    
	
	

	
	
	public static void workspace_sketchfab(WebDriver driver) {
		
//		Commented this section And Continued previously uploaded data for testing as process queue not working correctly.
		
		
//	//	Xpath to visit Administration
		findElementByXPath(driver,"//*[@href='#/clients']").click();
		
		
		   try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
////		Xpath to visit Process Queue 
		findElementByXPath(driver,"//*[h3='Process Queue']").click();
		
		

		   try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
////		Xpath for getting the name of the Queued data.
//		
	WebElement GetMission_Name	= findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr[1]/td[2]");
		
	 Mission_Name = GetMission_Name.getText();
		
		System.out.println(Mission_Name);
		
		
////		XPath for getting the Drop down Status of the mission.
		
		
	WebElement DropDown_Status0 = 	findElementByXPath(driver,"//select[@name='Filter']");
		
		String Mission_Status= DropDown_Status0.getText();
		System.out.println(Mission_Status);
		System.out.println("Status of mission is"+Mission_Status);
		
		
		
		if(Mission_Status.contains("Queued")) {
			
			
			Select ChangeMission_Status1 = new Select(
//					Xpath for filter    
					//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr[1]/td[8]/select
					findElementByXPath(driver, "//select[@name='Filter']"));
			        ChangeMission_Status1.selectByVisibleText("Processing");
	         
			System.out.println("Status Changed to Processing"+ChangeMission_Status1);
			
////		Commenting this section as We do not get the Notification When We Try to change The drop down with selenium
			 try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 
			 
//		Commenting this section as Email Notification Is not visible on UI When We change the Process Queue status

			 
//	     WebElement Notification_alert =findElementByXPath(driver,"//div[@class='humane humane-original-info']");
//			String Alert_Message = Notification_alert.getText();	
//			System.out.println(Alert_Message);	
//		
//		
//		
//			if(Alert_Message.equalsIgnoreCase("Email notification has been sent.")) {				
//				System.out.println("Status Changed to Processing");
//
//			} 
		
			
			
			
			  
			  
			  System.out.println("started working");
			                                                 //button[@class='btn btn-primary au-target au-target']
////			XPath For Uploading SF Model  
			WebElement sk_model =findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr[1]/td[6]/button");  
			sk_model.click();
			
			
////			Xpath For Browsing 
			 findElementByXPath(driver,"//input[@id='modelFile']").sendKeys("D:\\mesh obj\\mesh obj.zip");

			 // XPAth For 		
			 
			   try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    
			 
			 findElementByXPath(driver,"//a[@class ='btn btn-default fileinput-upload fileinput-upload-button']").click();
			 
			 
			 
			 
			 
			 
//			   try {
//					Thread.sleep(36000);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//              
			   
			   
			   System.out.println("Trying to get Notificationnnnnnnn");
			   
			   
			//Xpath For Upload notification     humane humane-original-info                         
			
			   
			WebDriverWait wait= new WebDriverWait(driver,100);
			
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='humane humane-original-info humane-animate']")));
			
			WebElement Upload_Notification  = findElementByXPath(driver,"//*[@id=\"body\"]/div");
			 String Notify = Upload_Notification.getText();
			
			try {
				Upload_Notification.getAttribute("class");
				   Upload_Notification.getText();
				  
				   
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				System.out.println("Error Could not find");
			}
			   
			   
			    
			    
			  if ( Upload_Notification.isDisplayed()) {
				  
				  String Notify1 = Upload_Notification.getText();
				    
				    System.out.println("Got>>>>>>"+Notify);
				  
				  try {
						Thread.sleep(10000);
						System.out.println("thread is waiting for 10 secnds and the it will close my modal.");
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				    
				                            //button[@class='close au-target']
	        	findElementByXPath(driver,"//*[@id=\"myModal\"]/div/div/div[3]/button").click();  
	        	System.out.println("Closed moy modal");
				   
//	            try {
//					Thread.sleep(30000);
//					
//					System.out.println("Closed moy modal");
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//	        	
//	        	  
	        	    System.out.println(Notify);
	        	
				    System.out.println(Notify1);
				    
				    
				   if (Notify.equalsIgnoreCase("3D model uploaded and being process.")) {
					   
					  
					   try {
							Thread.sleep(28000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					   
					   System.out.println("awake clicking refresh icon");
					   
					 findElementByXPath(driver,"//i[@class='fa fa-refresh']").click(); 
					 
					 System.out.println("clicked refresh icon");
					   
					WebElement check_upload_notification =findElementByXPath(driver,"//div[@class='humane humane-original-info humane-animate']");
					String Sketchfab_modal_upload = check_upload_notification.getText();
					
					
					System.out.println(Sketchfab_modal_upload);
					
					while(Sketchfab_modal_upload!="3D Model has been processed.")
					{
						
						 try {
								Thread.sleep(20000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						
						 findElementByXPath(driver,"//i[@class='fa fa-refresh']").click();
						
						 WebElement check_upload_notifications =findElementByXPath(driver,"//div[@class='humane humane-original-info humane-animate']");
							String Sketchfab_modal_uploads = check_upload_notifications.getText();
							
							System.out.println(Sketchfab_modal_uploads);
							
						 if (Sketchfab_modal_uploads.equals("3D Model has been processed.")) {
							 
							 
							 System.out.println("3D Model has been processed.");
							 break;
							 
						 }
					
					}
					
					
//					if(Sketchfab_modal_upload.equalsIgnoreCase("3D Model has been processed.")) {
//						
//						System.out.println("3D Model has been processed.");
//						
//					}
					
					
				   
				   }
				  
			  }
			          
			  else
			  {
				  
				  System.out.println("Notification Did not got displayed");
				  
			  }
			    
			   
			   //			Xpath for Closing the 3dmodal window 
			    
			   
			   
//			    while(true)
//			    {
//			    	 
//			    	 int size = driver.findElements(By.tagName("iframe")).size();
//			    	 if(size == 1)
//			    	 {
//			    		 System.out.println(size);
//			 		    
//					     
//					     break;
//			    	 }
//			    	 else
//			    	 {
//			    		 try {
//								Thread.sleep(5000);
//							} catch (InterruptedException e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
//			    	 }
//			    	 
//			    	 driver.switchTo().frame("3dViewer");
//					   
//				     WebElement sketchfab_canvas= findElementByXPath(driver,"//canvas[@class='canvas']");
//				     String canvas = sketchfab_canvas.getAttribute("class");
//				     
//				     System.out.println(canvas);
//			 
//			
//		}
		
    
		    
		 
		    }
		    
		     
		     
		//	WebElement progressbar =findElementByXPath(driver, "html/body/div[1]/div[4]/div[1]/div[2]/div/p[1]).getText()");
			  
		   //  WebElement progressbar  = findElementByXPath(driver, "html/body/div[1]/div[4]/div[1]/div[2]/div/p[1].getText();");
			
		    		//System.out.println(progressbar);
		    
	
		
	}
	
	public static void Three_D_Inspection_model(WebDriver driver) {
		
//		//XPAth for Flight log menu button
//		findElementByXPath(driver,"//*[@id=\"nav\"]/li[3]/a").click(); 
//		
//		
//		
//		excel= new ReadExcelConfig(excelWorkSheetpath);
//		String MissionName = excel.getData(1,0,0);
////		String password1 = excel.getData(0,0,1);
//		
//		System.out.println(MissionName);
//		
//
//		
//		//XPath for search box
//	     
//		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[1]/div/div/input").sendKeys(MissionName);
//		
//		try {
//			Thread.sleep(10000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		
//		
//		
//		//Xpath for workspace icon
//		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr[1]/td[8]/a[1]/i").click();
		             
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		findElementByXPath(driver,"//*[@id=\"icon-upload\"]").click();
		
		
		try {
			Thread.sleep(1300);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
//		WebDriverWait wait_for_3dinspection= new WebDriverWait(driver,180);
//		
//       wait_for_3dinspection.until(ExpectedConditions.elementToBeClickable(findElementByXPath(driver,"//*[@id=\"insp-main\"]/div[2]/ul/li[3]/ul/li[3]/a")));
//				
		System.out.println(">>>>>>>>>> finding upload for 3d inspection anchor tag");
		
		findElementByXPath(driver,"//*[@id=\"insp-main\"]/div[2]/ul/li[4]/ul/li[3]/a").click();
		
		
		System.out.println(">>>>>>>>>>>>found model  Anchortag clicked to upload ");
		
		try {
			Thread.sleep(9000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		findElementByXPath(driver,"//input[@type='file']").sendKeys("D:\\mesh obj\\mesh obj.zip");
		
		System.out.println(">>>>>>>>>>>>> not done");
		
		
		WebDriverWait wait= new WebDriverWait(driver,180);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='humane humane-original-info humane-animate']")));
		
		WebElement Upload_Notification  = findElementByXPath(driver,"//*[@id=\"body\"]/div");
		 String Notify = Upload_Notification.getText();
		 
		 
		 if ( Upload_Notification.isDisplayed()) {
			  
			  String Notify1 = Upload_Notification.getText();
			    
			    System.out.println("Got>>>>>>"+Notify);
		
			    if (Notify.equalsIgnoreCase("Media Uploaded Successfully!!")) {
					
			    	 try {
							Thread.sleep(10000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			    	
			    	 findElementByXPath(driver,"//button[@class='close au-target']").click();
			    	 
			    	
			    	 
			    	 
			    }else {
			    	
			    	System.out.println("Upload notification did not showed up sucessfully");
			    }
		 }
		
//			//Xpath for drop down 3d
//			
//			findElementByXPath(driver,"//*[@id=\"page-content\"]/compose[1]/section/div/ul/li[2]/a/div").click();
//			
//			//xpath for 3d inspection view
//			
//			findElementByXPath(driver,"//*[@id=\"btn3D\"]").click();
//			
//			try {
//				Thread.sleep(5000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			
//			//Xpath for loader
//			
//			WebElement loader1 =   findElementByXPath(driver,"//*[@class='progressbar-text']");
//			
//			String verify_loader = loader1.getText();
//			
//			System.out.println(verify_loader);
//			
//			if(verify_loader.contains("Loading...")){
//				
//				System.out.println("3D inspection modal is loading");
//				
//				WebDriverWait wait1= new WebDriverWait(driver,300);
//				
//				//Xpath for canvas_3d modal to be tried --- canvas[@id='threedViewer']
//				
//				boolean canvas_three_d =  wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"threedViewer\"]"))) !=null;
//				
//			if(canvas_three_d) {
//			
//				// Write Result in Excel PAssed 
//			System.out.println("3D inspection modal is loaded");
//			
//		          }
//				
//			}else {
//				
//				System.out.println("loader not found model is not loaded");
//			}
//				
			
		
		
	}
	
	
	
	public static void Orthomosaic (WebDriver driver) {
		
		//below two xpath are used for new sandbox url
		  // findElementByXPath(driver,"//*[@id=\"page-content\"]/compose[1]/section/div/ul/li[1]/a/div/div").click();
		   
		 //Xpath for orhtomosaic 
		//	findElementByXPath(driver, "//*[@id=\"ortho\"]").click();
		
		
		
		//Xpath for Flight data
				findElementByXPath(driver, "//*[@id=\"nav\"]/li[3]/a").click();
				
				//XPath for Serach box
				findElementByXPath(driver, "//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[1]/div/div/input").sendKeys("Vishwanath Temple");
				
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//Xpath for login Workspace icon
				findElementByXPath(driver, "//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr/td[8]/a[1]").click();
				
				//Xpath For Sidebar menu

				//WebDriverWait wait= new WebDriverWait(driver,400);
				
				//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"body\"]/div[2]")));
			
				try {
					Thread.sleep(40000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
		
		
		
		
		
		//Xpath for 2d orthomosaic button used this class  as this is the first box in the div
		
		findElementByXPath(driver,"//div[@class='das-bx']").click();
		
		
	    try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
//	    Xpath for Orthomosaic view.
		findElementByXPath(driver,"//*[@id='ortho']").click();
	    
		
	    try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	    
			//XPath for ortho title box  
		WebElement Ortho_Tile =	findElementByXPath(driver, "//*[@id=\"map\"]/div[1]/div[1]/div[2]/div[2]/img[1]");
		
		String Tiles = Ortho_Tile.getAttribute("src");
			
		//	findElementByXPath(driver,"//*[@id=\"map\"]/div[1]/div[1]/div[2]/div[2]']");
			
		    System.out.println(Tiles);
		    
		    
		    if(Tiles.contains("https://dashboard.flyanra.com/web/media/")) {
		    	
		    	// need to implement write result on excel 
		    	
		    	System.out.println("tiles loaded succesfully");
		    	
		    	
		    }else {
		    	
		    	System.out.println("tiles not loaded on map");
		    }
		    
	}
	
	

	
	public static void Live_drone_view (WebDriver driver) {
		
		
		// goto flight plan listing screen
		
		findElementByXPath(driver,"//*[@id=\"nav\"]/li[2]/a").click();
		
		// edit and get drone name and value
		
		//Edit on the mission planned get value
		//findElementByXPath(driver,"//*[@class='fa fa-pencil-square-o']").click();
		
	//String getDrone =	findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[1]/div[3]/fieldset/select").getText();
		
//		System.out.println(getDrone);
		
	    
	    try {
			Thread.sleep(18000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//after that click on live mission view Button under action
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr[1]/td[8]/a[3]").click();
		
		// click on side menu bar check the mode of the drone.
		
		findElementByXPath(driver,"//*[@id='menubutton']").click();
		
		String getMode = findElementByXPath(driver,"//*[@class='mode']").getText();
		
		System.out.println(getMode);
		
		if(getMode!=null && getMode.equalsIgnoreCase("Mode OFFLINE"))
		{
			findElementByXPath(driver,"//*[@class='anra-ico_videocamera_dark']").click();
			
			findElementByXPath(driver,"//*[@class='ppstart active']").click();
			
			String invalid_streamtype = findElementByXPath(driver,"//*[@class='pptestcard']").getText();
			
			if(invalid_streamtype.equalsIgnoreCase("#11 Invalid streamType and/or streamServer settings for ANRA Livestream.")) 
			{
				
				// write in excel 
				
				System.out.println("Offline mode Live streaming not enabled ");
			}
			else {
				
				System.out.println("Mode offline but vedio Streaming in not confiened to single mission with same drone.");
			}
		      
			
			
		}
		
	//	String getMode = findElementByXPath(driver,"//*[@class='mode']").getText();
		// System.out.println(getMode+"Online Mode packets recieved.");
		
		if(getMode!=null && getMode.equalsIgnoreCase("GPS_ATTI"))
		{
		
			//xpath for wifi connection. when drone gets connected.
			findElementByXPath(driver,"//*[@class='fa fa-wifi']");	
			
			//xpath for battery status when drone is live
		 String battery_Status = findElementByXPath(driver,"//*[@class='au-target progress-bar progress-bar-info progress-bar-striped active']").getText();
		
		   System.out.println(battery_Status);
		   
		//longitude xpath
		
		String Longitude = findElementByXPath(driver,"//*[@id=\"menu1\"]/div/div[6]/div[1]/label").getText();
		System.out.println(Longitude);
		//latitude xpath 
		
		String lattitude = findElementByXPath(driver,"//*[@id=\"menu1\"]/div/div[6]/div[2]/label").getText(); 
		System.out.println(lattitude);
		//altitude xpath
		
		String Altiude=findElementByXPath(driver,"//*[@id=\"menu1\"]/div/div[6]/div[3]/label").getText();
		
		
		System.out.println("Online mode Drone is live");
		
		// need to implement write result on excel 
		
		}
		
		
		
	}
	
	
	public static void Network_view_Live_drone (WebDriver driver) {
		
		
		
		findElementByXPath(driver,"//*[@id=\"nav\"]/li[1]/a/i").click();
		
		 try {
				Thread.sleep(18000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[1]/div/ul/li[2]/a").click();
		
		try {
			Thread.sleep(18000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		WebElement Network_live_drone = findElementByXPath(driver,"//img[@src='./image/quad-copter-small-live.png']");
		 
		
		try {
			
			System.out.println("Attribute of Drone"+Network_live_drone.getAttribute("src"));
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//img[contains(@src,'web/image/quad-copter-small-live.png')]
		String live_drone = Network_live_drone.getAttribute("src");
		
		System.out.println(live_drone);
		
		if (live_drone.equals("https://dashboard.flyanra.com/web/image/quad-copter-small-live.png")) {
			
			// need to implement write result on excel 
			
			System.out.println("Drone can be seen live on network view");
			
		}else {
			
			System.out.println("Network View Not showing live Drone and its  Movement ");
		}
		
		
		
	}
	
	 
	public static void Dashboard_LatestFlight_log(WebDriver driver) {
	
    findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr[1]/td[7]/a/i").click();
    
    
   WebElement flight_log_telemetry = findElementByXPath(driver,"//*[@class='leaflet-clickable']");
  
   WebDriverWait wait= new WebDriverWait(driver,180);
	wait.until(ExpectedConditions.visibilityOf(flight_log_telemetry));
     
//	String flight_telemetry = flight_log_telemetry.getAttribute("class");
//	
//	System.out.println(flight_telemetry);
//	
//	if (flight_telemetry.equals("leaflet-clickable")) {
//		
//		// need to implement write result on excel 
//		
//	System.out.println("Dashboard flight geometry Working fine");
//		
//	}else {
//		
//		System.out.println("Dashboard flight geometry Not Working fine");
//		
//	}
//	
     }
	
	
	
	public static void DashboardDroneCount_ActiveDrone(WebDriver driver) {
		
		//Xpath for dashboard link from side menu bar
		findElementByXPath(driver,"//*[@id=\"nav\"]/li[1]/a").click();
		
		//Xpath for drone count on the 
		
//		WebElement findLink = findElementByXPath(driver,"//img[@src='./image/quad-copter-small.png']");
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		//Xapth To get the total count displayed on the UI
		
	WebElement count_drone =	 findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[1]/div/ul/li[4]/a/div/div[2]/span[@class='cnt-no']");
	 
	System.out.println("total count of the drone : "+count_drone.getText());
	
	   String Dashboard_ActiveDroneCount = count_drone.getText();
	   
	   System.out.println(Dashboard_ActiveDroneCount);
		
//		 findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[1]/div/ul/li[4]/a/div/div[2]/span").getAttribute("text");
		 
	
			try {
					Thread.sleep(48000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	
		 
//		 Xpath for Finding Active Drone 
		 
		 findElementByXPath(driver,"//li[@class='col-md-4 bas-bx-4']").click();
		
		//XPAth to get the Total Count  of active Drones
		 
		 try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		 
		 Select Page_size = new Select(
					findElementByXPath(driver, "//select[@id='pageSize']"));
		 Page_size.selectByVisibleText("50");
			
		 
		 try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
//TODO   Commennted this section As This code has been used in testng 
		 
//		 WebElement GetListingTable = findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr/td[6]");
//		 List <WebElement> InActive_Drone_Count = GetListingTable.findElements(By.xpath("//*[contains(text(),'InActive')]"));
//		 List <WebElement> Total_Drone_Count = GetListingTable.findElements(By.xpath("//*[contains(text(),'Active')]"));
//		 
//		 int Active_drones = Total_Drone_Count.size()-InActive_Drone_Count.size();
//         System.out.println("Total Active Drone Count :"+Active_drones);
//		
//			
//		 
//		 
//		 int RequiredDroneCount=0;
//		 for(WebElement currentDrone : Total_Drone_Count){
//			  System.out.println("Current Drone is  "+currentDrone.getText()); 
//			  if( currentDrone.getText().trim().equalsIgnoreCase("Active")) {
//				  RequiredDroneCount= RequiredDroneCount+1;
//			  }
//		 }
//		 System.out.println("Active Drone Count :"+RequiredDroneCount);	  
	
	}
	
	
	
	
	public static void DroneCount_networkView(WebDriver driver) {
		
		
		
		 //	XPAth for Network View Count and verification	 
		 
		 
		 findElementByXPath(driver,"//a[@href='#/']").click();
		 
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		 
			//Xpath To get the total count displayed on the UI
			
			WebElement Count_On_NetworkView =	 findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[1]/div/ul/li[4]/a/div/div[2]/span[@class='cnt-no']");
			 
			System.out.println("total count of the drone on Network view : "+Count_On_NetworkView.getText());
			
			   String Dashboard_NetworkViewCount = Count_On_NetworkView.getText();
			   
			   System.out.println(Dashboard_NetworkViewCount);
			   
       //			   XPath for Clicking the Div Of Network View
			   
			   
			   findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[1]/div/ul/li[2]/a/div").click();
			   
			   
			//  Xpath to get the listing of of Drone on UI  Comented Below code Used InNew TestngClass
			   
			   
//			   WebElement droneImageLink = findElementByXPath(driver,"//img[@src='./image/quad-copter-small.png']");
//			  List <WebElement> findLink = droneImageLink.findElements(By.xpath("//img[@src='./image/quad-copter-small.png']"));
//			   
//			   int Count_total_link =findLink.size();
//		 
//			   System.out.println("Total Active Drone Count :"+Count_total_link);
		
		
		
		
	}
	
	


	public static void Polygrid_mission (WebDriver driver) {
		
		//XPath for Side menu Flight plan button.
		findElementByXPath(driver,"//*[@href='#/missions']").click();	
		
		
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Xpath for adding new mission button
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[1]/div/div/button").click();
		
		System.out.println("clicked on button");
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Xpath To select polygrid mission Div
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div/div/div[2]/div/div[5]/button/div[1]/div[2]/span").click();
		
		// read operation from excel
		    
		String Mission_Name = excel.getData(1,0,0);
		
		
		
		
		
		
       //	Running Code for that has been commented
		
//		String[] rowValues = excel.getAllDataOfaRow(1, 0, 0);
//		String[] columnValues = excel.getAllDataOfaColumn(1, 0, 0);
		
//		for(int i=0;i<rowValues.length;i++){
//			System.out.println("Value of "+i+" cell is = "+rowValues[i]);
//		}
		
//		for(int i=0;i<columnValues.length;i++){
//			System.out.println("Value of "+i+" cell is = "+columnValues[i]);
			
//		}
		
		
		
		
       // Commented area of code to be uncommented
		
		
		
		
		
		
		//String Mission_Name2 = excel.getData(1,1,0);
		
		
		
//		System.out.println(Mission_Name);
//		System.out.println(Mission_Name2);
		
		String[] columnValues = excel.getAllDataOfaColumn(1, 0, 0);
		
		
		
		
		for(int i=0;i<columnValues.length;i++){
			System.out.println("Value of "+i+" cell is = "+columnValues[i]);
			
			
			//xpath For Mission Name
			
			findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[1]/div[1]/fieldset/input").sendKeys(columnValues[i]);
			
			//Xpath for Pilot for selecting a prticular pilot from the Drop down listing
			
			Select select_pilot = new Select(
					findElementByXPath(driver, "//div[@class='col-md-4']//select[@class='form-control au-target']"));
			select_pilot.selectByVisibleText("Amit Ganjoo");
			
			Select Select_drone = new Select(
					findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[1]/div[3]/fieldset/select"));
			Select_drone.selectByVisibleText("Testing Drone");
			
			Select Select_Project = new Select(
					findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[2]/div[1]/fieldset/select"));
			Select_Project.selectByVisibleText("Tower Inspection 1");
			
			Select Select_Location = new Select(
					findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[2]/div[2]/fieldset/select"));
			Select_Location.selectByVisibleText("testing_location");
			
			//xpath for date selection
			findElementByXPath(driver,"//*[@id=\"flightDate\"]").sendKeys("10052018");
			
			Select Select_Flight_type = new Select(
					findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[3]/div[2]/fieldset/select"));
			Select_Flight_type.selectByVisibleText("Test Flight");
			
			   
			
			Select Select_Organisation = new Select(
					findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[3]/div[3]/fieldset/select"));
			Select_Organisation.selectByVisibleText("ANRA Technologies");
			
			
			
			
			// Xpath for setting altitude
			findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[4]/div[2]/fieldset[1]/input").sendKeys("0");
		
		    //Xpath for Selecting battery
		    
			Select Select_Battery = new Select(
					findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[3]/div/div[1]/fieldset/select"));
			Select_Battery.selectByVisibleText("Battery1");
			
			//Xpath for Selecting Sensor
			Select Select_Sensor = new Select(
					findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[3]/div/div[2]/fieldset/select"));
			Select_Sensor.selectByVisibleText("Camera");
			
			try {
				Thread.sleep(15000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[4]/button[1]").click();
			
			
			String CurrentUrl = driver.getCurrentUrl();
			
			WebElement  Mission_name = findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr[1]/td[1]");
			
			String Get_mission_name = Mission_name.getText();
			
			System.out.println(Get_mission_name);
			
			String loopvalue = columnValues[i];
			
			System.out.println("Loop value is = "+loopvalue);
			
//			if (CurrentUrl!=null && Get_mission_name.equalsIgnoreCase(loopvalue) && CurrentUrl.equals("https://sandbox-oss.flyanra.net/#/missions")) {
//				
				if(Get_mission_name.equalsIgnoreCase(loopvalue) ) {
//				System.out.println("value taken for mission "+columnValues[i]);
//				
//				if (Get_mission_name.equals(columnValues[i])) {
					
					System.out.println("PAssed ");
					
					WriteExcelData.Write_mission_data(1, 0, 1,"Passed",excelWorkSheetpath );
					
//				}
				
			}else {
				
				System.out.println("Failed ");
				WriteExcelData.Write_mission_data(1, 0, 1,"Failed",excelWorkSheetpath );
				
			}
			
			
			
			
			
		}
		
		
		
	
		
		
		
		
		
		
		
	}
	
	public static void Edit_waypoint_Polygird(WebDriver driver) {
		
		
		        //XPath for Side menu Flight plan button.
				findElementByXPath(driver,"//*[@href='#/missions']").click();	
		
		        // Xpath to clickOn search
				
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[1]/div/div/input").sendKeys("Polygrid");
				
				// Xpath to click on edit button
				
			WebDriverWait wait=new WebDriverWait (driver,10);
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@class='fa fa-pencil-square-o']")));
					
					findElementByXPath(driver,"//*[@class='fa fa-pencil-square-o']").click();
					
//			findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr/td[8]/a[6]/i").click();
				

				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				// Xpath To Save and waypoint screen
				
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[4]/button[2]").click();
				
				
				//XPath for to select the draw button
				
				findElementByXPath(driver,"//*[@id=\"map\"]/div[3]/div[1]/div[3]/div[1]/div/a").click();
				
				clickOn_PolygridScreen() ;
				
				
			//Xpath to click Finish option on drawer button.
				findElementByXPath(driver,"//*[@id=\"map\"]/div[3]/div[1]/div[3]/div[1]/ul/li[1]/a").click();
				
				
				 //Xpath for side menu bar
				
				findElementByXPath(driver,"//*[@id=\"menubutton\"]/i").click();		
				
		       //XPath for Saving the mission from Waypoint screen
				
				
				WebDriverWait Wait_for_save_button=new WebDriverWait (driver,30);
				Wait_for_save_button.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@class='btn btn-primary btn-lg btnMargin au-target']")));
				
				
				
				findElementByXPath(driver,"//button[@class='btn btn-primary btn-lg btnMargin au-target']").click();
				
			
				
				
				
					
				
				
	}
	
	
	public static void Freehand_Mission(WebDriver driver) {
		
		
		
		//XPath for Side menu Flight plan button.
		findElementByXPath(driver,"//*[@href='#/missions']").click();	
		
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Xpath for adding new mission button
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[1]/div/div/button").click();
		
		
		
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//Xapth for Selecting Freehand mission div
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div/div/div[2]/div/div[2]/button/div[1]/div[2]").click();
		   
      
		
		
		String[] columnValue = excel.getAllDataOfaColumn(2, 0, 0);
		
		int Count= columnValue.length;
		
		System.out.println(Count);
		
		for(int i=0;i<columnValue.length;i++){
			System.out.println("Value of "+i+" cell is = "+columnValue[i]);
			
			
			
			
			// Xpath for mission name feild 
			findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[1]/div[1]/fieldset/input").sendKeys(columnValue[i]);
			
			
			Select select_pilot = new Select(
					findElementByXPath(driver, "//div[@class='col-md-4']//select[@class='form-control au-target']"));
			select_pilot.selectByVisibleText("Amit Ganjoo");
			
			
			Select Select_drone = new Select(
					findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[1]/div[3]/fieldset/select"));
			Select_drone.selectByVisibleText("Selenium Dutchman");
			
			Select Select_Project = new Select(
					findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[2]/div[1]/fieldset/select"));
			Select_Project.selectByVisibleText("Tower Inspection 1");
			
			Select Select_Location = new Select(
					findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[2]/div[2]/fieldset/select"));
			Select_Location.selectByVisibleText("Selenium Test Location");
			
			//xpath for date selection
			findElementByXPath(driver,"//*[@id=\"flightDate\"]").sendKeys("10052018");
			
			Select Select_Flight_type = new Select(
					findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[3]/div[2]/fieldset/select"));
			Select_Flight_type.selectByVisibleText("Test Flight");
			
			   
			
//			Select Select_Organisation = new Select(
//					findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[3]/div[3]/fieldset/select"));
//			Select_Organisation.selectByVisibleText("ANRA Technologies");
			
			
			
			
			// Xpath for setting altitude
			findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[4]/div[2]/fieldset[1]/input").sendKeys("0");
		
		    //Xpath for Selecting battery
		    
			Select Select_Battery = new Select(
					findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[3]/div/div[1]/fieldset/select"));
			Select_Battery.selectByVisibleText("DJI Battery 2");
			
			//Xpath for Selecting Sensor
			Select Select_Sensor = new Select(
					findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[3]/div/div[2]/fieldset/select"));
			Select_Sensor.selectByVisibleText("None");
			
			try {
				Thread.sleep(15000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[4]/button[1]").click();
			
			
			String CurrentUrl = driver.getCurrentUrl();
			
			WebElement  Mission_name = findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr[1]/td[1]");
			
			String Get_mission_name = Mission_name.getText();
			
			System.out.println(Get_mission_name);
			
			String loopvalue = columnValue[i];
			
			System.out.println("Loop value is = "+loopvalue);
			
//			if (CurrentUrl!=null && Get_mission_name.equalsIgnoreCase(loopvalue) && CurrentUrl.equals("https://sandbox-oss.flyanra.net/#/missions")) {
//				
				if(Get_mission_name.equalsIgnoreCase(loopvalue) ) {
//				System.out.println("value taken for mission "+columnValues[i]);
//				
//				if (Get_mission_name.equals(columnValues[i])) {
					
					System.out.println("PAssed ");
					
					WriteExcelData.Write_mission_data(2, 0, 1,"Passed",excelWorkSheetpath );
					
//				}
				
			}else {
				
				System.out.println("Failed ");
				WriteExcelData.Write_mission_data(2, 0, 1,"Failed",excelWorkSheetpath );
				
			}
			
			
		}
			
		
		
		
	}
	
	
	public static void Edit_waypoint_Freehand(WebDriver driver) {
		
		
        //XPath for Side menu Flight plan button.
		findElementByXPath(driver,"//*[@href='#/missions']").click();	

        // Xpath to clickOn search
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[1]/div/div/input").sendKeys("Freehand");
		
		// Xpath to click on edit button
		
	WebDriverWait wait=new WebDriverWait (driver,10);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@class='fa fa-pencil-square-o']")));
			
			findElementByXPath(driver,"//*[@class='fa fa-pencil-square-o']").click();
			
//	findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr/td[8]/a[6]/i").click();
		

		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		// Xpath To Save and waypoint screen
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[4]/button[2]").click();
		
		
		//XPath for to select the draw button
		
		findElementByXPath(driver,"//*[@id=\"map\"]/div[3]/div[1]/div[3]/div[1]/div/a").click();
		
		ClickOn_FreeHandScreen() ;
		
		
	   //Xpath to click Finish option on drawer button.
		findElementByXPath(driver,"//*[@id=\"map\"]/div[3]/div[1]/div[3]/div[1]/ul/li[1]/a").click();	
		
        //Xpath for side menu bar
		
		findElementByXPath(driver,"//*[@id=\"menubutton\"]/i").click();		
		
       //XPath for Saving the mission from Waypoint screen
		
		findElementByXPath(driver,"//*[@id=\"menu1\"]/mission-side-bar/section/div/form/div/div[2]/button").click();
		
}

	
	
	
	public static void LineString_Mission(WebDriver driver) {
		
		
		//XPath for Side menu Flight plan button.
		findElementByXPath(driver,"//*[@href='#/missions']").click();	
		
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Xpath for adding new mission button
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[1]/div/div/button").click();
		
		
		
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		Xpath for selection mission type LineString
	   findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div/div/div[2]/div/div[1]/button/div[1]/div[2]").click();
		
	   
	   
	   
	String[] columnValue = excel.getAllDataOfaColumn(3, 0, 0);
		
		int Count= columnValue.length;
		
		System.out.println(Count);
		
		for(int i=0;i<columnValue.length;i++){
			System.out.println("Value of "+i+" cell is = "+columnValue[i]);
			
	   
	   
	   
	   
	   
	   
//		Xpath for Mission name
	   findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[1]/div[1]/fieldset/input").sendKeys(columnValue[i]);
		
		Select select_pilot = new Select(
				findElementByXPath(driver, "//div[@class='col-md-4']//select[@class='form-control au-target']"));
		select_pilot.selectByVisibleText("Amit Ganjoo");
		
		
		Select Select_drone = new Select(
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[1]/div[3]/fieldset/select"));
		Select_drone.selectByVisibleText("Selenium Dutchman");
		
		Select Select_Project = new Select(
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[2]/div[1]/fieldset/select"));
		Select_Project.selectByVisibleText("Tower Inspection 1");
		
		Select Select_Location = new Select(
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[2]/div[2]/fieldset/select"));
		Select_Location.selectByVisibleText("Selenium Test Location");
		
		//xpath for date selection
		findElementByXPath(driver,"//*[@id=\"flightDate\"]").sendKeys("10052018");
		
		Select Select_Flight_type = new Select(
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[3]/div[2]/fieldset/select"));
		Select_Flight_type.selectByVisibleText("Test Flight");
		
		   
		
//		Select Select_Organisation = new Select(
//				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[3]/div[3]/fieldset/select"));
//		Select_Organisation.selectByVisibleText("ANRA Technologies");
		
		
		
		
		// Xpath for setting altitude
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[4]/div[2]/fieldset[1]/input").sendKeys("0");
	
	    //Xpath for Selecting battery
	    
		Select Select_Battery = new Select(
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[3]/div/div[1]/fieldset/select"));
		Select_Battery.selectByVisibleText("DJI Battery 2");
		
		//Xpath for Selecting Sensor
		Select Select_Sensor = new Select(
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[3]/div/div[2]/fieldset/select"));
		Select_Sensor.selectByVisibleText("None");
		
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		Xpath for Saving the mission 
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[4]/button[1]").click();
		

		String CurrentUrl = driver.getCurrentUrl();
		
		WebElement  Mission_name = findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr[1]/td[1]");
		
		String Get_mission_name = Mission_name.getText();
		
		System.out.println(Get_mission_name);
		
		String loopvalue = columnValue[i];
		
		System.out.println("Loop value is = "+loopvalue);
		
//		if (CurrentUrl!=null && Get_mission_name.equalsIgnoreCase(loopvalue) && CurrentUrl.equals("https://sandbox-oss.flyanra.net/#/missions")) {
//			
			if(Get_mission_name.equalsIgnoreCase(loopvalue) ) {
//			System.out.println("value taken for mission "+columnValues[i]);
//			
//			if (Get_mission_name.equals(columnValues[i])) {
				
				System.out.println("PAssed ");
				
				WriteExcelData.Write_mission_data(3, 0, 1,"Passed",excelWorkSheetpath );
				
//			}
			
		}else {
			
			System.out.println("Failed ");
			WriteExcelData.Write_mission_data(3, 0, 1,"Failed",excelWorkSheetpath );
			
		}
		}
	
	}
	
	public static void Edit_LineString_waypoint(WebDriver driver) {
		
		
		    //XPath for Side menu Flight plan button.
		      findElementByXPath(driver,"//*[@href='#/missions']").click();	

            // Xpath to clickOn search
		
		       findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[1]/div/div/input").sendKeys("Line");
		
		    // Xpath to click on edit button
		
	          WebDriverWait wait=new WebDriverWait (driver,10);
			  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@class='fa fa-pencil-square-o']")));
			
			findElementByXPath(driver,"//*[@class='fa fa-pencil-square-o']").click();
			
     //	findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr/td[8]/a[6]/i").click();
		

		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		// Xpath To Save and waypoint screen
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[4]/button[2]").click();
		
		
		//XPath for to select the draw button
		
				findElementByXPath(driver,"//*[@id=\"map\"]/div[3]/div[1]/div[3]/div[1]/div/a").click();
				
				ClickOn_LineStringScreen() ;
				
				
			//Xpath to click Finish option on drawer button.
				findElementByXPath(driver,"//*[@id=\"map\"]/div[3]/div[1]/div[3]/div[1]/ul/li[1]/a").click();	
				
				
				 //Xpath for side menu bar
				
				findElementByXPath(driver,"//*[@id=\"menubutton\"]/i").click();		
				
		       //XPath for Saving the mission from Waypoint screen
				
				findElementByXPath(driver,"//*[@id=\"menu1\"]/mission-side-bar/section/div/form/div/div[2]/button").click();	
		
		
		
	}
	
	
	public static void Tornado_Mission(WebDriver driver) {
		
		
		//XPath for Side menu Flight plan button.
		findElementByXPath(driver,"//*[@href='#/missions']").click();	
		
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Xpath for adding new mission button
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[1]/div/div/button").click();
		
		
		
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		Xpath for selection mission type Tower Tornado 
	   findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div/div/div[2]/div/div[4]/button/div[1]/div[2]").click();
		
	   
	   
	   
		String[] columnValue = excel.getAllDataOfaColumn(4, 0, 0);
			
			int Count= columnValue.length;
			
			System.out.println(Count);
	   
	   for(int i=0;i<columnValue.length;i++){
			System.out.println("Value of "+i+" cell is = "+columnValue[i]);
	   
	   
	   
//		Xpath for Mission name
	   findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[1]/div[1]/fieldset/input").sendKeys("columnValue[i]");
		
		Select select_pilot = new Select(
				findElementByXPath(driver, "//div[@class='col-md-4']//select[@class='form-control au-target']"));
		select_pilot.selectByVisibleText("Amit Ganjoo");
		
		
		Select Select_drone = new Select(
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[1]/div[3]/fieldset/select"));
		Select_drone.selectByVisibleText("Selenium Dutchman");
		
		Select Select_Project = new Select(
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[2]/div[1]/fieldset/select"));
		Select_Project.selectByVisibleText("Tower Inspection 1");
		
		Select Select_Location = new Select(
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[2]/div[2]/fieldset/select"));
		Select_Location.selectByVisibleText("Selenium Test Location");
		
		//xpath for date selection
		findElementByXPath(driver,"//*[@id=\"flightDate\"]").sendKeys("10052018");
		
		Select Select_Flight_type = new Select(
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[3]/div[2]/fieldset/select"));
		Select_Flight_type.selectByVisibleText("Test Flight");
		
		   
		
//		Select Select_Organisation = new Select(
//				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[3]/div[3]/fieldset/select"));
//		Select_Organisation.selectByVisibleText("ANRA Technologies");
		
		
		
		
		// Xpath for setting Tower Height
		
		 findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[4]/div[2]/fieldset[2]/input").clear();
		 
		 findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[4]/div[2]/fieldset[2]/input").sendKeys("500");
	

		 //	     Xpath for floor height 
		
		 findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[4]/div[3]/fieldset[2]/input").clear();
		
		 findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[4]/div[3]/fieldset[2]/input").sendKeys("100");
		 

		 //		 XPath for DroneDistance from tower.
		 
		 findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[5]/div[1]/fieldset/input").clear();
		 
		 findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[5]/div[1]/fieldset/input").sendKeys("35");
		 

		 //       XPath  For no. of division	cycle
		 
		 findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[5]/div[2]/fieldset/input").clear();
		 
		 findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[5]/div[2]/fieldset/input").sendKeys("6");
		
		 
		 
		 
		//Xpath for Rotaiton type 
	      
		Select Select_Battery = new Select(
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[5]/div[3]/fieldset/select"));
		Select_Battery.selectByVisibleText("Clockwise");
		
//		Xpath for Time capture interval
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[6]/div[3]/fieldset/input").sendKeys("5");;
		
		
		//Xpath for Selecting battery
	      
				Select Select_Battery2 = new Select(
						findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[3]/div/div[1]/fieldset/select"));
				Select_Battery.selectByVisibleText("DJI Battery 2");
		
		
		
		//Xpath for Selecting Sensor
		Select Select_Sensor = new Select(
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[3]/div/div[2]/fieldset/select"));
		Select_Sensor.selectByVisibleText("None");
		
		
		
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[4]/button[1]").click();
		
		
		
		
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String CurrentUrl = driver.getCurrentUrl();
		
		WebElement  Mission_name = findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr[1]/td[1]");
		
		String Get_mission_name = Mission_name.getText();
		
		System.out.println(Get_mission_name);
		
		String loopvalue = columnValue[i];
		
		System.out.println("Loop value is = "+loopvalue);
		
//		if (CurrentUrl!=null && Get_mission_name.equalsIgnoreCase(loopvalue) && CurrentUrl.equals("https://sandbox-oss.flyanra.net/#/missions")) {
//			
			if(Get_mission_name.equalsIgnoreCase(loopvalue) ) {
//			System.out.println("value taken for mission "+columnValues[i]);
//			
//			if (Get_mission_name.equals(columnValues[i])) {
				
				System.out.println("PAssed ");
				
				WriteExcelData.Write_mission_data(4, 0, 1,"Passed",excelWorkSheetpath );
				
//			}
			
		}else {
			
			System.out.println("Failed ");
			WriteExcelData.Write_mission_data(4, 0, 1,"Failed",excelWorkSheetpath );
			
		}
	   }
	}
	
	
	
	public static void Edit_Tornado_mission (WebDriver driver) {
		

	    //XPath for Side menu Flight plan button.
	      findElementByXPath(driver,"//*[@href='#/missions']").click();	

        // Xpath to clickOn search
	
	       findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[1]/div/div/input").sendKeys("Tornado");
	
	    // Xpath to click on edit button
	
          WebDriverWait wait=new WebDriverWait (driver,10);
		  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@class='fa fa-pencil-square-o']")));
		
		findElementByXPath(driver,"//*[@class='fa fa-pencil-square-o']").click();
		
    //	findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr/td[8]/a[6]/i").click();
	

	try {
		Thread.sleep(10000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	 	
	   // Xpath To Save and waypoint screen
	
			findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[4]/button[2]").click();
			
			
	   //XPath for to select the draw button
			
					findElementByXPath(driver,"//*[@id=\"map\"]/div[3]/div[1]/div[3]/div[1]/div/a").click();
					
					ClickOn_Tower_TornadoWaypointScreen();					
					
       //Xpath to click Finish option on drawer button.
					findElementByXPath(driver,"//*[@id=\"map\"]/div[3]/div[1]/div[3]/div[1]/ul/li[1]/a").click();	
			
       //Xpath for side menu bar
		
		findElementByXPath(driver,"//*[@id=\"menubutton\"]/i").click();		
		
       //XPath for Saving the mission from Waypoint screen
		
		findElementByXPath(driver,"//*[@id=\"menu1\"]/mission-side-bar/section/div/form/div/div[2]/button").click();
		
       	
		
		
	}
	
	
	public static void Tower_Circular_Mission(WebDriver driver ) {
		
		
		
		//XPath for Side menu Flight plan button.
				findElementByXPath(driver,"//*[@href='#/missions']").click();	
				
				
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//Xpath for adding new mission button
				
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[1]/div/div/button").click();
				
				
				
				try {
					Thread.sleep(15000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
//				Xpath for selection mission type Tower Circular mission 
			   findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div/div/div[2]/div/div[4]/button/div[1]/div[2]").click();
				
			   
			   
			   
				String[] columnValue = excel.getAllDataOfaColumn(5, 0, 0);
					
					int Count= columnValue.length;
					
					System.out.println(Count);
			   
			   for(int i=0;i<columnValue.length;i++){
					System.out.println("Value of "+i+" cell is = "+columnValue[i]);
			   
			   
			   
//				Xpath for Mission name
			   findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[1]/div[1]/fieldset/input").sendKeys("columnValue[i]");
				
				Select select_pilot = new Select(
						findElementByXPath(driver, "//div[@class='col-md-4']//select[@class='form-control au-target']"));
				select_pilot.selectByVisibleText("Amit Ganjoo");
				
				
				Select Select_drone = new Select(
						findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[1]/div[3]/fieldset/select"));
				Select_drone.selectByVisibleText("Selenium Dutchman");
				
				Select Select_Project = new Select(
						findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[2]/div[1]/fieldset/select"));
				Select_Project.selectByVisibleText("Tower Inspection 1");
				
				Select Select_Location = new Select(
						findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[2]/div[2]/fieldset/select"));
				Select_Location.selectByVisibleText("Selenium Test Location");
				
				//xpath for date selection
				findElementByXPath(driver,"//*[@id=\"flightDate\"]").sendKeys("10052018");
				
				Select Select_Flight_type = new Select(
						findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[3]/div[2]/fieldset/select"));
				Select_Flight_type.selectByVisibleText("Test Flight");
				
				   
				
//				Select Select_Organisation = new Select(
//						findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[3]/div[3]/fieldset/select"));
//				Select_Organisation.selectByVisibleText("ANRA Technologies");
				
				
				
				
				// Xpath for setting Tower Height
				
				 findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[4]/div[2]/fieldset[2]/input").clear();
				 
				 findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[4]/div[2]/fieldset[2]/input").sendKeys("500");
			

				 //	     Xpath for floor height 
				
				 findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[4]/div[3]/fieldset[2]/input").clear();
				
				 findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[4]/div[3]/fieldset[2]/input").sendKeys("100");
				 

				 //		 XPath for DroneDistance from tower.
				 
				 findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[5]/div[1]/fieldset/input").clear();
				 
				 findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[5]/div[1]/fieldset/input").sendKeys("35");
				 

				 //       XPath  For no. of division	cycle
				 
				 findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[5]/div[2]/fieldset/input").clear();
				 
				 findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[5]/div[2]/fieldset/input").sendKeys("6");
				
				 
				 
				 
				//Xpath for Rotaiton type 
			      
				Select Select_Battery = new Select(
						findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[5]/div[3]/fieldset/select"));
				Select_Battery.selectByVisibleText("Clockwise");
				
//				Xpath for Time capture interval
				
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[6]/div[3]/fieldset/input").sendKeys("5");;
				
				
				//Xpath for Selecting battery
			      
						Select Select_Battery2 = new Select(
								findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[3]/div/div[1]/fieldset/select"));
						Select_Battery.selectByVisibleText("DJI Battery 2");
				
				
				
				//Xpath for Selecting Sensor
				Select Select_Sensor = new Select(
						findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[3]/div/div[2]/fieldset/select"));
				Select_Sensor.selectByVisibleText("None");
				
				
				
				
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[4]/button[1]").click();
				
				
				
				
				try {
					Thread.sleep(15000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				String CurrentUrl = driver.getCurrentUrl();
				
				WebElement  Mission_name = findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr[1]/td[1]");
				
				String Get_mission_name = Mission_name.getText();
				
				System.out.println(Get_mission_name);
				
				String loopvalue = columnValue[i];
				
				System.out.println("Loop value is = "+loopvalue);
				
//				if (CurrentUrl!=null && Get_mission_name.equalsIgnoreCase(loopvalue) && CurrentUrl.equals("https://sandbox-oss.flyanra.net/#/missions")) {
//					
					if(Get_mission_name.equalsIgnoreCase(loopvalue) ) {
//					System.out.println("value taken for mission "+columnValues[i]);
//					
//					if (Get_mission_name.equals(columnValues[i])) {
						
						System.out.println("PAssed ");
						
						WriteExcelData.Write_mission_data(5, 0, 1,"Passed",excelWorkSheetpath );
						
//					}
					
				}else {
					
					System.out.println("Failed ");
					WriteExcelData.Write_mission_data(5, 0, 1,"Failed",excelWorkSheetpath );
					
				}
			   }
			
	}
	
	
public static void Edit_Cricular_mission (WebDriver driver) {
		

	    //XPath for Side menu Flight plan button.
	      findElementByXPath(driver,"//*[@href='#/missions']").click();	

        // Xpath to clickOn search
	
	       findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[1]/div/div/input").sendKeys("Inspection");
	
	    // Xpath to click on edit button
	
          WebDriverWait wait=new WebDriverWait (driver,10);
		  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@class='fa fa-pencil-square-o']")));
		
		findElementByXPath(driver,"//*[@class='fa fa-pencil-square-o']").click();
		
    //	findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr/td[8]/a[6]/i").click();
	

	try {
		Thread.sleep(10000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	 	
	   // Xpath To Save and waypoint screen
	
			findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[4]/button[2]").click();
			
			
	   //XPath for to select the draw button
			
					findElementByXPath(driver,"//*[@id=\"map\"]/div[3]/div[1]/div[3]/div[1]/div/a").click();
					
					ClickOn_Tower_CircularWaypointScreen();		
					
					
				
					
       //Xpath to click Finish option on drawer button.
					findElementByXPath(driver,"//*[@id=\"map\"]/div[3]/div[1]/div[3]/div[1]/ul/li[1]/a").click();	
					
					
					try {
		    			Thread.sleep(10000);
		    		} catch (InterruptedException e) {
		    			// TODO Auto-generated catch block
		    			e.printStackTrace();
		    		}
		    			
			
       //Xpath for side menu bar
		
		findElementByXPath(driver,"//*[@id=\"menubutton\"]/i").click();		
		
       //XPath for Saving the mission from Waypoint screen
		
		findElementByXPath(driver,"//*[@id=\"menu1\"]/mission-side-bar/section/div/form/div/div[2]/button").click();
}
	
	

  public static void Facade_mission(WebDriver driver) {
	  
	  
	//XPath for Side menu Flight plan button.
		findElementByXPath(driver,"//*[@href='#/missions']").click();	
		
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Xpath for adding new mission button
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[1]/div/div/button").click();
		
		
		
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		Xpath for selection mission type Facade mission 
	   findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div/div/div[2]/div/div[8]/button/div[1]/div[2]").click();
		
//	  
	   String[] columnValue = excel.getAllDataOfaColumn(6, 0, 0);
//		
		int Count= columnValue.length;
//		
		System.out.println("value vhhjgh"+Count);
		
		for(int i=0;i<columnValue.length;i++){
			System.out.println("Value of "+i+" cell is = "+columnValue[i]);
			
	   
	   
	   
	 //xpath For Mission Name
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[1]/div[1]/fieldset/input").sendKeys(columnValue[i]);
		
		//Xpath for Pilot for selecting a prticular pilot from the Drop down listing
		
		Select select_pilot = new Select(
				findElementByXPath(driver, "//div[@class='col-md-4']//select[@class='form-control au-target']"));
		select_pilot.selectByVisibleText("Amit Ganjoo");
		
		Select Select_drone = new Select(
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[1]/div[3]/fieldset/select"));
		Select_drone.selectByVisibleText("Selenium Dutchman");
		
		Select Select_Project = new Select(
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[2]/div[1]/fieldset/select"));
		Select_Project.selectByVisibleText("Tower Inspection 1");
		
		Select Select_Location = new Select(
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[2]/div[2]/fieldset/select"));
		Select_Location.selectByVisibleText("Selenium Test Location");
		
		//xpath for date selection
		findElementByXPath(driver,"//*[@id=\"flightDate\"]").sendKeys("10052018");
		
		Select Select_Flight_type = new Select(
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[3]/div[2]/fieldset/select"));
		Select_Flight_type.selectByVisibleText("Test Flight");
		
		   
		
//		Select Select_Organisation = new Select(
//				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[3]/div[3]/fieldset/select"));
//		Select_Organisation.selectByVisibleText("ANRA Technologies");
		
		
		
		
		// Xpath for setting facade
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[4]/div[2]/fieldset[2]/input").clear();
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[4]/div[2]/fieldset[2]/input").sendKeys("500");
	    
		
//		Xpath for setting tower height
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[4]/div[3]/fieldset[2]/input").clear();
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[4]/div[3]/fieldset[2]/input").sendKeys("150");
		
		//Xpath for Selecting battery
	    
		Select Select_Battery = new Select(
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[3]/div/div[1]/fieldset/select"));
		Select_Battery.selectByVisibleText("DJI Battery 2");
		
		//Xpath for Selecting Sensor
		Select Select_Sensor = new Select(
				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[3]/div/div[2]/fieldset/select"));
		Select_Sensor.selectByVisibleText("None");
		
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[4]/button[1]").click();
		
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String CurrentUrl = driver.getCurrentUrl();
		
		WebElement  Mission_name = findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr[1]/td[1]");
		
		String Get_mission_name = Mission_name.getText();
		
		System.out.println(Get_mission_name);
		
		String loopvalue = columnValue[i];
		
		System.out.println("Loop value is = "+loopvalue);
		
//		if (CurrentUrl!=null && Get_mission_name.equalsIgnoreCase(loopvalue) && CurrentUrl.equals("https://sandbox-oss.flyanra.net/#/missions")) {
//			
			if(Get_mission_name.equalsIgnoreCase(loopvalue) ) {
//			System.out.println("value taken for mission "+columnValues[i]);
//			
//			if (Get_mission_name.equals(columnValues[i])) {
				
				System.out.println("PAssed ");
				
				WriteExcelData.Write_mission_data(6, 0, 1,"Passed",excelWorkSheetpath );
				
//			}
			
		}else {
			
			System.out.println("Failed ");
			WriteExcelData.Write_mission_data(6, 0, 1,"Failed",excelWorkSheetpath );
			
		}
	   
		
		}
	  
  }

  
  
     public static void Edit_Facade_waypointScreen(WebDriver driver) {
    	 
    	 
    	 
    			

    		    //XPath for Side menu Flight plan button.
    		      findElementByXPath(driver,"//*[@href='#/missions']").click();	
    		      
    		      try {
    	    			Thread.sleep(10000);
    	    		} catch (InterruptedException e) {
    	    			// TODO Auto-generated catch block
    	    			e.printStackTrace();
    	    		}
    		      

    	        // Xpath to clickOn search
    		
    		       findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[1]/div/div/input").sendKeys("Facade");
    		
    		    // Xpath to click on edit button
    		
    	          WebDriverWait wait=new WebDriverWait (driver,15);
    			  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@class='fa fa-pencil-square-o']")));
    			
    			findElementByXPath(driver,"//*[@class='fa fa-pencil-square-o']").click();
    			
    	    //	findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr/td[8]/a[6]/i").click();
    		

    		try {
    			Thread.sleep(15000);
    		} catch (InterruptedException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    		
    		 	
    		   // Xpath To Save and waypoint screen
    		
    				findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[4]/button[2]").click();
    				
    				
    		   //XPath for to select the draw button
    				
    						findElementByXPath(driver,"//*[@id=\"map\"]/div[3]/div[1]/div[3]/div[1]/div/a").click();
    						
    						clickOn_facadeScreen1();	
    						
    		 //XPath for to select the draw button
    	    				
    						findElementByXPath(driver,"//*[@id=\"map\"]/div[3]/div[1]/div[3]/div[1]/div/a").click();		
    						
    						clickOn_facadeScreen2();	
						
                 //XPath for to select the draw button
    	    				
    						findElementByXPath(driver,"//*[@id=\"map\"]/div[3]/div[1]/div[3]/div[1]/div/a").click();	
    						
    						clickOn_facadeScreen3();	
    						
                   //XPath for to select the draw button
    	    				
    						findElementByXPath(driver,"//*[@id=\"map\"]/div[3]/div[1]/div[3]/div[1]/div/a").click();	
    						
    						clickOn_facadeScreen4();	
    						

    						
    	       //Xpath to click Finish option on drawer button.
    						findElementByXPath(driver,"//*[@id=\"map\"]/div[3]/div[1]/div[3]/div[1]/ul/li[1]/a").click();	
    				
    						

    			    		try {
    			    			Thread.sleep(10000);
    			    		} catch (InterruptedException e) {
    			    			// TODO Auto-generated catch block
    			    			e.printStackTrace();
    			    		}
    			    			
    						
    						
    	       //Xpath for side menu bar
    			
    			findElementByXPath(driver,"//*[@id=\"menubutton\"]/i").click();		
    			
    	       //XPath for Saving the mission from Waypoint screen
    			
    			findElementByXPath(driver,"//*[@id=\"menu1\"]/mission-side-bar/section/div/form/div/div[2]/button").click();
    	}
    		 
    	 
    	 
    	 
    	 
    public static void Guyed_wire_mission(WebDriver driver) {
    	
    	
    	
    		
    		//XPath for Side menu Flight plan button.
    		findElementByXPath(driver,"//*[@href='#/missions']").click();	
    		
    		
    		try {
    			Thread.sleep(10000);
    		} catch (InterruptedException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    		
    		//Xpath for adding new mission button
    		
    		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[1]/div/div/button").click();
    		
    		
    		
    		try {
    			Thread.sleep(10000);
    		} catch (InterruptedException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    		
    		//Xpath To select polygrid mission Div
    		findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div/div/div[2]/div/div[6]/button/div[1]/div[2]").click();
    		
    		// read operation from excel
    		    
//    		String Mission_Name = excel.getData(1,0,0);
    		
    		
    		
    		
    		
    		
           //	Running Code for that has been commented
    		
//    		String[] rowValues = excel.getAllDataOfaRow(1, 0, 0);
//    		String[] columnValues = excel.getAllDataOfaColumn(1, 0, 0);
    		
//    		for(int i=0;i<rowValues.length;i++){
//    			System.out.println("Value of "+i+" cell is = "+rowValues[i]);
//    		}
    		
//    		for(int i=0;i<columnValues.length;i++){
//    			System.out.println("Value of "+i+" cell is = "+columnValues[i]);
    			
//    		}
    		
    		
    		
    		
           // Commented area of code to be uncommented
    		
    		
    		
    		
    		
    		
    		//String Mission_Name2 = excel.getData(1,1,0);
    		
    		
    		
//    		System.out.println(Mission_Name);
//    		System.out.println(Mission_Name2);
    		
    		String[] columnValues = excel.getAllDataOfaColumn(7, 0, 0);
    		
    		
    		
    		
    		for(int i=0;i<columnValues.length;i++){
    			System.out.println("Value of "+i+" cell is = "+columnValues[i]);
    			
    			
    			//xpath For Mission Name
    			
    			findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[1]/div[1]/fieldset/input").sendKeys(columnValues[i]);
    			
    			//Xpath for Pilot for selecting a prticular pilot from the Drop down listing
    			
    			Select select_pilot = new Select(
    					findElementByXPath(driver, "//div[@class='col-md-4']//select[@class='form-control au-target']"));
    			select_pilot.selectByVisibleText("Amit Ganjoo");
    			
    			Select Select_drone = new Select(
    					findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[1]/div[3]/fieldset/select"));
    			Select_drone.selectByVisibleText("Selenium Dutchman");
    			
    			Select Select_Project = new Select(
    					findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[2]/div[1]/fieldset/select"));
    			Select_Project.selectByVisibleText("Tower Inspection 1");
    			
    			Select Select_Location = new Select(
    					findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[2]/div[2]/fieldset/select"));
    			Select_Location.selectByVisibleText("Selenium Test Location");
    			
    			//xpath for date selection
    			findElementByXPath(driver,"//*[@id=\"flightDate\"]").sendKeys("10052018");
    			
    			Select Select_Flight_type = new Select(
    					findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[3]/div[2]/fieldset/select"));
    			Select_Flight_type.selectByVisibleText("Test Flight");
    			
    			   
    			
//    			Select Select_Organisation = new Select(
//    					findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[3]/div[3]/fieldset/select"));
//    			Select_Organisation.selectByVisibleText("ANRA Technologies");
    			
    			
    			
    			
    			// Xpath for setting altitude
    			findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[2]/div[4]/div[2]/fieldset[1]/input").sendKeys("0");
    		
    		    //Xpath for Selecting battery
    		    
    			Select Select_Battery = new Select(
    					findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[3]/div/div[1]/fieldset/select"));
    			Select_Battery.selectByVisibleText("DJI Battery 2");
    			
    			//Xpath for Selecting Sensor
    			Select Select_Sensor = new Select(
    					findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[3]/div/div[2]/fieldset/select"));
    			Select_Sensor.selectByVisibleText("None");
    			
    			try {
    				Thread.sleep(15000);
    			} catch (InterruptedException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    			
    			
    			
    			findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[3]/div/div/form/div/div[4]/button[1]").click();
    			
    			
    			String CurrentUrl = driver.getCurrentUrl();
    			
    			WebElement  Mission_name = findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr[1]/td[1]");
    			
    			String Get_mission_name = Mission_name.getText();
    			
    			System.out.println(Get_mission_name);
    			
    			String loopvalue = columnValues[i];
    			
    			System.out.println("Loop value is = "+loopvalue);
    			
//    			if (CurrentUrl!=null && Get_mission_name.equalsIgnoreCase(loopvalue) && CurrentUrl.equals("https://sandbox-oss.flyanra.net/#/missions")) {
//    				
    				if(Get_mission_name.equalsIgnoreCase(loopvalue) ) {
//    				System.out.println("value taken for mission "+columnValues[i]);
//    				
//    				if (Get_mission_name.equals(columnValues[i])) {
    					
    					System.out.println("PAssed ");
    					
    					WriteExcelData.Write_mission_data(7, 0, 1,"Passed",excelWorkSheetpath );
    					
//    				}
    				
    			}else {
    				
    				System.out.println("Failed ");
    				WriteExcelData.Write_mission_data(7, 0, 1,"Failed",excelWorkSheetpath );
    				
    			}
    			
    			
    			
    			
    			
    		}
    			
 
    	
    }	 
    	 
    	 
    	 
    	 
    	 
    	 
    	 
 

	
	public static  WebElement findElementByXPath(WebDriver driver, String xpath) {
		return driver.findElement(By.xpath(xpath));
	}
	
	
	
		

	
	
	
	public static java.awt.Point getMousePosition() {
		 System.out.println("(" + MouseInfo.getPointerInfo().getLocation().x + 
	              ", " + 
	              MouseInfo.getPointerInfo().getLocation().y + ")");
		 return MouseInfo.getPointerInfo().getLocation();
	}
	
	
	
	
	

	
	
		public static void clickOn_PolygridScreen() {
	 
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			java.awt.Point centerPoint = new java.awt.Point();
					
//					centerPoint.x = screenSize.height/2;
//					centerPoint.y = screenSize.width/2;
					
//					centerPoint.x =650;
//					centerPoint.y = 375;
					centerPoint = MouseInfo.getPointerInfo().getLocation();
					
			try {
				Robot robot = new Robot();
				robot.mouseMove(centerPoint.getLocation().x-100, centerPoint.getLocation().y-100);
				robot.mousePress( InputEvent.BUTTON1_MASK );
				robot.mouseRelease( InputEvent.BUTTON1_MASK );
				
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				robot.mouseMove(centerPoint.getLocation().x+100, centerPoint.getLocation().y - 100);
				robot.mousePress( InputEvent.BUTTON1_MASK );
				robot.mouseRelease( InputEvent.BUTTON1_MASK );
				
				
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				robot.mouseMove(centerPoint.getLocation().x + 100, centerPoint.getLocation().y + 100);
				robot.mousePress( InputEvent.BUTTON1_MASK );
				robot.mouseRelease( InputEvent.BUTTON1_MASK );
				
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
				robot.mouseMove(centerPoint.getLocation().x-100, centerPoint.getLocation().y+100);
				robot.mousePress( InputEvent.BUTTON1_MASK );
				robot.mouseRelease( InputEvent.BUTTON1_MASK );
				

			} catch (AWTException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
		
		
		
		public static void ClickOn_FreeHandScreen() {
			 
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			java.awt.Point centerPoint = new java.awt.Point();
					
//					centerPoint.x = screenSize.height/2;
//					centerPoint.y = screenSize.width/2;
					
//					centerPoint.x =650;
//					centerPoint.y = 375;
					centerPoint = MouseInfo.getPointerInfo().getLocation();
					
			try {
				Robot robot = new Robot();
				robot.mouseMove(centerPoint.getLocation().x-100, centerPoint.getLocation().y-100);
				robot.mousePress( InputEvent.BUTTON1_MASK );
				robot.mouseRelease( InputEvent.BUTTON1_MASK );
				
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				robot.mouseMove(centerPoint.getLocation().x+100, centerPoint.getLocation().y - 100);
				robot.mousePress( InputEvent.BUTTON1_MASK );
				robot.mouseRelease( InputEvent.BUTTON1_MASK );
				
				
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				robot.mouseMove(centerPoint.getLocation().x + 100, centerPoint.getLocation().y + 100);
				robot.mousePress( InputEvent.BUTTON1_MASK );
				robot.mouseRelease( InputEvent.BUTTON1_MASK );
				
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
				robot.mouseMove(centerPoint.getLocation().x-100, centerPoint.getLocation().y+100);
				robot.mousePress( InputEvent.BUTTON1_MASK );
				robot.mouseRelease( InputEvent.BUTTON1_MASK );
				
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				robot.mouseMove(centerPoint.getLocation().x-60, centerPoint.getLocation().y-60);
				robot.mousePress( InputEvent.BUTTON1_MASK );
				robot.mouseRelease( InputEvent.BUTTON1_MASK );
				
				

			} catch (AWTException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
			
			
			
	}
		
		
		
		public static void ClickOn_LineStringScreen() {
			
			
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			java.awt.Point centerPoint = new java.awt.Point();
					
//					centerPoint.x = screenSize.height/2;
//					centerPoint.y = screenSize.width/2;
					
//					centerPoint.x =650;
//					centerPoint.y = 375;
					centerPoint = MouseInfo.getPointerInfo().getLocation();
					
			try {
				Robot robot = new Robot();
				robot.mouseMove(centerPoint.getLocation().x-100, centerPoint.getLocation().y-100);
				robot.mousePress( InputEvent.BUTTON1_MASK );
				robot.mouseRelease( InputEvent.BUTTON1_MASK );
				
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				robot.mouseMove(centerPoint.getLocation().x+100, centerPoint.getLocation().y - 100);
				robot.mousePress( InputEvent.BUTTON1_MASK );
				robot.mouseRelease( InputEvent.BUTTON1_MASK );
				
				
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				robot.mouseMove(centerPoint.getLocation().x + 100, centerPoint.getLocation().y + 100);
				robot.mousePress( InputEvent.BUTTON1_MASK );
				robot.mouseRelease( InputEvent.BUTTON1_MASK );
				
				
				

			} catch (AWTException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
		}
		
		
public static void ClickOn_Tower_TornadoWaypointScreen() {
			
			
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			java.awt.Point centerPoint = new java.awt.Point();
					
//					centerPoint.x = screenSize.height/2;
//					centerPoint.y = screenSize.width/2;
					
//					centerPoint.x =650;
//					centerPoint.y = 375;
					centerPoint = MouseInfo.getPointerInfo().getLocation();
					
			try {
				Robot robot = new Robot();
				robot.mouseMove(centerPoint.getLocation().x-10, centerPoint.getLocation().y-10);
				robot.mousePress( InputEvent.BUTTON1_MASK );
				robot.mouseRelease( InputEvent.BUTTON1_MASK );

			} catch (AWTException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		}
			
		
public static void ClickOn_Tower_CircularWaypointScreen() {
	
	
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	java.awt.Point centerPoint = new java.awt.Point();
			
//			centerPoint.x = screenSize.height/2;
//			centerPoint.y = screenSize.width/2;
			
//			centerPoint.x =650;
//			centerPoint.y = 375;
			centerPoint = MouseInfo.getPointerInfo().getLocation();
			
	try {
		Robot robot = new Robot();
		robot.mouseMove(centerPoint.getLocation().x-10, centerPoint.getLocation().y-10);
		robot.mousePress( InputEvent.BUTTON1_MASK );
		robot.mouseRelease( InputEvent.BUTTON1_MASK );

	} catch (AWTException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

}	


public static void clickOn_facadeScreen1() {
	 
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	java.awt.Point centerPoint = new java.awt.Point();
			
//			centerPoint.x = screenSize.height/2;
//			centerPoint.y = screenSize.width/2;
			
//			centerPoint.x =650;
//			centerPoint.y = 375;
			centerPoint = MouseInfo.getPointerInfo().getLocation();
			
	try {
		Robot robot = new Robot();
		robot.mouseMove(centerPoint.getLocation().x-100, centerPoint.getLocation().y-100);
		robot.mousePress( InputEvent.BUTTON1_MASK );
		robot.mouseRelease( InputEvent.BUTTON1_MASK );
		
		

	} catch (AWTException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}



public static void clickOn_facadeScreen2() {
	 
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	java.awt.Point centerPoint = new java.awt.Point();
			
//			centerPoint.x = screenSize.height/2;
//			centerPoint.y = screenSize.width/2;
			
//			centerPoint.x =650;
//			centerPoint.y = 375;
			centerPoint = MouseInfo.getPointerInfo().getLocation();
			
	try {
		Robot robot = new Robot();
		robot.mouseMove(centerPoint.getLocation().x+100, centerPoint.getLocation().y-100);
		robot.mousePress( InputEvent.BUTTON1_MASK );
		robot.mouseRelease( InputEvent.BUTTON1_MASK );
		
		

	} catch (AWTException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}


public static void clickOn_facadeScreen3() {
	 
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	java.awt.Point centerPoint = new java.awt.Point();
			
//			centerPoint.x = screenSize.height/2;
//			centerPoint.y = screenSize.width/2;
			
//			centerPoint.x =650;
//			centerPoint.y = 375;
			centerPoint = MouseInfo.getPointerInfo().getLocation();
			
	try {
		Robot robot = new Robot();
		robot.mouseMove(centerPoint.getLocation().x+100, centerPoint.getLocation().y+100);
		robot.mousePress( InputEvent.BUTTON1_MASK );
		robot.mouseRelease( InputEvent.BUTTON1_MASK );
		
		

	} catch (AWTException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}


public static void clickOn_facadeScreen4() {
	 
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	java.awt.Point centerPoint = new java.awt.Point();
			
//			centerPoint.x = screenSize.height/2;
//			centerPoint.y = screenSize.width/2;
			
//			centerPoint.x =650;
//			centerPoint.y = 375;
			centerPoint = MouseInfo.getPointerInfo().getLocation();
			
	try {
		Robot robot = new Robot();
		robot.mouseMove(centerPoint.getLocation().x-100, centerPoint.getLocation().y+100);
		robot.mousePress( InputEvent.BUTTON1_MASK );
		robot.mouseRelease( InputEvent.BUTTON1_MASK );
		
		

	} catch (AWTException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}



  public static void ClickforAnnotation() {
	  
	  
	  Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		java.awt.Point centerPoint = new java.awt.Point();
				
//				centerPoint.x = screenSize.height/2;
//				centerPoint.y = screenSize.width/2;
				
//				centerPoint.x =650;
//				centerPoint.y = 375;
				centerPoint = MouseInfo.getPointerInfo().getLocation();
				
		try {
			Robot robot = new Robot();
			robot.mouseMove(centerPoint.getLocation().x-100, centerPoint.getLocation().y-100);
			robot.mousePress( InputEvent.BUTTON1_MASK );
			robot.mouseRelease( InputEvent.BUTTON1_MASK );
			
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	

			robot.mouseMove(centerPoint.getLocation().x + 100, centerPoint.getLocation().y + 50);
			robot.mousePress( InputEvent.BUTTON1_MASK );
			robot.mouseRelease( InputEvent.BUTTON1_MASK );
			
	  
		}catch (AWTException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	  
		
	  
  }











		
}
