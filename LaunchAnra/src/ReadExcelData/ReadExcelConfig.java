package ReadExcelData;


import java.awt.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

   

public class ReadExcelConfig {
	
	static XSSFWorkbook wb;	
	
	static XSSFSheet sheet1;
	public  ReadExcelConfig (String excelpath) {
		// TODO Auto-generated method stub
        try {
			File src =new File(excelpath);
			
			FileInputStream fis =new  FileInputStream(src);
			
			 wb =new XSSFWorkbook(fis);
			 
			
			 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getLocalizedMessage());
		}
		
       }	
	
	
	public  static String  getData (int sheetNumber,int row,int coloumn) 
	
	{  
		XSSFSheet sheet1 = wb.getSheetAt(sheetNumber);
		
		int total_row_count=  sheet1.getLastRowNum();
		
		 int lstcoloum= sheet1.getRow(row).getLastCellNum();
		 
		System.out.println("total row count iss"+total_row_count);
		
//		System.out.println("total no of coloumn"+lstcoloum);
		
		String data = "";

			data = sheet1.getRow(row).getCell(coloumn).getStringCellValue();

		return data;
	}
	

	public  static String[]  getAllDataOfaRow (int sheetNumber,int row,int coloumn) 
	
	{  
		XSSFSheet sheet1 = wb.getSheetAt(sheetNumber);
		
		int total_row_count=  sheet1.getLastRowNum();
		
		 int lstcoloum= sheet1.getRow(row).getLastCellNum();
		 
		System.out.println("total row count iss"+total_row_count);
		System.out.println("total no of coloumn"+lstcoloum);
		String[] data = new String[lstcoloum];

		for( int r=0;r<lstcoloum;r++) {
			data[r] = sheet1.getRow(row).getCell(r).getStringCellValue();
		}
		return data;
	}
	
	public  static String[]  getAllDataOfaColumn (int sheetNumber,int row,int coloumn) 
	
	{  
		XSSFSheet sheet1 = wb.getSheetAt(sheetNumber);
		
		int total_row_count=  sheet1.getLastRowNum();
		
		 int lstcoloum= sheet1.getRow(row).getLastCellNum();
		 
		System.out.println("total row count iss"+total_row_count);
		System.out.println("total no of coloumn"+lstcoloum);
		String[] data =new String[total_row_count];

		for( int r=0;r<total_row_count;r++) {
			data[r] = sheet1.getRow(r).getCell(coloumn).getStringCellValue();
		}
		return data;
	}
	
	
		
		
	
	
} 


  
