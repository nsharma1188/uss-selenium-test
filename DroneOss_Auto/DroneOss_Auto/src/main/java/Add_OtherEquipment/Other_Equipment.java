package Add_OtherEquipment;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class Other_Equipment {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	
public static void create_equipment(WebDriver driver) {
	
	
	
	
	
	

	try {
		Thread.sleep(2000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	findElementByXPath(driver,"//a[@href='#/equipments']").click();
	
	
	
	 try {
		  
//			int SensorListcount= findElementsByXPath(driver,"//table[@class='table table-striped table-bordered dataTable no-footer au-target']/tbody/tr").size();
//			 
//			System.out.println(">>>>>>countof User list"+ SensorListcount);
		
			
		 WebElement GetListing_Table =  findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr");
			
		  List <WebElement> Equipment_existance = GetListing_Table.findElements(By.xpath("//*[contains(text(),'Automatic Equipment')]"));
			
			
		  int OtherEquipment_count  = Equipment_existance.size();
			
			 System.out.println("Count Of Other Equipment  List  "+OtherEquipment_count);
		 
		 
		 
		 
//			
				
			if (OtherEquipment_count==0) {
	
	             System.out.println("Creating Auto Equipment For Automated Drone");
	             
	             findElementByXPath(driver,"//button[@click.delegate='add()']").click();
	             findElementByXPath(driver,"//input[@value.bind='equipment.Name & validate']").sendKeys("Automatic Equipment");
	             findElementByXPath(driver,"//input[@value.bind='equipment.PurchaseDate & validate']").sendKeys("03-06-2019");
	             findElementByXPath(driver,"//input[@value.bind='equipment.SerialNumber & validate']").sendKeys("214565");
	            
	             Select associated_drone = new Select (findElementByXPath(driver,"//select[@value.bind='equipment.DroneId & validate']"));
	             associated_drone.selectByVisibleText("Automated Drone");
	            
	             findElementByXPath(driver,"//span[@class='ui-switch-handle']").click();
	             
	             Select associated_equipment_type = new Select (findElementByXPath(driver,"//select[@value.bind='equipment.TypeId & validate']"));
	             associated_equipment_type.selectByVisibleText("Camera");
	             
	             
	             findElementByXPath(driver,"//input[@value.bind='equipment.Weight & validate']").sendKeys("1");
	             findElementByXPath(driver,"//input[@value.bind='equipment.FirmwareVersion & validate']").sendKeys("5548");
	             findElementByXPath(driver,"//input[@value.bind='equipment.HardwareVersion & validate']").sendKeys("48001");
//	             findElementByXPath(driver,"//input[@value.bind='battery.InitialCycleCount & validate']").clear();
//	             findElementByXPath(driver,"//input[@value.bind='battery.InitialCycleCount & validate']").sendKeys("1");  
//	             findElementByXPath(driver,"//input[@value.bind='battery.FirmwareVersion & validate']").sendKeys("15478");
//	             findElementByXPath(driver,"//input[@value.bind='battery.HardwareVersion & validate']").sendKeys("2548");
	             findElementByXPath(driver,"//textarea[@value.bind='equipment.Notes']").sendKeys("Notes");
	             findElementByXPath(driver,"//button[@click.delegate='saveItem()']").click();
	         
	             
	
			}
			else if  (OtherEquipment_count>0){
				
				
				System.out.println(">>>>>>>>>  OtherEquipment already created ");
			}
			
			
			
	 
	 
	 
	 
	 }catch (NoSuchElementException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
	
	
	
	
	
	
}	
	
public static  WebElement findElementByXPath(WebDriver driver, String xpath) {
	return driver.findElement(By.xpath(xpath));
}


public static  List <WebElement> findElementsByXPath(WebDriver driver, String xpath) {
	return driver.findElements(By.xpath(xpath));
}



}	
	
	

