package Launch_Test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Read_Write_excel.ReadExcelConfig;
import Read_Write_excel.WriteExcelData;

public class Login_dashboard {
	
	private static ReadExcelConfig excel ;
	private static String excelWorkSheetpath = "D:\\SeleniumProject\\Excel\\Book.xlsx";
	
	public static void loginFunctionality(WebDriver driver) {
		//Below code for login to Anra Dashboard
		if(driver!=null){
		System.out.println("Driver is not Null");
		}else {
			System.out.println("Driver is Null");
		}
		
		driver.get("https://dev-oss.flyanra.net/#/login");
		
		// https://dashboard.flyanra.com/web/#/login
		driver.manage().window().maximize();

		driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
		
		// thread.sleep applied for 
		
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				

				findElementByXPath(driver, "//*[@id='chkTermsCondition']").click();
				findElementByXPath(driver, "//*[@id='chkPrivacyPolicy']").click();

				findElementByXPath(driver, "//*[@id='btnAgree']").click();
				
				

				excel= new ReadExcelConfig(excelWorkSheetpath);
				String username1 = excel.getData(0,0,0);
				String password1 = excel.getData(0,0,1);
				
				System.out.println("User : "+username1);
				System.out.println("Pass : "+password1);

				findElementByXPath(driver, "//*[@id='email']").sendKeys(username1);
				findElementByXPath(driver, "//*[@id='password']").sendKeys(password1);
				findElementByXPath(driver, "//button[@type='submit']").click();
				
				String CurrentUrl = driver.getCurrentUrl();
				
				
				if(CurrentUrl.equals("https://dev-oss.flyanra.net/#/login"))
				{  
					// https://dashboard.flyanra.com/web/#/login
					
					System.out.println("Passed");
			
//					Write EXcel PAssed 
					
					WriteExcelData.writedata(0, 0, 0,"Passed",excelWorkSheetpath );
				
				}
				else
			        {
					System.out.println("Failed");
					
//					Write EXcel Failed 
					WriteExcelData.writedata(0, 0, 0, "Failed",excelWorkSheetpath );
				}
				
				
		
	}

	public static  WebElement findElementByXPath(WebDriver driver, String xpath) {
		return driver.findElement(By.xpath(xpath));
	}
	
	
	
}
