package Mission_Planning;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import GetLocal_datetime.Get_DateTime;

public class Mission_Polygrid {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
public static void create_polygrid(WebDriver driver) {
	
	
	findElementByXPath(driver,"//a[@href='#/missions']").click();
	
	
	
	try {
		  
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	//		int SensorListcount= findElementsByXPath(driver,"//table[@class='table table-striped table-bordered dataTable no-footer au-target']/tbody").size();
	//		 
	//		System.out.println(">>>>>>countof User list"+ SensorListcount);
	
		
		
		
		int countoflist_intable = findElementsByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr").size();
	
	System.out.println(">>>>>>countof User list"+ countoflist_intable);
	
	if (countoflist_intable <= 0) {

		  System.out.println("Creating Automated Polygrid Mission");
	        
	        findElementByXPath(driver,"//button[@click.delegate='add()']").click();
	        findElementByXPath(driver,"//button[@click.delegate='selectMissionType(5)']").click();
	        
	        findElementByXPath(driver,"//input[@value.bind='mission.Name & validate']").sendKeys("Automated Polygrid Test");
	      
	        
	        String date = Get_DateTime.GetDatetime();
	        
	        System.out.println("current date is "+date);
	        
	        
	        
	        
	        
	        
	        findElementByXPath(driver,"//input[@value.bind='mission.FlightDate & validate']").sendKeys(date);
	        
	        
	        System.out.println(">>>>Slecting Project");
	      
	        Select select_project = new Select (findElementByXPath(driver,"//select[@value.bind='mission.ProjectId & validate']"));
	        select_project.selectByVisibleText("KGF");
	      
	        
	        Select select_Crew= new Select (findElementByXPath(driver,"//select[@change.delegate='setselectedOption($event)']"));
	        select_Crew.selectByVisibleText("Automated VisualObserver");
	        
	        
	        
	        System.out.println("Selecting Operation Location");
	        
	        Select associated_Location = new Select (findElementByXPath(driver,"//select[@value.bind='mission.LocationId & validate']"));
	        associated_Location.selectByVisibleText("Automated ANRA Loc");
	        
	        
	        
	        
	        System.out.println(">>>>Slecting Drone");  
    
	       Select associated_drone = new Select (findElementByXPath(driver,"//select[@value.bind='mission.DroneId & validate']"));
		      associated_drone.selectByVisibleText("Automated Drone");
		      
//		      Select project_location = new Select (findElementByXPath(driver,"//select[@value.bind='mission.LocationId & validate']"));
//		      project_location.selectByVisibleText("Automated Drone");
	      
		      
		      Select Flight_type = new Select (findElementByXPath(driver,"//select[@value.bind='mission.FlightTypeId & validate']"));
		      Flight_type.selectByVisibleText("Commerical - Other");
		      
		      
//		      Select battery_associated_drone = new Select (findElementByXPath(driver,"//select[@value.bind='isBatterySelected(item.BatteryId)']"));
//		      battery_associated_drone.selectByVisibleText("Automatic Battery");
		      
		      Select associated_sensor_type = new Select (findElementByXPath(driver,"//select[@change.delegate='setselectedSensorOption($event)']"));
			     associated_sensor_type.selectByVisibleText("Automatic Sensor");
			     
			     Select associated_battery_type = new Select(findElementByXPath(driver,"//select[@change.delegate='setselectedBatteryOption($event)']"));
			     associated_battery_type.selectByVisibleText("Automatic Battery");
			     
			     Select associated_Equipment_type = new Select(findElementByXPath(driver,"//select[@change.delegate='setselectedEquipmentOption($event)']"));
			     associated_Equipment_type.selectByVisibleText("Automatic Equipment");
			     
		      
//		      findElementByXPath(driver,"//input[@value.bind='mission.Altitude & validate']").sendKeys("0");
	     
	   
	     
	      findElementByXPath(driver,"//span[@class='ui-switch-handle']").click();
	      
	    
	     try {
	    	 Thread.sleep(3000);
			findElementByXPath(driver,"//input[@value.bind='mission.Altitude & validate']").clear();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	     
	     findElementByXPath(driver,"//input[@value.bind='mission.Altitude & validate']").sendKeys("250");
	     
	     
	      
//	      findElementByXPath(driver,"//input[@value.bind='equipment.Weight & validate']").sendKeys("1");
//	     findElementByXPath(driver,"//input[@value.bind='equipment.FirmwareVersion & validate']").sendKeys("5548");
//	      findElementByXPath(driver,"//input[@value.bind='equipment.HardwareVersion & validate']").sendKeys("48001");
//
//	      findElementByXPath(driver,"//textarea[@value.bind='equipment.Notes']").sendKeys("Notes");
//	      findElementByXPath(driver,"//button[@click.delegate='saveItem()']").click();	
				
				
			}
	
	if(countoflist_intable>0) {
//		
		WebElement GetListing_Table =  findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr");
			
	        List <WebElement> Mission_existance = GetListing_Table.findElements(By.xpath("//*[contains(text(),'Automated Polygrid Test')]"));
		
		
	  int PlannedMission_count  = Mission_existance.size();
		
		 System.out.println("Count Of Other Equipment  List  "+PlannedMission_count);
//		
		 if (PlannedMission_count>0 ) {
			 
			 System.out.println("Polygrid operation alreday created");
			 
			 
		 }
		
		}
 
 }catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	
}	
	
public static  WebElement findElementByXPath(WebDriver driver, String xpath) {
	return driver.findElement(By.xpath(xpath));
}


public static  List <WebElement> findElementsByXPath(WebDriver driver, String xpath) {
	return driver.findElements(By.xpath(xpath));
}


}
