package Add_flightLocation;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class Create_flightLocation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

public static void add_flightLoc(WebDriver driver) {
	
	
	
	findElementByXPath(driver,"//a[@href='#/missions']").click();
	
	try {
		Thread.sleep(5000);
		findElementByXPath(driver,"//a[@href='#/locations']").click();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	findElementByXPath(driver,"//input[@class='searchTable au-target']").sendKeys("Automated ANRA Loc");
	
	int countoflist_intable = findElementsByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr").size();

System.out.println(">>>>>>countof Operation Location "+ countoflist_intable);
	
	if (countoflist_intable<=0) {
		
		System.out.println("Creating Automated loc");
		
		 findElementByXPath(driver,"//button[@click.delegate='add()']").click();
    
	findElementByXPath(driver,"//input[@value.bind='location.Name & validate']").sendKeys("Automated ANRA Loc");;
	     
	try {
		
		Thread.sleep(2000);
		findElementByXPath(driver,"//input[@placeholder='Latitude']").clear();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	findElementByXPath(driver,"//input[@placeholder.bind='placeholder']").sendKeys("28.617678840");
	
try {
		
		Thread.sleep(2000);
		findElementByXPath(driver,"//input[@placeholder='Longitude']").clear();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
//	findElementByXPath(driver,"//input[@placeholder='Longitude']").clear();
	
	findElementByXPath(driver,"//input[@placeholder='Longitude']").sendKeys("77.3907074332");
	findElementByXPath(driver,"//input[@value.bind='location.Altitude & validate']").sendKeys("20");
	findElementByXPath(driver,"//input[@value.bind='location.City & validate']").sendKeys("Noida");
	
	
	Select State = new Select(findElementByXPath(driver, "//select[@value.bind='location.StateId & validate']"));
	State.selectByVisibleText("Other");
	
	
	findElementByXPath(driver,"//input[@value.bind='location.Zipcode & validate']").sendKeys("201301");
	
	Select Country = new Select(findElementByXPath(driver, "//select[@value.bind='location.CountryId & validate']"));
	Country.selectByVisibleText("Other");
	
	
	Select Project = new Select(findElementByXPath(driver, "//select[@value.bind='location.ProjectId & validate']"));
	Project.selectByVisibleText("KGF");
	
	findElementByXPath(driver,"//span[@class='ui-switch-handle']").click();
	findElementByXPath(driver,"//textarea[@value.bind='location.Notes']").sendKeys("Notes");
	findElementByXPath(driver,"//button[@click.delegate='saveItem()']").click();

	}
	
	else if (countoflist_intable>0) {
		
		
		System.out.println("Auto Location Already created");
	}
	
	
	
	
}	
	
	
	
	

public static  WebElement findElementByXPath(WebDriver driver, String xpath) {
	return driver.findElement(By.xpath(xpath));
}


public static  List <WebElement> findElementsByXPath(WebDriver driver, String xpath) {
	return driver.findElements(By.xpath(xpath));
}
	
	
}
