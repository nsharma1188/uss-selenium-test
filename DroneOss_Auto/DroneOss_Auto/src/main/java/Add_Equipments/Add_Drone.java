package Add_Equipments;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class Add_Drone {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	
	public static void create_drone(WebDriver driver) {
		
		
		
		findElementByXPath(driver,"//*[@id=\"nav\"]/li[4]/a").click();
		
		

		 try {
			  
			 findElementByXPath(driver,"//input[@class='searchTable au-target']").sendKeys("Automated Drone");
			 
			 try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			 
			 
			 
			 
			  WebElement GetListing_Table =  findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr");
				
			  List <WebElement> Drone_existance = GetListing_Table.findElements(By.xpath("//*[contains(text(),'Automated Drone')]"));
				
				
			  int Drone_existance_count = Drone_existance.size();
				
				 System.out.println("Count Of Drone List  "+Drone_existance_count);
			  
			  
		
				
			if (Drone_existance_count==0) {
					
				
				System.out.println("Drone Count Is less or equal");
//					Add New Drone To Organisation
					findElementByXPath(driver,"//button[@click.delegate='add()']").click();
					findElementByXPath(driver,"//input[@value.bind='drone.Title & validate']").sendKeys("Automated Drone");
					findElementByXPath(driver,"//input[@value.bind='drone.LegalIdentification & validate']").sendKeys("214578");
					findElementByXPath(driver,"//span[@class='ui-switch-handle']").click();
					Select select_brand = new Select(findElementByXPath(driver, 
							"//select[@value.bind='drone.BrandId & validate']"));
					select_brand.selectByVisibleText("DJI");
					
					
					
					findElementByXPath(driver,"//input[@value.bind='drone.ModelName & validate']").sendKeys("Phantom 4 pro");
					findElementByXPath(driver,"//input[@value.bind='drone.PurchaseDate & validate']").sendKeys("31-05-2019");
					findElementByXPath(driver,"//input[@value.bind='drone.Weight & validate']").sendKeys("1");
					findElementByXPath(driver,"//input[@value.bind='drone.FirmwareVersion & validate']").sendKeys("2365");
					findElementByXPath(driver,"//input[@value.bind='drone.HardwareVersion & validate']").sendKeys("214578");
					findElementByXPath(driver,"//input[@value.bind='drone.BaseAlt & validate']").sendKeys("25");
					findElementByXPath(driver,"//input[@value.bind='drone.CollisionThresholdV']").sendKeys("1000");
					findElementByXPath(driver,"//input[@value.bind='drone.CollisionThresholdH']").sendKeys("1000");
					findElementByXPath(driver,"//textarea[@value.bind='drone.Notes']").sendKeys("Notes");
					findElementByXPath(driver,"//button[@click.delegate='saveItem()']").click();
					
					
				}
			else if (Drone_existance_count > 0 ) {
				
				System.out.println(">>Drone already created<<<<");
				
				
			}
				
			
		
		 } catch (NoSuchElementException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		
		
		
	}
	
	
	public static  WebElement findElementByXPath(WebDriver driver, String xpath) {
		return driver.findElement(By.xpath(xpath));
	}

	
	public static  List <WebElement> findElementsByXPath(WebDriver driver, String xpath) {
		return driver.findElements(By.xpath(xpath));
	}
	
	
}
