package Add_Equipments;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class Add_Batteries {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static void create_battery(WebDriver driver) {
		
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		findElementByXPath(driver,"//a[@href='#/batteries']").click();
		
		
		
		
		
		  
		
		
		 try {
			  
//				int batteryListcount= findElementsByXPath(driver,"//table[@class='table table-striped table-bordered dataTable no-footer au-target']/tbody/tr").size();
//				 
//				System.out.println(">>>>>>countof User list"+ batteryListcount);
			
			 WebElement GetListing_Table =  findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr");
				
			  List <WebElement> Drone_Battery_existance = GetListing_Table.findElements(By.xpath("//*[contains(text(),'Automatic Battery')]"));
				
				
			  int Drone_battery_count  = Drone_Battery_existance.size();
				
				 System.out.println("Count Of Drone List  "+Drone_battery_count);
			 
			 
			 
			 
				
				
					
				if (Drone_battery_count==0) {
		
		             System.out.println("Creating Auto Battery For Automated Drone");
		             
		             findElementByXPath(driver,"//button[@click.delegate='add()']").click();
		             findElementByXPath(driver,"//input[@value.bind='battery.BatteryName & validate']").sendKeys("Automatic Battery");
		             findElementByXPath(driver,"//input[@value.bind='battery.PurchaseDate']").sendKeys("03-06-2019");
		             findElementByXPath(driver,"//input[@value.bind='battery.SerialNumber & validate']").sendKeys("214565");
		             
		             findElementByXPath(driver,"//span[@class='ui-switch-handle']").click();
		             
		             Select associated_drone = new Select (findElementByXPath(driver,"//select[@value.bind='battery.DroneId & validate']"));
		             associated_drone.selectByVisibleText("Automated Drone");
		             
		             
		             findElementByXPath(driver,"//input[@value.bind='battery.CellCount & validate']").clear();;
		             findElementByXPath(driver,"//input[@value.bind='battery.CellCount & validate']").sendKeys("5");
		             findElementByXPath(driver,"//input[@value.bind='battery.Capacity & validate']").sendKeys("4800");
		             findElementByXPath(driver,"//input[@value.bind='battery.InitialCycleCount & validate']").clear();
		             findElementByXPath(driver,"//input[@value.bind='battery.InitialCycleCount & validate']").sendKeys("1");  
		             findElementByXPath(driver,"//input[@value.bind='battery.FirmwareVersion & validate']").sendKeys("15478");
		             findElementByXPath(driver,"//input[@value.bind='battery.HardwareVersion & validate']").sendKeys("2548");
		             findElementByXPath(driver,"//textarea[@value.bind='battery.Notes']").sendKeys("Notes");
		             findElementByXPath(driver,"//button[@click.delegate='saveItem()']").click();
		         
		             
		
				}
				else if  (Drone_battery_count>0){
					
					
					System.out.println(">>>>>>>>>  Battery already created ");
				}
				
				
				
		 
		 
		 
		 
		 }catch (NoSuchElementException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
	}
	
	public static  WebElement findElementByXPath(WebDriver driver, String xpath) {
		return driver.findElement(By.xpath(xpath));
	}

	
	public static  List <WebElement> findElementsByXPath(WebDriver driver, String xpath) {
		return driver.findElements(By.xpath(xpath));
	}
	
	
	
	
	
}
