package Add_Equipments;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class Add_Sensor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

public static void create_Sensor(WebDriver driver) {
	
	
	
	try {
		Thread.sleep(4000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	findElementByXPath(driver,"//a[@href='#/sensors']").click();
	
	
	
	 try {
		  
//			int SensorListcount= findElementsByXPath(driver,"//table[@class='table table-striped table-bordered dataTable no-footer au-target']/tbody/tr").size();
//			 
//			System.out.println(">>>>>>countof User list"+ SensorListcount);
		
			
		 WebElement GetListing_Table =  findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr");
			
		  List <WebElement> Drone_Sensor_existance = GetListing_Table.findElements(By.xpath("//*[contains(text(),'Automatic Sensor')]"));
			
			
		  int Drone_Sensor_count  = Drone_Sensor_existance.size();
			
			 System.out.println("Count Of sensor List  "+Drone_Sensor_count);
		 
		 
		 
		 
//			
				
			if (Drone_Sensor_count==0) {
	
	             System.out.println("Creating Auto Battery For Automated Drone");
	             
	             findElementByXPath(driver,"//button[@click.delegate='add()']").click();
	             findElementByXPath(driver,"//input[@value.bind='sensor.Name & validate']").sendKeys("Automatic Sensor");
	             findElementByXPath(driver,"//input[@value.bind='sensor.PurchaseDate & validate']").sendKeys("03-06-2019");
	             findElementByXPath(driver,"//input[@value.bind='sensor.SerialNumber & validate']").sendKeys("214565");
	            
	             Select associated_drone = new Select (findElementByXPath(driver,"//select[@value.bind='sensor.DroneId & validate']"));
	             associated_drone.selectByVisibleText("Automated Drone");
	             findElementByXPath(driver,"//span[@class='ui-switch-handle']").click();
	             
	             
	             
	             
	             findElementByXPath(driver,"//input[@value.bind='sensor.Weight & validate']").sendKeys("1");
	             findElementByXPath(driver,"//input[@value.bind='sensor.FirmwareVersion & validate']").sendKeys("5");
	             findElementByXPath(driver,"//input[@value.bind='sensor.HardwareVersion & validate']").sendKeys("4800");
//	             findElementByXPath(driver,"//input[@value.bind='battery.InitialCycleCount & validate']").clear();
//	             findElementByXPath(driver,"//input[@value.bind='battery.InitialCycleCount & validate']").sendKeys("1");  
//	             findElementByXPath(driver,"//input[@value.bind='battery.FirmwareVersion & validate']").sendKeys("15478");
//	             findElementByXPath(driver,"//input[@value.bind='battery.HardwareVersion & validate']").sendKeys("2548");
	             findElementByXPath(driver,"//textarea[@value.bind='sensor.Notes']").sendKeys("Notes");
	             findElementByXPath(driver,"//button[@click.delegate='saveItem()']").click();
	         
	             
	
			}
			else if  (Drone_Sensor_count>0){
				
				
				System.out.println(">>>>>>>>>  Sensor already created ");
			}
			
			
			
	 
	 
	 
	 
	 }catch (NoSuchElementException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
	
	
	
	
	
	
}	
	
	
public static  WebElement findElementByXPath(WebDriver driver, String xpath) {
	return driver.findElement(By.xpath(xpath));
}


public static  List <WebElement> findElementsByXPath(WebDriver driver, String xpath) {
	return driver.findElements(By.xpath(xpath));
}


	
	
}
