package Admin_Login_Organisation;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import GetLocal_datetime.Get_DateTime;

public class Admin_Login_ProjectUserClient_Creation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	
	public static void Admin_Add_Client(WebDriver driver) {
		
		System.out.println ("Finding The Client Link. Project Creation");
		  findElementByXPath(driver,"//a[@href='#/clients']").click();
		  
//		  WebDriverWait wait_for_addbutton= new WebDriverWait(driver,160);
//		  
//		  wait_for_addbutton.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\\\"body\\\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr")));
		  
		  
		  findElementByXPath(driver,"//input[@class='searchTable au-target']").sendKeys("Qainfotech");
		  
		  WebElement GetListingTable =  findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr");
		  
		  List <WebElement> Client_existance = GetListingTable.findElements(By.xpath("//*[contains(text(),'Active')]"));
		
		  
		 
		
		int Client_existance_count = Client_existance.size();
		
		 System.out.println(Client_existance_count);
		 
		 if (Client_existance_count<0) {
			 
			 
			 
			 System.out.println("count check is less");
			 
//			 To be Continued if Client _existance_count is Less then add Client 
			 
			 System.out.println("Adding client");
			 
			 findElementByXPath(driver,"//button[@click.delegate='add()']").click();
			 findElementByXPath (driver,"//div[@class='input-group']/input").sendKeys("");

			 
			 
		 }
		 else if (Client_existance_count>0) {
			 
			 
			 System.out.println("count check is greater Client Already Exists"); 
			 
			

			 
			 
			 
			 
		 }
		
		
	}
	
	
	public static void Admin_add_Project(WebDriver driver) {
		
		
		System.out.println("Finding the Link to Create Project");
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		findElementByXPath(driver,"//a[@href='#/projects']").click();

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		//		Search for Active Client
		
		findElementByXPath(driver,"//input[@class='searchTable au-target']").sendKeys("Qainfotech");
		  
		  WebElement GetListing_Table =  findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr");
		
		  List <WebElement> Project_existance = GetListing_Table.findElements(By.xpath("//*[contains(text(),'Qainfotech')]"));
		
		  int Project_existance_count = Project_existance.size();
			
			 System.out.println(Project_existance_count);
			 
			 
			 
			 if (Project_existance_count<0) {
				 
				 try {
						Thread.sleep(4000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				 System.out.println("finding button");
				findElementByXPath(driver,"//button[@click.delegate='add()']").click();
				
				System.out.println("Clickd on Add button");
				 try {
						Thread.sleep(3000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
				findElementByXPath(driver,"//input[@placeholder='Project Name']").sendKeys("Project TEST 1");
				
				findElementByXPath(driver,"//input[@value.bind='project.RevenueAmount & validate']").sendKeys("1000");
				 
				
				System.out.println("finding the Select icon");
				
				Select select_Client = new Select(findElementByXPath(driver, 
						"//*[@id=\"body\"]/main/router-view/project-editor/section/div/div/form/div/div/div[1]/div[2]/div[2]/fieldset/select"));
				select_Client.selectByVisibleText("Inspection Test");
				
				System.out.println("Found brij mohan ");
				
				 findElementByXPath(driver,"//span[@class='ui-switch-handle']").click();
				
				findElementByXPath(driver,"//input[@type='file']").sendKeys("D:\\SeleniumProject\\DroneOss_Auto\\DroneOss_Auto\\Confidential\\ANRA_Park.kml");
				
				findElementByXPath(driver,"//button[@click.delegate='saveItem()']"); 
				
				 
				 
			 }
			 else if (Project_existance_count>0) {
				 
				 
				 System.out.println("count check is greater Project Alreday Exists"); 
				 
				 }
			 
		 		 
	}

	
	
	public static void Admin_add_User(WebDriver driver) {
		
		
		
        System.out.println("Finding the Link to Create User/Pilot");
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		findElementByXPath(driver,"//a[@href='#/users']").click();
		
		
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		findElementByXPath(driver,"//input[@class='searchTable au-target']").sendKeys("Automated Pilot  1");
		
		
		 try {
			  
			int usercount= findElementsByXPath(driver,"//table[@class='table table-striped table-bordered dataTable no-footer au-target']/tbody/tr").size();
			 
			System.out.println(">>>>>>countof User list"+ usercount);
			
			if( usercount<=0)
			{
				System.out.println("User Does not  Exists");
				
				findElementByXPath(driver,"//button[@click.delegate='add()']").click();
				
				findElementByXPath(driver,"//input[@placeholder='First Name']").sendKeys("Automated Pilot ");
				
				findElementByXPath(driver,"//input[@value.bind='user.LastName & validate']").sendKeys("1");
				
				findElementByXPath(driver,"//input[@masked='value.bind: user.PhoneNumber & validate; mask: (999) 999-9999;']").sendKeys("7503663618");
				
				findElementByXPath(driver,"//input[@value.bind='user.Email & validate']").sendKeys("automatedpilot1@yopmail.com");
				
				findElementByXPath(driver,"//input[@value.bind='user.IMEINumber & validate']").sendKeys("2145789865");
				
				findElementByXPath(driver,"//input[@value.bind='user.LicenseNumber']").sendKeys("1548712");
				
				findElementByXPath(driver,"//input[@value.bind='user.MedicalCertNumber']").sendKeys("2154789");
				findElementByXPath(driver,"//input[@value.bind='user.UnmannedHour']").sendKeys("12");
				findElementByXPath(driver,"//input[@value.bind='user.MannedHour']").sendKeys("26");
				
//				Select select_Client = new Select(findElementByXPath(driver, 
//						"//*[@id=\"ddlRole\"]"));
//				select_Client.selectByVisibleText("Pilot");
				
				
				findElementByXPath(driver,"//option[@model.bind='role.RoleId']").click();
				findElementByXPath(driver,"//input[@value.bind='user.Password & validate']").sendKeys("Password1");
				findElementByXPath(driver,"//input[@value.bind='ConfirmPassword']").sendKeys("Password1");
				
//				add document needs to be added afterwards.
				
				
//				---------Adding Pliot expiration date--------
				findElementByXPath(driver,"//input[@value.bind='user.PilotLicenseExpiryDate']").sendKeys("30-05-2020");
				findElementByXPath(driver,"//input[@value.bind='user.MedicalLicenseExpiryDate']").sendKeys("30-05-2020");
				
				findElementByXPath(driver,"//span[@class='ui-switch-handle']").click();
					
				findElementByXPath(driver,"//button[@click.delegate='saveItem()']").click();
				
				System.out.println("User created :- Pilot");
				
				
				
			}
			
			
			
			if (usercount>0) {
				
				System.out.println(">>>>>>Automated Pilot already created");
				
				
			}	
			
			
			} catch (NoSuchElementException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				  
		
	}
	
	
	public static void Admin_add_VisualUser(WebDriver driver) {
		
		
		 System.out.println("Finding the Link to Create AutomatedVisualObserver");
			
		
			
			
//			findElementByXPath(driver,"//a[@href='#/users']").click();
			
			
			
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		String getSearchName = 	findElementByXPath(driver,"//input[@class='searchTable au-target']").getText();
			
		     findElementByXPath(driver,"//input[@class='searchTable au-target']").clear();
		     
			
		     findElementByXPath(driver,"//input[@class='searchTable au-target']").sendKeys("Automated VisualObserver");
			
			
			 try {
				  
				int usercount= findElementsByXPath(driver,"//table[@class='table table-striped table-bordered dataTable no-footer au-target']/tbody/tr").size();
				 
				System.out.println(">>>>>>countof User list"+ usercount);
				
				if( usercount<=0)
				{
					System.out.println("User Does not  Exists");
					
					findElementByXPath(driver,"//button[@click.delegate='add()']").click();
					
					findElementByXPath(driver,"//input[@value.bind='user.FirstName & validate']").sendKeys("Automated");
					
					findElementByXPath(driver,"//input[@value.bind='user.LastName & validate']").sendKeys("VisualObserver");
					
					findElementByXPath(driver,"//input[@masked='value.bind: user.PhoneNumber & validate; mask: (999) 999-9999;']").sendKeys("7503663618");
					
					findElementByXPath(driver,"//input[@value.bind='user.Email & validate']").sendKeys("automatedvisualob@yopmail.com");
					
					findElementByXPath(driver,"//input[@value.bind='user.IMEINumber & validate']").sendKeys("21457898651");
					
					findElementByXPath(driver,"//input[@value.bind='user.LicenseNumber']").sendKeys("15487121");
					
					findElementByXPath(driver,"//input[@value.bind='user.MedicalCertNumber']").sendKeys("21547819");
					findElementByXPath(driver,"//input[@value.bind='user.UnmannedHour']").sendKeys("112");
					findElementByXPath(driver,"//input[@value.bind='user.MannedHour']").sendKeys("226");
					
//					Select select_VisualObserver = new Select(findElementByXPath(driver, 
//							"select[@change.delegate='setselectedOption($event)']/option[2]"]"));
//					select_VisualObserver.selectByVisibleText("Pilot");
					
					findElementByXPath(driver,"//*[@id=\"ddlRole\"]/option[2]").click();
//					findElementByXPath(driver,"//option[@model.bind='role.RoleId']").click();
					findElementByXPath(driver,"//input[@value.bind='user.Password & validate']").sendKeys("Password1");
					findElementByXPath(driver,"//input[@value.bind='ConfirmPassword']").sendKeys("Password1");
					

					
//					Need to Implement Feature to Get Local date and time and Add 1 month To actual date.
					  
					
					
					
//					---------Adding Pliot expiration date--------
					findElementByXPath(driver,"//input[@value.bind='user.PilotLicenseExpiryDate']").sendKeys("30-05-2020");
					findElementByXPath(driver,"//input[@value.bind='user.MedicalLicenseExpiryDate']").sendKeys("30-05-2020");
					
					findElementByXPath(driver,"//span[@class='ui-switch-handle']").click();
						
					findElementByXPath(driver,"//button[@click.delegate='saveItem()']").click();
					
					System.out.println("User created :- Automated Visual Observer");
					
					
					
				}
				
				
				
				if (usercount>0) {
					
					System.out.println(">>>>>>Automated Visual Observer already created");
					
					
				}	
				
				
				} catch (NoSuchElementException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
		
		
	}
	
	
	
	public static  WebElement findElementByXPath(WebDriver driver, String xpath) {
		return driver.findElement(By.xpath(xpath));
	}

	
	public static  List <WebElement> findElementsByXPath(WebDriver driver, String xpath) {
		return driver.findElements(By.xpath(xpath));
	}
	
}
