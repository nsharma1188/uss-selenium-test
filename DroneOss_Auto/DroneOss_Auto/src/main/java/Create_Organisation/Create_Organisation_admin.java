package Create_Organisation;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class Create_Organisation_admin {

//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//
//	}
	
	public static void Create_Admin(WebDriver driver) {
		
		
		// Below code for opening Admin Screen
		
		
		System.out.println("finding organisation");
		
		try {
			Thread.sleep(15000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//				driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
				findElementByXPath(driver, "//*[@id=\"nav\"]/li[8]/a").click();

				driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
				System.out.println("finding organisation");
				driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);

				findElementByXPath(driver, "//div[@class='container']/ul[@class='row bashboard-block nav nav-pills']/li[@class='col-md-2 bas-bx-3 au-target']/a").click();
				System.out.println("Organisation Found");
		  
//				String Add_organisation_button = ""
			
				
				 WebDriverWait wait_for_addbutton= new WebDriverWait(driver,160);
				
				 
				 wait_for_addbutton.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn btn-primary btn-lg au-target']")));
				
				 
				 findElementByXPath(driver,"//input[@placeholder='Search']").sendKeys("Qainfotech");
				 
				 
				 
				 WebElement GetListingTable = findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[2]/table/tbody/tr");
				 List <WebElement> Organisation_existance = GetListingTable.findElements(By.xpath("//*[contains(text(),'Qainfotech')]"));
				 
				 
				 int Search_result = Organisation_existance.size();
				 
				 System.out.println("Qainfotech Found"+Search_result);
				 
				 
				 
				 if(Search_result==0) {
				 
				 findElementByXPath(driver,"//button[@class='btn btn-primary btn-lg au-target']").click();
				 
				 
//				try {
//					Thread.sleep(15000);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
				
				
//				 WebElement Add_Organisation = findElementByXPath(driver,"//*[@id=\"body\"]/main/router-view/section[2]/div/div/div[1]/div/div/button"); 
//				 
//				 String getbuttontext = Add_Organisation.getText();
//				 
//		         System.out.println(getbuttontext);
//		     
//		        if (getbuttontext!="Add New Organization") {
//		        	
//		        	Add_Organisation.click();
				 
				 
				 

				 
				 
//		        	Change To Dyanmic Entry
		        	findElementByXPath(driver,"//input[@placeholder=\"Organization Name\"]").sendKeys("Qainfotech");		        	
		        	findElementByXPath(driver,"//input[@placeholder=\"Contact Email\"]").sendKeys("nsharma1@flyanra.com");		        	
		        	findElementByXPath(driver,"//input[@masked='value.bind: organization.ContactPhone & validate; mask: (999) 999-9999;']").sendKeys("7503663518");		        	
		        	findElementByXPath(driver,"//select[@value.bind='organization.CurrencyId & validate']").sendKeys("USD");		        	
		        	findElementByXPath(driver,"//select[@value.bind='organization.UnitId & validate']").sendKeys("Metric");		        	
		        	findElementByXPath(driver,"//input[@placeholder='Website']").sendKeys("www.Qainfotech.com");		     
		        	findElementByXPath(driver,"//input[@placeholder='Government License']").sendKeys("4565654421");		        	
		        	findElementByXPath(driver,"//input[@placeholder='Federal Tax ID/EIN']").sendKeys("8155452");
//		        	-------------------
		        	
		        	
//		        	
		        	findElementByXPath(driver,"//ui-switch[@checked.bind='organization.IsInsuranceAvailable']").click();
		        	findElementByXPath(driver,"//ui-switch[@checked.bind='organization.IsAdminApprovalRequired']").click();
		        	findElementByXPath(driver,"//textarea[@id='notes']").sendKeys("G-111 Second Floor  Sector 63 Noida Uttar Pradesh");
		        	findElementByXPath(driver,"//input[@placeholder='Email']").sendKeys("nsharma1@flyanra.com");
		        	findElementByXPath(driver,"//select[@value.bind='organization.PackageId']").sendKeys("Free");
		        	findElementByXPath(driver,"//input[@placeholder='Password']").sendKeys("Pasword1");
		        	findElementByXPath(driver,"//input[@placeholder='ReType Password']").sendKeys("Pasword1");
		        	findElementByXPath(driver,"//input[@placeholder='First Name']").sendKeys("Nikhil");
		        	findElementByXPath(driver,"//input[@placeholder='Last Name']").sendKeys("Sharma");
		        	findElementByXPath(driver,"//button[@click.delegate='saveItem()']").click();
		        	
		        	
//		        	------For LogOut from the -----
		        	findElementByXPath(driver,"//*[@id=\"body\"]/nav-bar/header/div[1]/div[2]/div/div/ul/li[4]/a").click(); 
					findElementByXPath(driver,"//*[@id=\"body\"]/nav-bar/header/div[1]/div[2]/div/div/ul/li[4]/ul/li[2]/a").click();
				 }
				 else if (Search_result==1) {
					 
					findElementByXPath(driver,"//*[@id=\"body\"]/nav-bar/header/div[1]/div[2]/div/div/ul/li[4]/a").click(); 
					findElementByXPath(driver,"//*[@id=\"body\"]/nav-bar/header/div[1]/div[2]/div/div/ul/li[4]/ul/li[2]/a").click();
					 
					
				   findElementByXPath(driver,"//input[@placeholder='Email']").sendKeys("admin123@yopmail.com");
				   findElementByXPath(driver,"//input[@placeholder='Password']").sendKeys("Password1");	
				   findElementByXPath(driver,"//button[@type='submit']").click();

					
					
					
				 }
	}
	
	
	
	public static  WebElement findElementByXPath(WebDriver driver, String xpath) {
		return driver.findElement(By.xpath(xpath));
	}


}
