package Read_Write_excel;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



public class WriteExcelData {

	
	
	
	//public static void main() throws Exception  {
		// TODO Auto-generated method stub
       
       
	//}
    
	
	public static void writedata(int sheetnumber,int row,int column,String data, String excelpath ) {
	
		try
		{
		File src =new File(excelpath);
		
		FileInputStream fis =new  FileInputStream(src);
		
		XSSFWorkbook wb=new XSSFWorkbook(fis);
		
		XSSFSheet sheet1= wb.getSheetAt(sheetnumber);
		
		sheet1.getRow(0).createCell(2).setCellValue(data);
		
		FileOutputStream fout= new FileOutputStream(src);
		
		wb.write(fout);
		
		wb.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	
		
	}
	
	
	
	
	
	public static void Write_mission_data(int sheetnumber,int row,int column,String data, String excelpath ) {
		
		try
		{
		File src =new File(excelpath);
		
		FileInputStream fis =new  FileInputStream(src);
		
		XSSFWorkbook wb=new XSSFWorkbook(fis);
		
		XSSFSheet sheet1= wb.getSheetAt(sheetnumber);
		
		sheet1.getRow(row).createCell(column).setCellValue(data);
		
		FileOutputStream fout= new FileOutputStream(src);
		
		wb.write(fout);
		
		wb.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	
		
	}	
		
}
