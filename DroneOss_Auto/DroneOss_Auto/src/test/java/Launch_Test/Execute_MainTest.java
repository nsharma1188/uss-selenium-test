package Launch_Test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import Add_Equipments.Add_Batteries;
import Add_Equipments.Add_Drone;
import Add_OtherEquipment.Other_Equipment;
import Admin_Login_Organisation.Admin_Login_ProjectUserClient_Creation;
import Create_Organisation.Create_Organisation_admin;
import Mission_Planning.Mission_Polygrid;
import Pilot_auth.Pilot_login;



public class Execute_MainTest {

static WebDriver driver;

	  private static void CreateDriver() {

			System.setProperty("webdriver.chrome.driver", "C:\\selenium driver\\chromedriver.exe");
			driver = new ChromeDriver();
//			if(null!=driver){
//			System.out.println("Driver is Null");
//		
//			}else {
//				System.out.println("Driver is not Null");
//				
//			}
		} 
	
	
	
	
  @Test
  public void LaunchFunction() {
	  
	  CreateDriver();
	  Login_dashboard.loginFunctionality(driver);
	  
	  String CurrentUrl = driver.getCurrentUrl();
	  System.out.println(CurrentUrl);
	  if (CurrentUrl.equals("https://dev-oss.flyanra.net/#/login")) {
	  Assert.assertEquals(CurrentUrl, "https://dev-oss.flyanra.net/#/login","Failed Url Did not mateched");
	  }
	  else if (CurrentUrl.equals("https://dev-oss.flyanra.net/#/")) {
		  
		  Assert.assertEquals(CurrentUrl, "https://dev-oss.flyanra.net/#/","Failed Url Did not mateched");   
	  }
    
  }
  
  
  @Test (priority=2,dependsOnMethods= {"LaunchFunction"})
  public void Create_Organisation() {
	  
	  
	 Create_Organisation_admin.Create_Admin(driver); 
	
	  
	  
  }
  
  @Test (priority=2,dependsOnMethods= {"Create_Organisation"})
  public void CreateClient() {
	  
	  System.out.println(">>>>>>>> admin Add Client ");
	  Admin_Login_ProjectUserClient_Creation.Admin_Add_Client(driver);
	  
	  
	  
  }
  
  @Test(priority=3,dependsOnMethods= {"CreateClient"})
  public void CreateProject() {
	  
	  
	  System.out.println(">>>>>>>>>admin");
	  Admin_Login_ProjectUserClient_Creation.Admin_add_Project(driver);
  }
  
  
  
 @Test(priority=4,dependsOnMethods= {"CreateProject"}) 
  public void CreateUser() {
	 
	 System.out.println(">>>>>>>>Create User");
	 Admin_Login_ProjectUserClient_Creation.Admin_add_User(driver);
	 
 }
  
 
 
 @Test(priority=5,dependsOnMethods= {"CreateUser"}) 
 
 public void CreateVisualUser() {
	 
	 System.out.println(">>>>>>>>Create AutomatedVisualUser");
	 
	 Admin_Login_ProjectUserClient_Creation.Admin_add_VisualUser(driver);
	 
	 
	 
 }
 
 
 
 @Test (priority=6,dependsOnMethods= {"CreateVisualUser"})
 public void Pilot_authentication() {
	 
	 System.out.println(">>>>>>>>Pilot Login Authentication");
	 
	 Pilot_login.Pilot_lgin(driver);
	
	 
 }
  
 
@Test (priority=7,dependsOnMethods= {"Pilot_authentication"}) 
public void Add_Equipment() {
	
	 System.out.println(">>>>>>Adding Equipment");
		
	 Add_Drone.create_drone(driver);
 
} 
	 
@Test (priority=8,dependsOnMethods= {"Add_Equipment"})	
public void Add_battery() {
	
	System.out.println(">>>>>>> Adding Batteries For Organisation");
	
	Add_Batteries.create_battery(driver);
	
}


@Test(priority=9,dependsOnMethods= {"Add_battery"})
public void Add_Sensor() {
	
	
	System.out.println(">>>>>>> Adding Sensor For Organisation");
	
	Add_Equipments.Add_Sensor.create_Sensor(driver);
	
}
 
@Test(priority=10,dependsOnMethods= {"Add_Sensor"})
	public void Add_OtherEquipment	() {
	
	System.out.println(">>>>>>>>>> Adding Other Equipment ");
		Other_Equipment.create_equipment(driver);
	
	
}

@Test(priority=11,dependsOnMethods= {"Add_OtherEquipment"})
public void Add_DefaultLocation	() {

System.out.println(">>>>>>>>>> Adding Automated DefaultLocation ");
Add_flightLocation.Create_flightLocation.add_flightLoc(driver);	


}

@Test(priority=12,dependsOnMethods= {"Add_DefaultLocation"})
public void Add_Mission_polygrid() {

System.out.println(">>>>>>>>>> Adding Mission Polygrid  ");

   Mission_Polygrid.create_polygrid(driver);


}


}
 
 

