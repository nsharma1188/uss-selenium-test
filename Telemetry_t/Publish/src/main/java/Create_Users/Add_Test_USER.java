package Create_Users;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Create_ANSP.Add_ANSP;
import Create_Organisation.Add_TestOrganisation;
import Generate_Token.Token;

public class Add_Test_USER {

	
	public static String pilot_Email;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	
public static void add_pilot(WebDriver driver) {
	
	
	
		 try {
			
			 
			 System.out.println(">>>>>>> trying to login to Admin user");
			 
			 WebDriverWait wait = new WebDriverWait (driver,180);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='Email']"))); 
			 
				findElementByXPath(driver,"//input[@placeholder='Email']").sendKeys(Add_TestOrganisation.Org_Email);
				
				findElementByXPath(driver,"//input[@type='password']").sendKeys("Password1");
				
				findElementByXPath(driver,"//a[@role='button']").click();
			    Thread.sleep(5000);
				Token.Admin_token();
			    System.out.println("ADmin OTP is>>>"+ Token.Admin_OTP_Token);
			    
			    findElementByXPath(driver,"//ui-input[@value.bind='VerificationCode']/div/input").sendKeys(Token.Admin_OTP_Token);
			    
			    findElementByXPath(driver,"//ui-button[@click.trigger='OTPverification()']").click();
			
			 WebElement loaderClass = findElementByXPath(driver,"//ui-loader[@busy.bind='isWaiting']");
			   String getloaderclass = loaderClass.getAttribute("class");
			   System.out.println("class is "+getloaderclass);
			   
			   if(getloaderclass.equalsIgnoreCase("au-target ui-loader") || getloaderclass.equalsIgnoreCase("au-target ui-loader aurelia-hide") ) {
					
//					-------------Creating New Test Organisation-------------	 			 
				 
				 System.out.println(" clicking on ATC Icon");
				 Thread.sleep(2000);
				 
				 WebDriverWait Wait_Organisation_Elemenet = new WebDriverWait(driver,38);     
				 Wait_Organisation_Elemenet.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),\"USERS \")]//parent::a")));  //a[@click.delegate='organizationLayout()'],//a[@click.delegate=\"goto('anspPage', true)\"]
				 System.out.println(" located User icon from header"); 
				 Thread.sleep(2000);
				 findElementByXPath(driver,"//span[contains(text(),\"USERS \")]//parent::a").click();	 
				 System.out.println("clicked on User icon");
				 
				 Thread.sleep(3000);
				  System.out.println("clicking  search  icon");
				  WebElement element = driver.findElement(By.xpath("//span[@class='search_bx']")); 
				  
				  Actions action = new Actions(driver);
				  action.moveToElement(element).build().perform();			  
			      findElementByXPath(driver,"//span[@class='search_bx']/input").sendKeys("Auto_");
			      
			      Thread.sleep(1000);
			        WebElement Get_PilotDetail = findElementByXPath(driver,"//tbody[@class='op_data']");
			        String details = Get_PilotDetail.getText();
			        
			        System.out.println(">>>>>> serach result"+details);  
			   
			        if (details.contains("Auto_pilot1"))	{
						  
						  
			        	System.out.println(" ATC alreday created logOut.");
			        	Thread.sleep(1000);
			        	System.out.print("clicking on the logout button");
		                  
			        	findElementByXPath(driver,"//i[@class='au-target fa fa-info-circle actve']/parent::a").click();
			        	Thread.sleep(1000);
			        	findElementByXPath(driver,"//i[@class='au-target fa fa-info-circle']/parent::a").click();
			        	
			        	pilot_Email =	findElementByXPath(driver,"//span[contains(text(), '@')]").getText();
							
							System.out.println(">>>>>> Email Address of ANSP is <<<<<<<<<<"+pilot_Email);
			        	
		                  
		            	Thread.sleep(2000);
		            	WebDriverWait ProfileImage = new WebDriverWait(driver,60);
		            	ProfileImage.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@title='Profile']/img")));
		            	System.out.println("clicked on the profile");
		            	
		            	WebElement logelement = driver.findElement(By.xpath("//a[@title='Profile']/img"));		            	
		            	 logelement.click();
		            	 
		            	 findElementByXPath(driver,"//ul[@class='dropdown-menu']/li[3]/a").click();
				  
				 
			 }else {

				 System.out.println(" Create New ATC  ");
			 
//				 -------------Creating New Test Organisation-------------
			 
				 WebDriverWait Wait_Element = new WebDriverWait(driver,180);     
				 Wait_Element.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@click.delegate='getUser()']")));
			 
				 findElementByXPath(driver,"//button[@click.delegate='getUser()']").click();
				 System.out.println("Kardiya Click Behencodd");
			
				 Thread.sleep(2000);
				 System.out.println("Trying to Click on first name");
				 findElementByXPath(driver,"//ui-input[@value.bind='user.FirstName & validate']/div/input").sendKeys("Auto_pilot1");
				 System.out.println("Passed value on first name");
				 findElementByXPath(driver,"//ui-input[@value.bind='user.LastName & validate']/div/input").sendKeys("Singh");
				 findElementByXPath(driver,"//ui-input[@value.bind='user.PhoneNumber & validate']/div/input").sendKeys("7503663518");
				 findElementByXPath(driver,"//ui-input[@value.bind='user.Email & validate']/div/input").sendKeys("auto_pilot1@yopmail.com");
				 findElementByXPath(driver,"//ui-input[@value.bind='user.Password & validate']/div/input").sendKeys("Password1");
				 findElementByXPath(driver,"//ui-input[@value.bind='user.AddressLine1']/div/input").sendKeys("Ithum Building");
				 findElementByXPath(driver,"//ui-input[@value.bind='user.AddressLine2']/div/input").sendKeys("B-808");
				 findElementByXPath(driver,"//ui-input[@value.bind='user.City']/div/input").sendKeys("Noida");
				 findElementByXPath(driver,"//ui-input[@value.bind='user.State']/div/input").sendKeys("UttarPradesh");
				 findElementByXPath(driver,"//ui-input[@value.bind='user.Zipcode']/div/input").sendKeys("201301");
				 
				 findElementByXPath(driver,"//label[@for='ui-switch-1']").click();
				 
				 findElementByXPath(driver,"//ui-button[@click.trigger='save()']").click();
				 
				 Thread.sleep(1000);
		    	 WebElement Toast_Message = findElementByXPath(driver,"//span[@class='ui-message']/p");
		    	String Toastmessage = Toast_Message.getText();
		    	System.out.println(Toastmessage);
		    	
		    	if (Toastmessage.contains("User Created")) {
					
					
					System.out.println(" ANSP User Loggin OUT.");
			
				findElementByXPath(driver,"//i[@class='au-target fa fa-info-circle actve']/parent::a").click();
				findElementByXPath(driver,"//i[@class='au-target fa fa-info-circle']/parent::a").click();
				pilot_Email =	findElementByXPath(driver,"//span[contains(text(), '@')]").getText();
					
					System.out.println(">>>>>> Email Address of ANSP is "+pilot_Email);
	                  
	            	Thread.sleep(2000);
	            	WebElement logelement = driver.findElement(By.xpath("//a[@title='Profile']/img"));
	            	
	            	 logelement.click();
	            	 Thread.sleep(2000);
	            	 findElementByXPath(driver,"//ul[@class='dropdown-menu']/li[3]/a").click();
//					-------------Creating New Test Organisation-------------
					  	} 
				 
			 }
			   
			   
			   }   
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	
	
	
}	
	
	
public static  WebElement findElementByXPath(WebDriver driver, String xpath) {
	return driver.findElement(By.xpath(xpath));
}		
	
}
