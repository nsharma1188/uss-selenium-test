package Create_ANSP;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Create_Organisation.Organisation_Loc_Json;

public class Add_ANSP {

//	private static final String String = null;

	public static String Ansp_Email;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		
		
		
		

	}

public static void Create_ANSP_User(WebDriver driver) {
	
//	  au-target ui-loader;
	  
	 try {
		 
		 

//		 Thread.sleep(2000);
//		 System.out.println("Sleep for few min");
//		 WebDriverWait Loader= new WebDriverWait(driver,5);
//		 Loader.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ui-loader[@busy.bind='isWaiting']")));
//
//		 
//		 Thread.sleep(18000);
//		 System.out.println("Sleep for few min for verifying loader class");
		 WebDriverWait Wait_loader_Elemenet = new WebDriverWait(driver,38); 
		 Wait_loader_Elemenet.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ui-loader[@busy.bind='isWaiting']")));
		 WebElement loaderClass = findElementByXPath(driver,"//ui-loader[@busy.bind='isWaiting']");
       String getloaderclass = loaderClass.getAttribute("class");
       System.out.println("class is "+getloaderclass);

// -------loader class when loader shows ---->>>-----au-target ui-loader aurelia-hide ------------------------
		 if(getloaderclass.equalsIgnoreCase("au-target ui-loader") || getloaderclass.equalsIgnoreCase("au-target ui-loader aurelia-hide") ) {
			 
			 
//			 System.out.println("Trying to click on portal ");Automated_ATC
//			 
//			 WebDriverWait waitforoperationlisting = new WebDriverWait(driver,20);
//				waitforoperationlisting.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='main_menus hidden-nav-stab hidden-nav-itab']/ui-menu[2]/a")));
//				
//				System.out.println("portal element found clicking it");
//				
//				findElementByXPath(driver,"//div[@class='main_menus hidden-nav-stab hidden-nav-itab']/ui-menu[2]/a").click();
//			 
//				System.out.println("portal element found clicked");
//			 
//			 
//			 Thread.sleep(5000);
//			 
//			 System.out.println("Sleeping to Reload");
//				
//			 driver.navigate().refresh();
//			 Thread.sleep(2000);
//			 driver.navigate().refresh();
//			 System.out.println("Reloaded");
			
			
			 
//		-------------Creating New Test Organisation-------------	 
			 
			 System.out.println(" clicking on ANSP icon");
			 Thread.sleep(2000);
//			 
			 WebDriverWait Wait_Organisation_Elemenet = new WebDriverWait(driver,38);     
			 Wait_Organisation_Elemenet.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),\"USERS \")]//parent::a")));
//					 visibilityOfElementLocated(By.xpath("//span[contains(text(),\"USERS \")]//parent::a")));  //a[@click.delegate='organizationLayout()'],//a[@click.delegate=\"goto('anspPage', true)\"]
			 System.out.println(" located");
			 
			 WebDriverWait Wait_for_User_click  = new WebDriverWait(driver,38); 
			 Wait_Organisation_Elemenet.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),\"USERS \")]//parent::a")));
			System.out.println(">>>>>> Sleeping For 4 sec <<<<");
			 Thread.sleep(4000);
            findElementByXPath(driver,"//span[contains(text(),'USERS ')]//parent::a").click();	 
			  System.out.println("clicked on ANSP icon");
	  
				 Thread.sleep(3000);
				  System.out.println("clicking  search  icon");
				  WebElement element = driver.findElement(By.xpath("//span[@class='search_bx']"));
					 
				 Actions action = new Actions(driver);
				  action.moveToElement(element).build().perform();			  
			        findElementByXPath(driver,"//span[@class='search_bx']/input").sendKeys("Automated_ANSP");
				 
				 
			        Thread.sleep(1000);
			        WebElement Get_ANSP = findElementByXPath(driver,"//tbody[@class='op_data']");
			        String ANSP = Get_ANSP.getText();
				 
//					-------------Creating New Test Organisation-------------			        
			        
			        
			        if (ANSP.contains("Automated_ANSP"))	{
			  
			  
			        	System.out.println(" ANSP alreday created logOut.");
			        	
			        	
			        	findElementByXPath(driver,"//i[@class='au-target fa fa-info-circle']/parent::a").click();
			        	
						 Ansp_Email =	findElementByXPath(driver,"//span[contains(text(), '@')]").getText();
							
							System.out.println(">>>>>> Email Address of ANSP is <<<<<<<<<<"+Ansp_Email);
			        	
		                  
		            	Thread.sleep(2000);
		            	WebDriverWait ProfileImage = new WebDriverWait(driver,60);
		            	ProfileImage.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@title='Profile']/img")));
		            	System.out.println("clicked on the profile");
		            	
		            	WebElement logelement = driver.findElement(By.xpath("//a[@title='Profile']/img"));		            	
		            	 logelement.click();
		            	 
		            	 findElementByXPath(driver,"//ul[@class='dropdown-menu']/li[2]/a").click();
			 
		    	
//		    	 findElementByXPath(driver,"//ui-button[@click.trigger='save()']").click();
		    	 
		    	 //div[@click.trigger='fireSelect(item.model)']
//    ------------ Implementation of Opening new Tab in Same Browser ---------- 		    
		    	 
		    	 
//		        for (int i = 0; i <= 1; i++) { 	
//		    	 
//		    	System.out.println("Trying to open new tab by robot class"); 
//		    	Robot rob = new Robot();
//		    	rob.keyPress(KeyEvent.VK_CONTROL);
//		    	rob.keyPress(KeyEvent.VK_T);
//		    	rob.keyRelease(KeyEvent.VK_CONTROL);
//		    	rob.keyRelease(KeyEvent.VK_T);  
//		    	
//		    	
//		    	ArrayList<String> tabs1 = new ArrayList<String> (driver.getWindowHandles());
//		    	driver.switchTo().window((String)tabs1.get(i));
//		    	
//		    	 }
//		   
//		          driver.get("http://www.yopmail.com/en/"); 
//		          
//		          findElementByXPath(driver,"//input[@name='login']").sendKeys("Automated_Ansp@yopmail.com");;
//		          findElementByXPath(driver,"//input[@type='submit']").click();
//		          
//		          Thread.sleep(1000);
//		          String totalcount = findElementByXPath(driver,"//span[@id='nbmail']").getAttribute("span");
//		          System.out.println("total_count of mails "+totalcount);
//		         
//		          
//		          ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
//		          System.out.println("No. of tabs: " + tabs.size());
//		          driver.switchTo().window(tabs.get(0));
		     } else {
		    	 
		    	 
		    	System.out.println(" Equals called  ");
		    	 
		    	
//		    	-------------Creating New Test Organisation-------------	 
				 				 
				 WebDriverWait Wait_Element = new WebDriverWait(driver,180);     
				 Wait_Element.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@click.delegate='getUser()']")));  //a[@click.delegate='organizationLayout()'],//a[@click.delegate=\"goto('anspPage', true)\"]
				 
	              findElementByXPath(driver,"//button[@click.delegate='getUser()']").click();	 
				  System.out.println("clicked on USER icon to Create ANSP");
				 
				 
//				  WebDriverWait Wait_Add_icon = new WebDriverWait(driver,180); 
//				  Wait_Add_icon.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn-add']")));
//				  
//				  System.out.println("Add Icon Found");
				  
//				  WebElement hover_icon = driver.findElement(By.xpath("//button[@class='btn-add']"));				 
//			        Actions action = new Actions(driver);
//			        action.moveToElement(hover_icon).perform();
////		            action.build().perform();
				  
//			        System.out.println(">>>>> Action Preformed<<<<");
				    
				  
				   
			        
			        
//			        ----------------- Selecting the Role of the User---------------.
				  
				   /* Thread.sleep(2000);
			         findElementByXPath(driver,"//button[@class='btn-add']").click();	
			         System.out.println(">>>>> Cliked  Preformed<<<<");			     
			         Thread.sleep(1000);
			        findElementByXPath(driver,"//a[@click.delegate=\"initAnspModal('ANSP')\"]").click();
				  
			         Thread.sleep(1000);   
				     WebElement  Roles_class = findElementByXPath(driver,"//ui-tags[@value.bind='selectedRoles | array:selectedRoles & validate']/div[2]");
				     System.out.println(Roles_class.getAttribute("class"));
				     String class_role = Roles_class.getAttribute("class");
				     System.out.println(class_role);*/
    
//			        String Required_role_class = "ui-list-container ui-floating au-target ui-open ui-tether-top ui-tether-left";  			     
//			        if (class_role!=Required_role_class) { 
	    	 
			    	 
//				  ----------------- End of Selecting the Role of the User---------------
				  
				  
				  
			    	 System.out.println(" NOt Equals called  ");
			    	 
//			    	 findElementByXPath(driver,"//ui-tags[@value.bind='selectedRoles | array:selectedRoles & validate']").click();
			    	 
			    	 WebDriverWait Wait_Add_icon = new WebDriverWait(driver,30); 
			    	 Wait_Add_icon.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='ui-switch-1']")));
			    	 
			    	 findElementByXPath(driver,"//label[@for='ui-switch-1']").click();
			    	 System.out.println("licked on the switch to active state");
			    	 
			    	 findElementByXPath(driver,"//input[@placeholder='Zipcode']").sendKeys("201301");
			    	 findElementByXPath(driver,"//input[@placeholder='State']").sendKeys("Alaska");
			    	 findElementByXPath(driver,"//input[@placeholder='City']").sendKeys("Norway");
			    	 findElementByXPath(driver,"//input[@placeholder='Address Line 1']").sendKeys("thum tower");
			    	 findElementByXPath(driver,"//input[@placeholder='Password']").sendKeys("Password1");
			    	 findElementByXPath(driver,"//input[@placeholder='Email']").sendKeys("auto_ansp@yopmail.com");
			    	 findElementByXPath(driver,"//input[@placeholder='Phone']").sendKeys("7503663518");
			    	 findElementByXPath(driver,"//input[@placeholder='Identifier']").sendKeys("ANSP_Identifier");
//			    	 findElementByXPath(driver,"//ui-input[@value.bind='user.LastName & validate']/div/input").sendKeys("Ansp");				        
//			    	 findElementByXPath(driver,"//ui-input[@value.bind='user.FirstName & validate']/div/input").sendKeys("Nik_");
			    	 findElementByXPath(driver,"//ui-input[@placeholder='${currentLanguage.ansp_page_addpage_fieldanspname}']/div/input").sendKeys("Automated_ANSP");
			    	 

			    	 
//			    	 WebElement chng_roleclass = findElementByXPath(driver,"//ui-tags[@value.bind='selectedRoles | array:selectedRoles & validate']/div[2]");		    	 
//			    	 String Required_role_class1 = "ui-list-container ui-floating au-target ui-open ui-tether-top ui-tether-left";  
//			    	 selectUser_role(driver,chng_roleclass,Required_role_class1);
//			    	 Thread.sleep(3000);
//			    	 
//			    	 WebDriverWait ANSP_span = new WebDriverWait(driver,20);     
//			    	 ANSP_span.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='ui-list-container ui-floating au-target ui-open ui-tether-top ui-tether-left']")));		    	
//			    	 findElementByXPath(driver,"//div[@class='ui-list-container ui-floating au-target ui-open ui-tether-top ui-tether-left']").click();
			    	
			    	 findElementByXPath(driver,"//ui-button[@click.trigger='save()']").click();
		    	 Thread.sleep(1000);
		    	 WebElement Toast_Message = findElementByXPath(driver,"//span[@class='ui-message']/p");
		    	String Toastmessage = Toast_Message.getText();
			    	 
			    	 if (Toastmessage.contains("User Created")) {
							
							
							System.out.println(" ANSP User Loggin OUT.");
							
							
							
							
							
						findElementByXPath(driver,"//i[@class='au-target fa fa-info-circle']/parent::a").click();
						 Ansp_Email =	findElementByXPath(driver,"//span[contains(text(), '@')]").getText();
							
							System.out.println(">>>>>> Email Address of ANSP is "+Ansp_Email);
			                  
			            	Thread.sleep(2000);
			            	WebElement logelement = driver.findElement(By.xpath("//a[@title='Profile']/img"));
			            	
			            	 logelement.click();
			            	 
			            	 findElementByXPath(driver,"//ul[@class='dropdown-menu']/li[2]/a").click();
//							-------------Creating New Test Organisation-------------
							  	} 
			    	 
			    	 
			    	 
			    	 
			    	 
		    	 
		    	 
//		     }
			        }
		     }
		     
		     
		     
//		        au-target ui-input-wrapper ui-input-list ui-tags ui-tether-bottom ui-tether-left ui-focus	        
//		        au-target ui-input-wrapper ui-input-list ui-tags ui-tether-bottom ui-tether-left

		
	 
       
       
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}   
	
}

	

	
	public static  WebElement findElementByXPath(WebDriver driver, String xpath) {
		return driver.findElement(By.xpath(xpath));
	}	
	
	
	public static void selectUser_role(WebDriver driver,WebElement element ,String Required_role_class1) {
	
	JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("arguments[0].setAttribute('class','"+Required_role_class1+"');", element);
	
	
}
	
}
