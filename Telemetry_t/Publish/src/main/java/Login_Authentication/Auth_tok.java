package Login_Authentication;
import com.google.gson.JsonObject;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

public class Auth_tok {

   
	public static void main(String[] args) {
        RestAssured.baseURI = "http://uss-dev.flyanra.net/services/security/authenticate";
        RequestSpecification httpRequest = RestAssured.given();
        httpRequest.header("Content-Type", "application/json");

        // Create new JSON Object
        JsonObject loginCredentials = new JsonObject();
        loginCredentials.addProperty("Email", "admin@flyanra.com");
        loginCredentials.addProperty("Password", "Password1");

        httpRequest.body(loginCredentials.toString());

        Response response = httpRequest.post();
        
        String authorizationHeader = response.getHeader("Authorization");

        Assert.assertNotNull(authorizationHeader);
    }
}