package Create_Drone;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Create_ANSP.Add_ANSP;
import Create_Organisation.Add_TestOrganisation;
import Generate_Token.Token;

public class Add_TestDrone {

	public static String Drone_RegistartionId;
	public static String Drone_UUID;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	
public static void Create_TestDrone(WebDriver driver) {
	
	try {
		
		 
		 System.out.println(">>>>>>> trying to login to ANSP user");
		 
		 WebDriverWait wait = new WebDriverWait (driver,180);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='Email']"))); 
		 
			findElementByXPath(driver,"//input[@placeholder='Email']").sendKeys(Add_TestOrganisation.Org_Email);
			
			findElementByXPath(driver,"//input[@type='password']").sendKeys("Password1");
			
			findElementByXPath(driver,"//a[@role='button']").click();
		    Thread.sleep(2000);
			Token.Admin_token();
		    System.out.println("ANSP OTP is>>>"+ Token.Admin_OTP_Token);
		    
		    findElementByXPath(driver,"//ui-input[@value.bind='VerificationCode']/div/input").sendKeys(Token.Admin_OTP_Token);
		    
		    findElementByXPath(driver,"//ui-button[@click.trigger='OTPverification()']").click();
		
		 WebElement loaderClass = findElementByXPath(driver,"//ui-loader[@busy.bind='isWaiting']");
	       String getloaderclass = loaderClass.getAttribute("class");
	       System.out.println("class is "+getloaderclass);
	       
	       System.out.println("chaal gaya Behenchood");
	       
	       if(getloaderclass.equalsIgnoreCase("au-target ui-loader") || getloaderclass.equalsIgnoreCase("au-target ui-loader aurelia-hide") ) {
				
//				-------------Creating New Test Organisation-------------	 			 
	    	   System.out.println(" clicking on ATC Icon");
				 Thread.sleep(2000);
				 
				 WebDriverWait Wait_Organisation_Elemenet = new WebDriverWait(driver,38);     
				 Wait_Organisation_Elemenet.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),\"DRONES \")]//parent::a")));  //a[@click.delegate='organizationLayout()'],//a[@click.delegate=\"goto('anspPage', true)\"]
				 System.out.println(" located User icon from header"); 
				 Thread.sleep(2000);
				 findElementByXPath(driver,"//span[contains(text(),\"DRONES \")]//parent::a").click();	 
				 System.out.println("clicked on User icon");
				 
				 Thread.sleep(3000);
				  System.out.println("clicking  search  icon");
				  WebElement element = driver.findElement(By.xpath("//span[@class='search_bx']")); 
				  
				  Actions action = new Actions(driver);
				  action.moveToElement(element).build().perform();			  
			      findElementByXPath(driver,"//span[@class='search_bx']/input").sendKeys("Auto_");
			      
			      Thread.sleep(1000);
			        WebElement Get_PilotDetail = findElementByXPath(driver,"//tbody[@class='op_data']");
			        String details = Get_PilotDetail.getText();
			        
			        System.out.println(">>>>>> serach result"+details);  
			        
			        if (details.contains("AUTO_DRONE1"))	{
						  
						  
			        	System.out.println(" ATC alreday created logOut.");
			        	Thread.sleep(1000);
			        	System.out.print("clicking on the logout button");
		                  
			        	findElementByXPath(driver,"//i[@class='au-target fa fa-eye act']").click();
			        	Thread.sleep(1000);
			        	findElementByXPath(driver,"//i[@class='au-target fa fa-eye']").click();
			        	
			        	Drone_UUID =	findElementByXPath(driver,"//span[contains(text(), 'UUID ')]/following-sibling::span").getText();
							
							System.out.println(">>>>>> Email Address of ANSP is <<<<<<<<<<"+Drone_UUID);
			        	
		                  
		            	Thread.sleep(2000);
		            	WebDriverWait ProfileImage = new WebDriverWait(driver,60);
		            	ProfileImage.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@title='Profile']/img")));
		            	System.out.println("clicked on the profile");
		            	
		            	WebElement logelement = driver.findElement(By.xpath("//a[@title='Profile']/img"));		            	
		            	 logelement.click();
		            	 
		            	 findElementByXPath(driver,"//ul[@class='dropdown-menu']/li[3]/a").click();
				  
				 
			 } else {
				 
			 
				 System.out.println("Drone to be created");
					
					WebDriverWait Wait_CreateOrgDrone_icon = new WebDriverWait(driver,80);
		 			Wait_CreateOrgDrone_icon.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@click.delegate='handleAddClick()']"))); 
		 			
		 			System.out.println("Add drone Icon Found trying to click");
		 			
		 			Thread.sleep(2000);
		 			WebElement AddDrone = findElementByXPath(driver,"//button[@click.delegate='handleAddClick()']");
		 			AddDrone.click();
		 			System.out.println("Clicked on add Drone icon");
					
		 			Thread.sleep(2000);
		 			findElementByXPath(driver,"//ui-input[@value.bind='drone.Name & validate']/div/input").sendKeys("Auto_Drone1");
		 			findElementByXPath(driver,"//a[@click.trigger='generateUUid()']").click();
		 			findElementByXPath(driver,"//ui-input[@value.bind='drone.BaseAlt & validate']/div/input").sendKeys("100");
		 			findElementByXPath(driver,"//ui-input[@value.bind='drone.BaseLatLng | geography & validate']/div/input").sendKeys("{\"type\":\"Point\",\"coordinates\":[-101.41410827636719,48.211862417203214]}");
		 			findElementByXPath(driver,"//ui-input[@number.bind='drone.CollisionThreshold']/div/input").sendKeys("1500");
				
		 			findElementByXPath(driver,"//ui-combo[@value.bind='drone.TypeId & validate']/div/input").click();
		 			Thread.sleep(2000);
		 			
		 			WebElement Drone_Type = 	findElementByXPath(driver,"//ui-combo[@value.bind='drone.TypeId & validate']/div[2]");
		 			System.out.println("trying to select the Copter type");
		 			Drone_Type.click();
		 			System.out.println("Clicked");
		 			
		 			
		 			findElementByXPath(driver,"//ui-combo[@value.bind='drone.BrandId & validate']/div/input").click();
		 			Thread.sleep(2000);
		 			WebElement Drone_Brand = 	findElementByXPath(driver,"//ui-combo[@value.bind='drone.BrandId & validate']/div[2]/div[6]");
		 			Drone_Brand.click();
		 			System.out.println("brand Selected");
		 			Thread.sleep(2000);
		 			findElementByXPath(driver,"//label[@class='ui-switch-inner au-target']").click();
		 			
		 			findElementByXPath(driver,"//ui-button[@click.trigger='save()']").click();
			 
			 
			 
		 			 Thread.sleep(1000);
			    	 WebElement Toast_Message = findElementByXPath(driver,"//span[@class='ui-message']/p");
			    	String Toastmessage = Toast_Message.getText();
			    	System.out.println(Toastmessage);
			    	
			    	if (Toastmessage.contains("Drone Created Successfully.")) {
						
						
						System.out.println(" ANSP User Loggin OUT.");
				    Thread.sleep(5000);
					findElementByXPath(driver,"//i[@class='au-target fa fa-eye act']").click();
					findElementByXPath(driver,"//i[@class='au-target fa fa-eye']").click();
					Drone_UUID =	findElementByXPath(driver,"//span[contains(text(), 'UUID ')]/following-sibling::span").getText();
						
						System.out.println(">>>>>> Email Address of ANSP is "+Drone_UUID);
		                  
		            	Thread.sleep(2000);
		            	WebElement logelement = driver.findElement(By.xpath("//a[@title='Profile']/img"));
		            	
		            	 logelement.click();
		            	 Thread.sleep(2000);
		            	 findElementByXPath(driver,"//ul[@class='dropdown-menu']/li[3]/a").click();
//						-------------Creating New Test Organisation-------------
						  	} 
			 
			 
			 
			 
			 }  
			 }
			        
			        
//	---------------------------END of Script -------------------------------		        
			        
//		
//		WebDriverWait Dronelayout = new WebDriverWait(driver,20);
//		Dronelayout.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@click.delegate='dronesLayout()']")));
//		findElementByXPath(driver,"//a[@click.delegate='dronesLayout()']").click();
//	
//	
//		Thread.sleep(3000);
//		  System.out.println("clicking  Drone  icon");
//		  WebElement element = driver.findElement(By.xpath("//span[@class='search_bx']"));
//			 
//	        Actions action = new Actions(driver);
//	 
//	        action.moveToElement(element).build().perform();
//		  
//	        findElementByXPath(driver,"//span[@class='search_bx']/input").sendKeys("Auto Drone 1");
//	
//	        Thread.sleep(1000);
//	        WebElement GetOrg_Drone = findElementByXPath(driver,"//tbody[@class='op_data']");
//	        String Org = GetOrg_Drone.getText().toLowerCase();
//	
////	  int dronecount =    GetOrg_Drone.size();
//
//				System.out.println(">>>>>>> Drone is "+Org);
//				
//		if (Org.contains("auto drone 1"))	{
//			
//			
//			System.out.println("Drone Already created");
//			
//			WebDriverWait UssDrone_listing = new WebDriverWait(driver,20);
//				UssDrone_listing.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//tbody[@class='op_data']")));
//				
//				
//    		WebElement GetDrone = findElementByXPath(driver,"//tbody[@class='op_data']/tr/td/span");
//		       String  GetDrone_Id = GetDrone.getText();
//		       System.out.println("Drone Name Character"+ GetDrone_Id);
//		       
//		       String Operation_DRONEId = GetDrone_Id.substring(13, 24);
//		       
//		      
//		       
//		       System.out.println("Drone Last Gufi Character"+ Operation_DRONEId);
//		       
//		       WebElement GetDrone_RegistrationID= findElementByXPath(driver,"//div[@class='op_details']/div[3]/span[2]");
//		         String USSDrone_RegistartionId = GetDrone_RegistrationID.getText();
//		         
//		         System.out.println("DRONE REGISTARTION ID"+ USSDrone_RegistartionId); 
//		       
//		         if(USSDrone_RegistartionId.contains(Operation_DRONEId)) {
//		        	 
//		        	 
//		        	 System.out.println("DRONE REgistration ID  "+ USSDrone_RegistartionId);
//		        	 
//		        	 Drone_RegistartionId = GetDrone_RegistrationID.getText();
//		        	 
//		        	 
//		        	 
//		        	 
//		         }
//			
//			
//		}	
//		else {
//			
//			
//			System.out.println("Drone to be created");
//			
//			WebDriverWait Wait_CreateOrgDrone_icon = new WebDriverWait(driver,80);
// 			Wait_CreateOrgDrone_icon.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@click.delegate='getDrone()']"))); 
// 			
// 			System.out.println("Add drone Icon Found trying to click");
// 			
// 			Thread.sleep(2000);
// 			WebElement AddDrone = findElementByXPath(driver,"//button[@click.delegate='getDrone()']");
// 			AddDrone.click();
// 			System.out.println("Clicked on add Drone icon");
//			
// 			Thread.sleep(2000);
// 			findElementByXPath(driver,"//input[@placeholder='Name...']").sendKeys("Auto Drone 1");
// 			findElementByXPath(driver,"//a[@click.trigger='generateUUid()']").click();
// 			findElementByXPath(driver,"//input[@placeholder='Base Altitude...']").sendKeys("100");
// 			findElementByXPath(driver,"//input[@placeholder='Please drop a marker on the map']").sendKeys("{\"type\":\"Point\",\"coordinates\":[-101.41410827636719,48.211862417203214]}");
// 			findElementByXPath(driver,"//input[@placeholder='Collision Threshold...']").sendKeys("1500");
//		
// 			findElementByXPath(driver,"//ui-combo[@value.bind='drone.TypeId & validate']/div/input").click();
// 			Thread.sleep(2000);
// 			
// 			WebElement Drone_Type = 	findElementByXPath(driver,"//ui-combo[@value.bind='drone.TypeId & validate']/div[2]");
// 			System.out.println("trying to select the Copter type");
// 			Drone_Type.click();
// 			System.out.println("Clicked");
// 			
// 			
// 			findElementByXPath(driver,"//ui-combo[@value.bind='drone.BrandId & validate']/div/input").click();
// 			Thread.sleep(2000);
// 			WebElement Drone_Brand = 	findElementByXPath(driver,"//ui-combo[@value.bind='drone.BrandId & validate']/div[2]/div[6]");
// 			Drone_Brand.click();
// 			System.out.println("brand Selected");
// 			Thread.sleep(2000);
// 			findElementByXPath(driver,"//label[@class='ui-switch-inner au-target']").click();
// 			
// 			findElementByXPath(driver,"//ui-button[@click.trigger='save()']").click();
// 			
// 			
// 			WebDriverWait Toast_Message = new WebDriverWait(driver,20);
//    		Toast_Message.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ui-toast[@class='ui-info au-target ui-toast ui-open']/div/span/p")));
//    		
//    		String Toast_msg1 = findElementByXPath(driver,"//ui-toast[@class='ui-info au-target ui-toast ui-open']/div/span/p").getText();
//	    	
//	    	System.out.println("Hence the Toast Message Is" + Toast_msg1);
// 			
// 			if (Toast_msg1.contains("Drone Created Successfully.")) {
// 				
// 				
// 				WebDriverWait UssDrone_listing = new WebDriverWait(driver,20);
// 				UssDrone_listing.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//tbody[@class='op_data']")));
// 				
// 				
//	    		WebElement GetDrone = findElementByXPath(driver,"//tbody[@class='op_data']/tr/td/span");
//			       String  GetDrone_Id = GetDrone.getText();
//			       
//			       System.out.println(" Drone Name"+ GetDrone_Id);
//			       
//			       
//                     String Operation_DRONEId = GetDrone_Id.substring(13, 25);
//			       
//			       System.out.println("Drone Last Gufi Character"+ Operation_DRONEId);
//			       
//			       WebElement GetDrone_RegistrationID= findElementByXPath(driver,"//div[@class='op_details']/div[3]/span[2]");
//			         String USSDrone_RegistartionId = GetDrone_RegistrationID.getText();
//			         
//			         System.out.println("DRONE REGISTARTION ID"+ USSDrone_RegistartionId);
//			         
//			         
//			         if(USSDrone_RegistartionId.contains(Operation_DRONEId)) {
//			        	 
//			        	 
//			        	 System.out.println("DRONE REgistration ID  "+ USSDrone_RegistartionId);
//			        	 
//			        	 Drone_RegistartionId = GetDrone_RegistrationID.getText();
//			        	 
//			        	 
//			        	 
//			        	 
//			         }
//			         
//			         
			         
			         
			         
			         
			         
			         
			     
			       
			      
		    		
 				
// 			}
 			
 			
 			
//		}
	
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	
	
	
	
	
	
}	
	


public static  WebElement findElementByXPath(WebDriver driver, String xpath) {
	return driver.findElement(By.xpath(xpath));
}


}

