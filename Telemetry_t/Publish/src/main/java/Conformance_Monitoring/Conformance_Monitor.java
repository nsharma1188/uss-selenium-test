package Conformance_Monitoring;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Create_operation.Add_operation;
import Create_telemetry.AddTelemetry_Json;

public class Conformance_Monitor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	
public static void conformanceNonconformance_check(WebDriver driver) {
	
	try {
		
		
		WebDriverWait conformanceActivation_alert = new WebDriverWait(driver,65);
		 conformanceActivation_alert.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='ui-wrapper']/span/p")));
		
		 WebElement Current_Status = findElementByXPath(driver,"//div[@class='ui-wrapper']/span/p");
			String ConformanceStatus = Current_Status.getText();
			System.out.println("Operation Activated>>>>>>>>>>" +ConformanceStatus);
			String MissionId = Add_operation.GetMissionId;
			
			System.out.println(" mission id<<<<<<<<? "+MissionId);
			
			if(ConformanceStatus.contains(MissionId)) {
				
				System.out.println("<<<<<<<<<<AGAYA ANDAR>>>>>>>>");
				
				AddTelemetry_Json.Telemetry(driver);
				
				Thread.sleep(1000);
				AddTelemetry_Json.NonConforming_telemetry(driver);
				
				   WebDriverWait nonconformance_alert = new WebDriverWait(driver,20);
				   nonconformance_alert.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='toast-message']")));
				
				   WebElement conformance_notification = findElementByXPath(driver,"//div[@class='toast-message']");
				   String Get_conformance_status = conformance_notification.getText();
				   System.out.println(">>>>>>>>>>"+Get_conformance_status);
				   
				   
				   if (Get_conformance_status.contains("Operation is NONCONFORMING ")) {
					   
					   System.out.println("Operation Status Changes To "+Get_conformance_status);
					   					   
				   }
				
				
			}
		
		
		   
		   
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
}


public static void conformanceActivation_check(WebDriver driver) {
	
	
	try {
		
		System.out.println("WAiting For Activation");
		Thread.sleep(3000);
		System.out.println(">>>>>>>>>>>>>> Sending Activation telemetry <<<<<<<<<<<<<<<< ");
		AddTelemetry_Json.Telemetry(driver);
		
		 WebDriverWait conformanceActivation_alert = new WebDriverWait(driver,30);
		 conformanceActivation_alert.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='ui-wrapper']/span/p")));
		 
		 WebElement Current_Status = findElementByXPath(driver,"//div[@class='ui-wrapper']/span/p");
		String ConformanceStatus = Current_Status.getText();
		
		System.out.println("Operation Activated>>>>>>>>>>" +ConformanceStatus);
		
		if (ConformanceStatus.contains("Operation Activated"+ Add_operation.GetMissionId)) {
			
			System.out.println("Operation Activated>>>>>>>>>>" +ConformanceStatus);
				
		}
			
		
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	
}
	
public static  WebElement findElementByXPath(WebDriver driver, String xpath) {
	return driver.findElement(By.xpath(xpath));
}	
	
}
