package Create_Organisation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import Create_ANSP.Add_ANSP;
import Generate_Token.Token;

public class Add_TestOrganisation {

	public static String Org_Email;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
     
	
		
		
		
		
	}
	
	public static void add_org(WebDriver driver) {

 System.out.println("locating the Element to click on portal");

//		  au-target ui-loader;
		  
		 try {
			 
			 
//			WebElement  loader = findElementByXPath(driver,"//ui-loader[@busy.bind='isWaiting']");
//			 System.out.println("Loader Class is"+loader.getClass().getName());
			 
			 
//			    WebDriverWait Loader= new WebDriverWait(driver,90);
//			 Loader.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ui-loader[@class='au-target ui-loader aurelia-hide']")));
		
			 
//			 Thread.sleep(2000);
//			 System.out.println("Sleep for few min");
//			 WebDriverWait Loader= new WebDriverWait(driver,120);
//			 Loader.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ui-loader[@busy.bind='isWaiting']")));

			 
			 //			 Loader.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ui-loader[@class='au-target ui-loader aurelia-hide']")));
			 
//			 Thread.sleep(28000);
//			 System.out.println("Sleep for few min for verifying loader class");
//			 WebElement loaderClass = findElementByXPath(driver,"//ui-loader[@busy.bind='isWaiting']");
//	         String getloaderclass = loaderClass.getAttribute("class");
//	         System.out.println("class is "+getloaderclass);
	         
	         
	         
	         WebDriverWait wait = new WebDriverWait (driver,180);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='Email']"))); 
			 
				findElementByXPath(driver,"//input[@placeholder='Email']").sendKeys(Add_ANSP.Ansp_Email);
				
				findElementByXPath(driver,"//input[@type='password']").sendKeys("Password1");
				
				findElementByXPath(driver,"//a[@role='button']").click();
				
			    Thread.sleep(5000);
				Token.Ansp_token();
			    System.out.println("ANSP OTP is>>>"+ Token.Ansp_OTP_Token);
//			    Thread.sleep(1000);
			    findElementByXPath(driver,"//ui-input[@value.bind='VerificationCode']/div/input").sendKeys(Token.Ansp_OTP_Token);			   
			    findElementByXPath(driver,"//ui-button[@click.trigger='OTPverification()']").click();
			
			 WebElement loaderClass = findElementByXPath(driver,"//ui-loader[@busy.bind='isWaiting']");
		       String getloaderclass = loaderClass.getAttribute("class");
		       System.out.println("class is "+getloaderclass);
	         
	         
	         
	         
	         
	         
	         
	         
	         
	         
	         
	         
	         
	         
			   
			 if(getloaderclass.equalsIgnoreCase("au-target ui-loader") || getloaderclass.equalsIgnoreCase("au-target ui-loader aurelia-hide")) {
				 
				 
				 System.out.println("Trying to click on portal ");
				 
				 WebDriverWait waitforoperationlisting = new WebDriverWait(driver,20);
					waitforoperationlisting.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),\"ORGANIZATION \")]/parent::a")));
					Thread.sleep(5000);
					System.out.println("Organization icon found clicking it");
					
					findElementByXPath(driver,"//span[contains(text(),\"ORGANIZATION \")]/parent::a").click();
				 
					System.out.println("Organization Icon found clicked");
				 
				 
//				 Thread.sleep(5000);
//				 
//				 System.out.println("Sleeping to Reload");
//					
//				 driver.navigate().refresh();
//				 Thread.sleep(2000);
//				 driver.navigate().refresh();
//				 System.out.println("Reloaded");
				
				 
				 
//			-------------Creating New Test Organisation-------------	 
				 
				 
//				 WebDriverWait Wait_Organisation_Elemenet = new WebDriverWait(driver,180);     
//				 Wait_Organisation_Elemenet.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@click.delegate=\"goto('organizationPage', false)\"]")));  //a[@click.delegate='organizationLayout()']
//				 
//	              findElementByXPath(driver,"//a[@click.delegate=\"goto('organizationPage', false)\"]").click();	 
//				  System.out.println("clicked on organisation icon");
				 
				  WebDriverWait Wait_CreateOrg_icon = new WebDriverWait(driver,80);
				  Wait_CreateOrg_icon.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@click.delegate='getOrganization()']"))); 
				  
				  System.out.println("found  organisation icon");
				  
				  Thread.sleep(3000);
				  System.out.println("clicking  organisation icon");
				  
				  
				  WebElement element = driver.findElement(By.xpath("//span[@class='search_bx']"));
					 
			        Actions action = new Actions(driver);
			 
			        action.moveToElement(element).build().perform();
				  
			        findElementByXPath(driver,"//span[@class='search_bx']/input").sendKeys("Auto");
					
					Thread.sleep(1000);
					WebElement GetOrganistaion = findElementByXPath(driver,"//tbody[@class='op_data']");
				    String Org = GetOrganistaion.getText().toLowerCase();
						
						System.out.println(Org);
						
					String Org_id  = "automated test organisation"+ 
							         "0dc414c1";
					
//					automated test organisation
//					0dc414c1
					
//					System.out.println("String1>><"+Org+">String2>><"+Org_id+">");					
//					System.out.println("Compare string>>>>>"+Org.matches(Org_id));
					
					if (Org.contains("automated test organisation")) {
						
						
						System.out.println(" Organisation alreday created logOut.");
						
						
						WebDriverWait Wait_detail_icon = new WebDriverWait(driver,80);
						Wait_detail_icon.until(ExpectedConditions.elementToBeClickable(By.xpath("//i[@class='au-target fa fa-info-circle act']"))); 
		                  
						findElementByXPath(driver,"//i[@class='au-target fa fa-info-circle act']").click();
			        	Thread.sleep(1000);
			        	findElementByXPath(driver,"//i[@class='au-target fa fa-info-circle']").click();
			        	
			        	Org_Email =	findElementByXPath(driver,"//span[contains(text(), '@')]").getText();
							
							System.out.println(">>>>>> Email Address of Organization under ANSP is <<<<<<<<<<"+ Org_Email);
			        	
		                  
		            	Thread.sleep(2000);
		            	WebDriverWait ProfileImage = new WebDriverWait(driver,60);
		            	ProfileImage.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@title='Profile']/img")));
		            	System.out.println("clicked on the profile");
		            	
		            	WebElement logelement = driver.findElement(By.xpath("//a[@title='Profile']/img"));		            	
		            	 logelement.click();
		            	 
		            	 findElementByXPath(driver,"//ul[@class='dropdown-menu']/li[3]/a").click();
//						-------------Creating New Test Organisation-------------
						  	}
					
					else
			         {
			        	System.out.println("Create Orgnisation");

            	
            	findElementByXPath(driver,"//button[@click.delegate='getOrganization()']").click();
				  System.out.println("clicked on organisation icon");
			 	 
				  Thread.sleep(1000);
				  findElementByXPath(driver,"//input[@placeholder='Organization Name']").sendKeys("Automated Test Organisation");;
				  findElementByXPath(driver,"//input[@type=\"email\"]").sendKeys("admin_auto1@yopmail.com");
				  findElementByXPath(driver,"//input[@type=\"number\"]").sendKeys("7503663518");
				  
				  findElementByXPath(driver,"//textarea[@placeholder='Address']").sendKeys("ithum tower 808 b");
				  
				  
				  
				  String location =   Organisation_Loc_Json.orglocobj();
				  
				  Thread.sleep(2000);
				  findElementByXPath(driver,"//input[@placeholder='Base Latitude and Longitute']").sendKeys(location);
				  
				  
				  findElementByXPath(driver,"//input[@placeholder='Website']").sendKeys("www.autoTest.com");
				 
				 findElementByXPath(driver,"//input[@placeholder='Government Licence']").sendKeys("Gov 1256");
				 
				 findElementByXPath(driver,"//input[@placeholder='Federal Tax ID/EIN...']").sendKeys("Fed 1256");
				 
				 
			WebElement Unit_Type = 	 findElementByXPath(driver,"//input[@select.trigger='$event.stopPropagation()']");
			Unit_Type.click();
			Thread.sleep(1000);
			findElementByXPath(driver,"//div[@mouseover.trigger='hilightItem($event)']").click();
			        
			System.out.println("cliked on option");
			
			findElementByXPath(driver,"//input[@placeholder='First Name']").sendKeys("Nikhil");
			
			findElementByXPath(driver,"//ui-combo[@value.bind='organization.UnitId']/div/input").click();
			Thread.sleep(2000);
		    WebElement UnitType = 	findElementByXPath(driver,"//ui-combo[@value.bind='organization.UnitId']/div/div");			
		    UnitType.click();
		
		
		    findElementByXPath(driver,"//ui-combo[@value.bind='organization.AdsbSource']/div/input").click();
		    Thread.sleep(2000);
		    WebElement ADSB_Source = 	findElementByXPath(driver,"//ui-combo[@value.bind='organization.AdsbSource']/div/div");
	        ADSB_Source.click();
			
		
		    findElementByXPath(driver,"//input[@placeholder='Last Name']").sendKeys("Sharma");
		    findElementByXPath(driver,"//input[@placeholder='Username']").sendKeys("admin_auto1@yopmail.com");
		    findElementByXPath(driver,"//input[@placeholder='Password...']").sendKeys("Password1");
		    findElementByXPath(driver,"//input[@placeholder='ReType Password...']").sendKeys("Password1");			
		    findElementByXPath(driver,"//ui-button[@click.trigger='save()']").click();
		    
		    
		    Thread.sleep(1000);
	    	 WebElement Toast_Message = findElementByXPath(driver,"//span[@class='ui-message']/p");
	    	String Toastmessage = Toast_Message.getText();
	    	System.out.println(Toastmessage);
		    
		    if (Toastmessage.contains("Organisation Created")) {
				
				
				System.out.println(" ANSP User Loggin OUT.");
				Thread.sleep(3000);
				WebDriverWait Wait_detail_icon = new WebDriverWait(driver,80);
				Wait_detail_icon.until(ExpectedConditions.elementToBeClickable(By.xpath("//i[@class='au-target fa fa-info-circle act']"))); 
                  	
		
			findElementByXPath(driver,"//i[@class='au-target fa fa-info-circle act']").click();
			Org_Email =	findElementByXPath(driver,"//span[contains(text(), '@')]").getText();
			
			System.out.println(">>>>>> Email Address of Organization under ANSP is <<<<<<<<<<"+ Org_Email);
                  
            	Thread.sleep(2000);
            	WebElement logelement = driver.findElement(By.xpath("//a[@title='Profile']/img"));
            	
            	 logelement.click();
            	 
            	 findElementByXPath(driver,"//ul[@class='dropdown-menu']/li[3]/a").click();
//				-------------Creating New Test Organisation-------------
				  	} 
		    
		    
		    
		    
		    
		    
		    

         }

			 }
		 
	         
	         
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
		
	}
	
	
	
	
	
	
	
	
	public static  WebElement findElementByXPath(WebDriver driver, String xpath) {
		return driver.findElement(By.xpath(xpath));
	}
	

}
