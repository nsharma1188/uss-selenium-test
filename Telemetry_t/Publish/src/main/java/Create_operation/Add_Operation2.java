package Create_operation;

import java.awt.Robot;
import java.awt.event.InputEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import Create_Organisation.Add_TestOrganisation;
import Generate_Token.Token;

public class Add_Operation2 {

	public static String GetMissionId2nd;
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public static void add_Mission(WebDriver driver) {
		
		

		try {
			
             System.out.println(">>>>>>> trying to login to OrganizationAdmin ");
			 
			 WebDriverWait wait = new WebDriverWait (driver,180);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='Email']"))); 
			 
				findElementByXPath(driver,"//input[@placeholder='Email']").sendKeys(Add_TestOrganisation.Org_Email);
				
				findElementByXPath(driver,"//input[@type='password']").sendKeys("Password1");
				
				findElementByXPath(driver,"//a[@role='button']").click();
			 
				Token.Admin_token();
			    System.out.println("ANSP OTP is>>>"+ Token.Admin_OTP_Token);
			    
			    findElementByXPath(driver,"//ui-input[@value.bind='VerificationCode']/div/input").sendKeys(Token.Admin_OTP_Token);
			    
			    findElementByXPath(driver,"//ui-button[@click.trigger='OTPverification()']").click();
			
			 WebElement loaderClass = findElementByXPath(driver,"//ui-loader[@busy.bind='isWaiting']");
		       String getloaderclass = loaderClass.getAttribute("class");
		       System.out.println("class is "+getloaderclass);
			
			

			
   		 if(getloaderclass.equalsIgnoreCase("au-target ui-loader") || getloaderclass.equalsIgnoreCase("au-target ui-loader aurelia-hide")) {
   			 
   			 System.out.println(" AGYA ANDAR BEHENCHOOD");
   			 
   			 
   			WebDriverWait Portal_Selector  = new WebDriverWait(driver,20);
   			Portal_Selector.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),\"PORTAL \")]//parent::a")));
			System.out.println(" USS Selector found and  is clickable");
   			 
			Thread.sleep(2000);
			findElementByXPath(driver,"//span[contains(text(),\"PORTAL \")]//parent::a").click();
			
//			Select associated_drone = new Select (findElementByXPath(driver,"//span[contains(text(),\"PORTAL \")]//parent::a"));
//			System.out.println(" AGYA ANDAR BEHENCHOOD 2");
////			System.out.println("USS NAME"+);
//			if(associated_drone == null)
//			{
//				System.out.println(" NULL HAI BEHENCHOOD ");
//			}
//			
//			try {
//				
//				if(Add_USS.USS_Name == null)
//				{
//					System.out.println(" Add_USS.USS_Name is null ");
//				}
//				
//				associated_drone.selectByVisibleText(Add_USS.USS_Name);
//			}
//			catch(Exception e)
//			{
//				e.printStackTrace();
//			}
            
            
//            associated_drone.selectByVisibleText("82724b0b | Automated Test USS");
			
			
			
			WebDriverWait Operation_PageLoads = new WebDriverWait(driver,60);
			Operation_PageLoads.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@value.bind='selectedUssInstance']")));
			System.out.println(">>>>>> Portal Page Loaded and Uss Select Drop Down Visible<<<<<<<<<");
           
			
			Select select = new Select(driver.findElement(By.xpath("//select[@value.bind='selectedUssInstance']")));
            WebElement option = select.getFirstSelectedOption();
            String defaultItem = option.getText();
            System.out.println(defaultItem );
//			
//            System.out.println("Wait For the Script" );
						
   		 }
			
//			-------------We Will not be Submit all the fields as We have Already Binded some mandatory field data for Operation creation-------
		
			WebDriverWait Opreartion_add  = new WebDriverWait(driver,20);
			Opreartion_add.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='tooltip add_opr']/button")));
			System.out.println(" found the Add Operation button and the Button is clickable");
			
			Thread.sleep(2000);
			findElementByXPath(driver,"//span[@class='tooltip add_opr']/button").click();
	
			
			
			findElementByXPath(driver,"//ui-combo[@value.bind='selectedPilot & validate']/div/input").click();
			
			System.out.println("Agaya ANdar");
			
			
			Thread.sleep(2000);
			findElementByXPath(driver,"//ui-combo[@value.bind='operation.uas_registrations | registration:operation.uas_registrations & validate']/div/input").click();
			Thread.sleep(1000);
			findElementByXPath(driver,"//ui-combo[@value.bind='operation.uas_registrations | registration:operation.uas_registrations & validate']/div[2]/div").click();
			Thread.sleep(1000);
			findElementByXPath(driver,"//ui-combo[@value.bind='selectedPilot & validate']/div/input").click();
			Thread.sleep(1000);
			findElementByXPath(driver,"//ui-combo[@value.bind='selectedPilot & validate']/div[2]/div").click();
			
// ---------------------Unwanted As UI has been Changed according to New Min. User inputs	----------------------------------------------------------		
			
//			findElementByXPath(driver,"//ui-textarea[@value.bind='operation.flight_comments & validate']/div/textarea").click();
//			System.out.println("Clicked on the Textarea");
//			Thread.sleep(2000);
//			findElementByXPath(driver,"//ui-combo[@value.bind='selectedPilot & validate']/div/input").click();
//			Thread.sleep(1000);
//			findElementByXPath(driver,"//ui-combo[@value.bind='selectedPilot & validate']/div[2]/div").click();
////			findElementByXPath(driver,"//ui-combo[@value.bind='selectedPilot & validate']/div/input").sendKeys("");
//			
//			findElementByXPath(driver,"//ui-textarea[@value.bind='operation.aircraft_comments & validate']/div/textarea").click();
//			System.out.println("Clicked on the Textarea");
			
			
			
			
			
			Thread.sleep(2000);
			findElementByXPath(driver,"//ui-input[@value.bind='operation.controller_location | geography & validate']/div/input").sendKeys("{\"type\":\"Point\",\"coordinates\":[-95.36158561706543,28.954221476517933]}");
//			Thread.sleep(1000);
//			findElementByXPath(driver,"//div[@class='op_title2']").click();
//			Thread.sleep(1000);
//			findElementByXPath(driver,"//div[@class='lf_op2']/div[3]").click();
			
			Thread.sleep(2000);
			findElementByXPath(driver,"//a[@click.trigger='setVolumeDrawType(1)']").click();
			
						
//------------------------------AGL-----------------------------
			
			Thread.sleep(1000);
			findElementByXPath(driver,"//ui-input[@value.bind='operationVolume.operation_geography | geography & validate']/div/input").sendKeys("{\r\n" + 
					"        \"type\": \"Polygon\",\r\n" + 
					"        \"coordinates\": [\r\n" + 
					"          [\r\n" + 
					"            [\r\n" + 
					"              -95.3619396686554,\r\n" + 
					"              28.953921066343433\r\n" + 
					"            ],\r\n" + 
					"            [\r\n" + 
					"              -95.361328125,\r\n" + 
					"              28.95390229067859\r\n" + 
					"            ],\r\n" + 
					"            [\r\n" + 
					"              -95.3612744808197,\r\n" + 
					"              28.953921066343433\r\n" + 
					"            ],\r\n" + 
					"            [\r\n" + 
					"              -95.36125302314758,\r\n" + 
					"              28.95459698801057\r\n" + 
					"            ],\r\n" + 
					"            [\r\n" + 
					"              -95.36187529563904,\r\n" + 
					"              28.95459698801057\r\n" + 
					"            ],\r\n" + 
					"            [\r\n" + 
					"              -95.3619396686554,\r\n" + 
					"              28.953921066343433\r\n" + 
					"            ]\r\n" + 
					"          ]\r\n" + 
					"        ]\r\n" + 
					"      }");
			
			
			
			System.out.print("Selecting the Operation Volumes");
			
			
			
			
			   findElementByXPath(driver,"//ui-input[@number.bind='operationVolume.agl_altitude & validate']/div/input").click();
			     findElementByXPath(driver,"//ui-input[@number.bind='operationVolume.agl_altitude & validate']/div/input").clear();
			     findElementByXPath(driver,"//ui-input[@number.bind='operationVolume.agl_altitude & validate']/div/input").sendKeys("500");
			     System.out.println("Max Altitude set ");
				
			
			     Thread.sleep(2000);
			     WebDriverWait Opreartion_AGL_input  = new WebDriverWait(driver,20);
			     Opreartion_AGL_input.until(ExpectedConditions.elementToBeClickable(By.xpath("//ui-input[@number.bind='operationVolume.agl_altitude & validate']/div/input")));
			     
			     findElementByXPath(driver,"//ui-input[@number.bind='timeDuration.duration & validate']/div/input").click();
			     findElementByXPath(driver,"//ui-input[@number.bind='timeDuration.duration & validate']/div/input").sendKeys("60");
			
			 Thread.sleep(2000);
		     findElementByXPath(driver,"//ui-date[@date.bind='operationVolume.effective_time_begin | utcDateTimeFormat & validate']/div/input").click();
		     Thread.sleep(2000);
		     findElementByXPath(driver,"//ui-date-view[@class='floating au-target ui-date-view ui-tether-top ui-tether-left']/div[2]/div/div[4]/a").click();
		     System.out.println("Clicked  on the Minute 1min");
			
             Thread.sleep(2000);
		     
		     findElementByXPath(driver,"//ui-date[@date.bind='operationVolume.effective_time_begin | utcDateTimeFormat & validate']/div/input").click();
		     Thread.sleep(2000);
		     findElementByXPath(driver,"//ui-date-view[@class='floating au-target ui-date-view ui-tether-top ui-tether-left']/div[2]/div/div[4]/a").click();
		     System.out.println("Clicked  on the Minute 1min");
//			
		     
//		     Thread.sleep(1000);
		     
			 
		    
		     
		  findElementByXPath(driver,"//i[@class='marker_icon']").click();
			
		  findElementByXPath(driver,"//ui-date[@date.bind='operationVolume.effective_time_end | utcDateTimeFormat & validate']/div/input").click();
		
		     
			
			
//			findElementByXPath(driver,"//ui-input[@number.bind='timeDuration.duration & validate']/div/input").click();
			
			
		    
		     
		     
//			     findElementByXPath(driver,"//ui-input[@number.bind='operationVolume.max_altitude.altitude_value & validate']/div/input").sendKeys("1000");
//			     Thread.sleep(2000);
//			     findElementByXPath(driver,"//ui-date[@date.bind='operationVolume.effective_time_begin | utcDateTimeFormat & validate']/div/input").click();
//			     Thread.sleep(2000);
//			     findElementByXPath(driver,"//ui-date-view[@class='floating au-target ui-date-view ui-tether-top ui-tether-left']/div[2]/div/div[4]/a").click();
//			     System.out.println("Clicked  on the Minute 1min");
//			     Thread.sleep(2000);
//			     findElementByXPath(driver,"//ui-date-view[@class='floating au-target ui-date-view ui-tether-top ui-tether-left']/div[2]/div/div[4]/a").click();
//			     System.out.println("Clicked twice on the Minute");
			     
//	------------------- Trying to set Effective End Time ---------------------------------------
			    
//			     Thread.sleep(2000);
//			     findElementByXPath(driver,"//ui-textarea[@value.bind='operation.volumes_description']/div/textarea").click();
//			     findElementByXPath(driver,"//ui-input[@number.bind='timeDuration.duration & validate']/div/input").sendKeys("60");
//			     Thread.sleep(2000);
			     
			     
			    
			     
			     
//			     findElementByXPath(driver,"//ui-date-view[@class='floating au-target ui-date-view ui-tether-top ui-tether-left']/div[2]/div/div[2]/a").click();
//		     WebDriverWait endtimecalander  = new WebDriverWait(driver,20);
//		     Opreartion_AGL_input.until(ExpectedConditions.elementToBeClickable(By.xpath("//ui-date[@class='disbledDateTime au-target ui-input-wrapper ui-input-date ui-readonly ui-valid ui-tether-bottom ui-tether-left']/div/input/span")));

		     
		     //		     Thread.sleep(1000);
//			findElementByXPath(driver,"//a[@click.trigger='setVolumeDrawType(1)']").click();
			
			
			
			
			
			
//			Thread.sleep(1000);
		    
		     findElementByXPath(driver,"//ui-button[@click.trigger='save()']").click();
		     Thread.sleep(1000);
			 findElementByXPath(driver,"//ui-button[@click.trigger='saveOperation()']").click();
			     
			     System.out.println("Operation Created");
			     
			     WebDriverWait visibleWait_Toat_Msg = new WebDriverWait(driver,20);
				    visibleWait_Toat_Msg.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ui-toast[@class='ui-info au-target ui-toast ui-open']/div/span/p")));
				    
				    
		            String Toast_msg1 = findElementByXPath(driver,"//ui-toast[@class='ui-info au-target ui-toast ui-open']/div/span/p").getText();
			    	
			    	System.out.println("Hence the Toast Message Is" + Toast_msg1);
			    	
			    	Thread.sleep(5000);
			    	WebDriverWait Operation_Activation_Popup = new WebDriverWait(driver,62);
		    		Operation_Activation_Popup.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ui-toast[@class='ui-info au-target ui-toast ui-open']/div/span/p")));
		    		String Toast_msg2 = findElementByXPath(driver,"//ui-toast[@class='ui-info au-target ui-toast ui-open']/div/span/p").getText();
		    		

	    			System.out.println("Clicking on 2D View");
	    			findElementByXPath(driver,"//span[contains(text(),'2D View')]//parent::a").click();
	    			
	    			System.out.println("Clicked  On 2D View");
		    		
			    	if(Toast_msg1.contains("Operation created.")) {
			    		
			    		
			    		//ui-toast[@class='ui-info au-target ui-toast ui-open']/div/span/p
			    		
			    		WebDriverWait Operation_Activation_PopUp = new WebDriverWait(driver,62);
			    		Operation_Activation_PopUp.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ui-toast[@class='ui-info au-target ui-toast ui-open']/div/span/p")));
			    		
			    		WebElement OperationActivation = findElementByXPath(driver,"//ui-toast[@class='ui-info au-target ui-toast ui-open']/div/span/p");
			    		    String Activation_Message=    OperationActivation.getText();
			    		    System.out.println("So the Message Is "+ Activation_Message);
			    		    
			    		    if (Activation_Message.contains("Operation Activated")) {
			    		    
			    		    String Operation_MissionId = Activation_Message.substring(20, 56);
			    		    
			    		    System.out.println("Mission Activated For Gufi "+ Activation_Message);
			    		    System.out.println("Mission ID For Operation "+ Operation_MissionId);
			    		    
			    		   
//			    		WebDriverWait Refresh_icon_click = new WebDriverWait(driver,20);
//			    		Refresh_icon_click.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='refresh_opbx tooltip']")));
//			    		
//			    		findElementByXPath(driver,"//span[@class='refresh_opbx tooltip']").click();
			    		
			    		
			    		
			    		WebDriverWait Operation_listing = new WebDriverWait(driver,20);
			    		Operation_listing.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//tbody[@class='op_data']")));
			    		
			    		//tbody[@class='op_data']/tr/td/span
			    		
			    		
			    		WebElement GetOperation = findElementByXPath(driver,"//tbody[@class='op_data']/tr/td/span");
				       String  GetOperation_gufi = GetOperation.getText();
				       String OperationId = GetOperation_gufi.substring(0, 8);
				       
				       System.out.println("Operation Last Gufi Character"+ OperationId);
			    		
			    		
			    		WebElement GetOperation_Id = findElementByXPath(driver,"//div[@class='op_details']/div/span[2]");
			    		String GetOperationId = GetOperation_Id.getText();
			    		
			    		System.out.println("OPeration ID is "+ GetOperationId);
			    		
			    		
			    		if (Operation_MissionId.equalsIgnoreCase(GetOperationId)){
			    			
//			    			Thread.sleep(2000);
//			    			System.out.println("CLICKING ON Operation Volume VIEW");
//			    			WebElement forclicking_opVolme = findElementByXPath(driver,"//ui-drawer-toggle[@id='draw']");
//			    			JavascriptExecutor js = ((JavascriptExecutor) driver);
//			    			js.executeScript("arguments[0].setAttribute('style','display: block;');",forclicking_opVolme);
//			    			
//			    			System.out.println("CLICKING VIEW on volume");
//			    			
//			    			WebDriverWait UssExchange_panel = new WebDriverWait(driver,20);
//			    			UssExchange_panel.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ui-drawer-toggle[@id='drawpanel']")));
//			    			
//			    			
//			    			WebElement Change_style_ussExchange= findElementByXPath(driver,"//ui-drawer-toggle[@id='drawpanel']");
//			    			JavascriptExecutor j_sc = ((JavascriptExecutor) driver);
//			    			j_sc.executeScript("arguments[0].setAttribute('style','display: none;');",Change_style_ussExchange);
//			    				
//			    			System.out.println("Trying to CLICK  on volume");
//			    			Thread.sleep(2000);
//			    			forclicking_opVolme.click();
//			    			System.out.println("CLICKED VIEW on volume");
			    			
//			---------------------------------Style change Java Script and Click On the Drawer end -------------    			

//			---------------------------------    			
//			    			Thread.sleep(3000);
//			    			System.out.println("Executing Click with JavaScript Executor volume");
//			    			WebElement mapObject = driver.findElement(By.xpath("//*[name()='svg']/*[name()='g']/*[name()='path' and not(@fill='#37e4c6') and not(@fill='rgb(255, 0, 0)')]"));
//			    			((JavascriptExecutor) driver).executeScript("arguments[0].click();", mapObject);
			    			
			    			
//			    			WebElement mapObject2= driver.findElement(By.xpath("//*[name()='svg']/*[name()='g']/*[name()='path' and not(@fill='#37e4c6') and not(@fill='rgb(255, 0, 0)')]"));
//			    			mapObject2.click();
			    			
			    			//*[name()='svg']/*[name()='g' and not(@fill='#37e4c6') and not(@fill='rgb(255, 0, 0)')]
			    			
			    			
//			    			Authentication_Token.operation_details();
			    			
//			    			System.out.println("CLICKING ON 2D VIEW");
//			    			findElementByXPath(driver,"//a[@click.trigger='show2dView()']").click();
			    			
			    			
			    			
			    			
			    			
	//------------------------------------------ Trying to click 2d volume to open the Operation Detail  side Bar ------------------------------------------------------------------------------		    			
		
//		---------------------------------------Tried Editing On the mouse hover -----------------------------------
			    			
			    			
			    			
//	                        Thread.sleep(1000);			
//			    			findElementByXPath(driver,"//div[@class='leaflet-overlay-pane']").click();
//			    			System.out.println(">>> Clicked on the operation volume");
			    			
//			    			 Thread.sleep(3000);
//			    			 System.out.println("sleeping for 3 sec");
//			    			findElementByXPath(driver,"//tbody/tr/td/i[3]/ul/li/a").click();
			    			
			    			//a[@click.trigger='onEdit(item)']
			    			
//			    			System.out.println("clicked on the label icon");
//			    			Actions action = new Actions(driver);
//			    			WebElement element = driver.findElement(By.xpath("//tbody/tr/td/i[3]/ul/li/a"));
////			    			action.moveToElement(element).perform();
//			    			 System.out.println("Hover on edit");
//			    			 action.moveToElement(element).click().build().perform();
//			    			 System.out.println("Clicked on edit");	    			
//			    			 element.click();

	//-------------------------------------------Mouse Hover is End - ----------------------------
			    			
			    			
			    			
			    			
			    			 
			    			 String XPATH = "//*[name()='svg']/*[name()='g']/*[name()='path' and not(@fill='#37e4c6') and not(@fill='rgb(255, 0, 0)')]";		    			
			    			WebElement svgObj = driver.findElement(By.xpath(XPATH));
			    			Actions actionBuilder = new Actions(driver);
			    			actionBuilder.click(svgObj).build().perform();		    			
			    			System.out.println("Clicked on the Svg icon");
			    			
			    			
			    		      WebElement Image = driver.findElement(By.xpath("//*[name()='svg']/*[name()='g']/*[name()='path' and not(@fill='#37e4c6') and not(@fill='rgb(255, 0, 0)')]"));
			    		      String Mission_color =  Image.getAttribute("stroke");
			    		       
			    		      System.out.println(">>>>>>>>>> Colour Code is :"+ Mission_color);
			    		       
			    		    
			    		       //Used points class to get x and y coordinates of element.
			    		        Point classname = Image.getLocation();
			    		        int xcordi = classname.getX();
			    		        System.out.println("Element's Position from left side"+xcordi +" pixels.");
			    		        int ycordi = classname.getY();
			    		        System.out.println("Element's Position from top"+ycordi +" pixels.");
			    			
			    			
			    			 Robot r = new Robot();
			    			 r.mouseMove(xcordi,ycordi);
			    			 r.mousePress( InputEvent.BUTTON1_MASK );
//			    			 r.mouseRelease(InputEvent.BUTTON1_MASK);
			    			 r.mousePress( InputEvent.BUTTON1_MASK );
			    			 System.out.println("Clciked on the Svg Icon");
			    			 
			    			 
//			    		        Actions action2 = new Actions(driver);
//			    		        action2.moveToElement(Image).moveByOffset(xcordi, ycordi).click().build().perform();      		        
//			    		        System.out.println("Clciked on the Point");
			    		        
			    			
			    			
//			    			findElementByXPath(driver,"//*[name()='svg']/*[name()='g']/*[name()='path' and not(@fill='#37e4c6') and not(@fill='rgb(255, 0, 0)')]").click();		    			
//			    			WebDriverWait ui_Loader = new WebDriverWait(driver,20);
//			    			ui_Loader.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ui-loader[@class='au-target ui-loader aurelia-hide']")));
			    			
			    		
			    		if (GetOperationId.contains(OperationId)) {
			    			
			    			
			    			System.out.println("Operation Gufi is "+ GetOperationId);
			    			
			    			 GetMissionId2nd = GetOperation_Id.getText();
			    			 
			    			 System.out.println("Sleeping For 5min and Operation Gufi Is"+ GetMissionId2nd );		 
//			    			 Thread.sleep(10000);
			    			
			    			
			    		}}}
			    		else {
			    			
			    			System.out.println("Operation Gufi Did not matched / Or no Operation Gufi");
			    			
			    			
			    		}
			    		
			     
			     
			     
			    
//-------------------------------Contingency Form  ---------------------------------------	  
			     

//		    	findElementByXPath(driver,"//div[@class='lf_op2']/div[3]/div/span").click();
//		    	System.out.println("clicked on the Operation Label.");
//		    	Thread.sleep(2000);	    		
//		    	findElementByXPath(driver,"//div[@class='lf_op2']/div[4]/div/span").click();
//		    	System.out.println("Clicked on the contingency part ");
//		    	
//		    	findElementByXPath(driver,"//button[@click.delegate='addContingency()']").click();
//		    	findElementByXPath(driver,"//ui-tags[@value.bind='contingency_cause | contigency & validate']").click();
		    	
//	-------------------------- Particular section is written for Selecting Cause Of Contingency - -----------------	  
		    	
		    	
		    	
		    	
//		    	findElementByXPath(driver,"//ui-tags[@value.bind='contingency_cause | contigency & validate']/div/input").sendKeys("s");
//		    	Thread.sleep(2000);
//		    	findElementByXPath(driver,"//div[@class='ui-list-container ui-floating au-target ui-open ui-tether-bottom ui-tether-left']/div[4]").click();
		    	
//	--------------------------- Passing the Location Area (GeoJson) For Contingency Plan	 
		    	
		    	
		    	
//		    	findElementByXPath(driver,"//ui-input[@value.bind='contingency_polygon | geography & validate']/div/input").sendKeys("{\"type\":\"Polygon\",\"coordinates\":[[[-95.3609311580658,28.95345636763816],[-95.3609311580658,28.954090047173803],[-95.36031961441039,28.954090047173803],[-95.36031961441039,28.95345636763816],[-95.3609311580658,28.95345636763816]]]}");

		    	
		    	
		    	
//	--------------------------------Clicking On the ----------------------------------	    
		    	
		    	
//		        findElementByXPath(driver,"//ui-combo[@value.bind='contingency_location_description & validate']/div/input").click();
//		        Thread.sleep(1000);
//		         findElementByXPath(driver,"//ui-combo[@value.bind='contingency_location_description & validate']/div[2]/div").click();
//			    Thread.sleep(1000);
//			    findElementByXPath(driver,"//ui-combo[@value.bind='contingency_response & validate']/div/input").click();
//			    Thread.sleep(1000);
//			    findElementByXPath(driver,"//ui-combo[@value.bind='contingency_response & validate']/div[2]/div").click();
//			    findElementByXPath(driver,"//ui-button[@click.trigger='saveContigency()']").click();
//			    
//			    
//			    WebDriverWait visibleWait_for_contingency = new WebDriverWait(driver,20);
//			    visibleWait_for_contingency.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='lf_op2']/div[4]/div")));
//			    
//			    
//			    findElementByXPath(driver,"//div[@class='lf_op2']/div[4]/div").click();
//			    System.out.println("Clicked on Contingency Label");
			    
//	------------------------- Now Filling the Meta Data Form - ----------------------	
			    
			    
			    
//			    System.out.println("Clicking On Meta Data Tab to fill Meta Data form");
//			    findElementByXPath(driver,"//div[@class='lf_op2']/div[5]/div/span").click();
//			    System.out.println("Clicked On Meta Data Tab to fill Meta Data form");
//			    
//			    findElementByXPath(driver,"//ui-input[@value.bind='operation.metadata.call_sign & validate']/div/input").sendKeys("TestCall Sign");;
//			    findElementByXPath(driver,"//ui-input[@value.bind='operation.metadata.test_run']/div/input").sendKeys("Test Keys");
//			    
//			    findElementByXPath(driver,"//ui-button[@click.trigger='saveOperation()']").click();
//			    System.out.println("Operation Created");
//			    
			    
			   
		    		
		    	}
		    	
		    	
		    
			    
			  //ui-toast[@class='ui-info au-target ui-toast ui-open']/div/span/p
			     
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	public static  WebElement findElementByXPath(WebDriver driver, String xpath) {
		return driver.findElement(By.xpath(xpath));
	}
	

}
