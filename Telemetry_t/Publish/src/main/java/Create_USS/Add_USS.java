package Create_USS;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Create_ANSP.Add_ANSP;
import Create_ATC.Add_ATC;
import Generate_Token.Token;

public class Add_USS {

	public static String USS_Name=new String();
	public static String USS_InstanceId=new String();
	public static String uss_domain_name;
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	
	public static void Add_Uss_geography(WebDriver driver) {
		
		try {
			
			
			
              System.out.println(">>>>>>> trying to login to ANSP user");
			 
			 WebDriverWait wait = new WebDriverWait (driver,180);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='Email']"))); 
			 
				findElementByXPath(driver,"//input[@placeholder='Email']").sendKeys(Add_ATC.Atc_Email);
				
				findElementByXPath(driver,"//input[@type='password']").sendKeys("Password1");
				
				findElementByXPath(driver,"//a[@role='button']").click();
			 
				Token.ATC_token();
			    System.out.println("ANSP OTP is>>>"+ Token.Atc_OTP_Token);
			    
			    findElementByXPath(driver,"//ui-input[@value.bind='VerificationCode']/div/input").sendKeys(Token.Atc_OTP_Token);
			    
			    findElementByXPath(driver,"//ui-button[@click.trigger='OTPverification()']").click();
			
			 WebElement loaderClass = findElementByXPath(driver,"//ui-loader[@busy.bind='isWaiting']");
		       String getloaderclass = loaderClass.getAttribute("class");
		       System.out.println("class is "+getloaderclass);
			
			
		       if(getloaderclass.equalsIgnoreCase("au-target ui-loader") || getloaderclass.equalsIgnoreCase("au-target ui-loader aurelia-hide") ) {
					
//					-------------Creating New Test Organisation-------------	 			 
				 
				 System.out.println(" clicking on ATC Icon");
//				 Thread.sleep(2000);
//				 
//				 WebDriverWait Wait_Organisation_Elemenet = new WebDriverWait(driver,38);     
//				 Wait_Organisation_Elemenet.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),\"USERS \")]//parent::a")));  //a[@click.delegate='organizationLayout()'],//a[@click.delegate=\"goto('anspPage', true)\"]
//				 System.out.println(" located User icon from header"); 
//				 Thread.sleep(2000);
//				 findElementByXPath(driver,"//span[contains(text(),\"USERS \")]//parent::a").click();	 
//				 System.out.println("clicked on User icon");
				 
				 Thread.sleep(3000);
				  System.out.println("clicking  search  icon");
				  WebElement element = driver.findElement(By.xpath("//span[@class='search_bx']")); 
				  
				  Actions action = new Actions(driver);
				  action.moveToElement(element).build().perform();			  
			      findElementByXPath(driver,"//span[@class='search_bx']/input").sendKeys("Automated Test USS");
			      
			      Thread.sleep(1000);
			        WebElement Get_PilotDetail = findElementByXPath(driver,"//tbody[@class='op_data']");
			        String details = Get_PilotDetail.getText();
			        
			        System.out.println(">>>>>> serach result"+details);  
			   
			        if (details.contains("Automated Test USS"))	{
						  
						  
			        	System.out.println(" ATC alreday created logOut.");
			        	Thread.sleep(1000);
			        	System.out.print("clicking on the logout button");
		                  
			        	findElementByXPath(driver,"//i[@class='au-target fa fa-eye act']").click();
			        	Thread.sleep(1000);
			        	findElementByXPath(driver,"//i[@class='au-target fa fa-eye']").click();
			        	
			        	USS_Name =	findElementByXPath(driver,"//span[contains(text(), 'USS Name ')]").getText();
			        	uss_domain_name = findElementByXPath(driver,"//span[contains(text(), 'USS Name ')]/following-sibling::span").getText();
			        	System.out.println(">>>>>> Uss_Name <<<<<<<<<<"+USS_Name);
			        	System.out.println(">>>>>> Uss_domain_Name <<<<<<<<<<"+uss_domain_name);
			        	
			        	USS_InstanceId = findElementByXPath(driver,"//span[contains(text(), 'Instance ID ')]/following-sibling::span").getText();
							
							System.out.println(">>>>>> USS_InstanceId <<<<<<<<<<"+USS_InstanceId);
		                  
		            	Thread.sleep(2000);
		            	WebDriverWait ProfileImage = new WebDriverWait(driver,60);
		            	ProfileImage.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@title='Profile']/img")));
		            	System.out.println("clicked on the profile");
		            	
		            	WebElement logelement = driver.findElement(By.xpath("//a[@title='Profile']/img"));		            	
		            	 logelement.click();
		            	 
		            	 findElementByXPath(driver,"//ul[@class='dropdown-menu']/li[2]/a").click();
				  
				 
			 }else {
				 
			 
				 System.out.println("Need to Create USS Geography.");
		    		
		    		findElementByXPath(driver,"//button[@click.delegate='handleAddClick()']").click();
					System.out.println("clicked on USS icon");
		    		
		    		
		    		
		    		
//-------------------------- Implementation for clicking date // able to click date-------------------------------------------------		    		
		    		
		    		Thread.sleep(1000);
		    		findElementByXPath(driver,"//ui-date[@date.bind='utm.time_available_end & validate']/div/input").click();
		    		Thread.sleep(1000);
		    		findElementByXPath(driver,"//ui-date-view[@class='floating au-target ui-date-view ui-tether-top ui-tether-left']/div/div/a[3]").click();
		    		System.out.println("clicked on next month! Now waiting for Date");

		    		Thread.sleep(1000);
		    		findElementByXPath(driver,"//ui-date[@date.bind='utm.time_available_end & validate']/ui-date-view/div[2]/div[1]/div[2]/a").click();
//		    		findElementByXPath(driver,"//div[@class='ui-dv-container']/div[6]/span[5]").click();
		    		
		    		System.out.println("Clicked on Next month Date");
		    		
		    		System.out.println("Date Has Been Set To Next Month");
		 
		    		
		    		
		    		
		    		
		    		
//		    		findElementByXPath(driver,"//ui-input[@value.bind='utm.uss_informational_url & validate']/div/input").sendKeys("https://uss-dev.flyanra.net/services/uss/uss");
//		    		findElementByXPath(driver,"//ui-input[@value.bind='utm.uss_openapi_url & validate']/div/input").sendKeys("https://uss-dev.flyanra.net/services/uss/uss");
//                 findElementByXPath(driver,"//ui-input[@value.bind='utm.uss_registration_url & validate']/div/input").sendKeys("https://uss-dev.flyanra.net/services/uss/uss");

		    		
		    		findElementByXPath(driver,"//ui-input[@value.bind='utm.coverage_area | geography & validate']/div/input").sendKeys("{\r\n" + 
		    				"        \"type\": \"Polygon\",\r\n" + 
		    				"        \"coordinates\": [\r\n" + 
		    				"          [\r\n" + 
		    				"            [\r\n" + 
		    				"              -96.48193359375,\r\n" + 
		    				"              28.275358281817105\r\n" + 
		    				"            ],\r\n" + 
		    				"            [\r\n" + 
		    				"              -94.625244140625,\r\n" + 
		    				"              28.275358281817105\r\n" + 
		    				"            ],\r\n" + 
		    				"            [\r\n" + 
		    				"              -94.625244140625,\r\n" + 
		    				"              29.401319510041485\r\n" + 
		    				"            ],\r\n" + 
		    				"            [\r\n" + 
		    				"              -96.48193359375,\r\n" + 
		    				"              29.401319510041485\r\n" + 
		    				"            ],\r\n" + 
		    				"            [\r\n" + 
		    				"              -96.48193359375,\r\n" + 
		    				"              28.275358281817105\r\n" + 
		    				"            ]\r\n" + 
		    				"          ]\r\n" + 
		    				"        ]\r\n" + 
		    				"      }");
			    		
		    		Thread.sleep(1000);
		    		findElementByXPath(driver,"//input[@placeholder='USS Name']").sendKeys("Automated Test USS");
		    		Thread.sleep(2000);
		    		findElementByXPath(driver,"//span[contains(text(),'USS Name')]").click();
		    		System.out.println(">>>>>>clicked On Label");
   		
		    		findElementByXPath(driver,"//ui-button[@click.trigger='save()']").click();
			 
			 
		    		System.out.println("Clicked on save Volume Button");
		    		
		    		

//		 			 Thread.sleep(1000);
		    		WebDriverWait Toast_Notification =new WebDriverWait (driver,20);
		    		Toast_Notification.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='ui-message']/p")));
			    	 WebElement Toast_Message = findElementByXPath(driver,"//span[@class='ui-message']/p");
			    	String Toastmessage = Toast_Message.getText();
			    	System.out.println(Toastmessage);
			 
                      if (Toastmessage.contains("Uss Instance created.")) {
						
						
						System.out.println(" ANSP User Loggin OUT.");
				
						WebDriverWait Eyebutton = new WebDriverWait(driver,40);
						Eyebutton.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//i[@class='au-target fa fa-eye act']")));
					findElementByXPath(driver,"//i[@class='au-target fa fa-eye act']").click();
					Thread.sleep(1000);
					findElementByXPath(driver,"//i[@class='au-target fa fa-eye']").click();
					USS_Name =	findElementByXPath(driver,"//span[contains(text(), 'USS Name ')]").getText();
					uss_domain_name = findElementByXPath(driver,"//span[contains(text(), 'USS Name ')]/following-sibling::span").getText();
		        	System.out.println(">>>>>> Uss_Name <<<<<<<<<<"+USS_Name);
		        	System.out.println(">>>>>> Uss_domain_Name <<<<<<<<<<"+uss_domain_name);
					
		        	USS_InstanceId = findElementByXPath(driver,"//span[contains(text(), 'Instance ID ')]/following-sibling::span").getText();
						
						System.out.println(">>>>>> USS_InstanceId <<<<<<<<<<"+USS_InstanceId);
		                  
		            	Thread.sleep(2000);
		            	WebElement logelement = driver.findElement(By.xpath("//a[@title='Profile']/img"));
		            	
		            	 logelement.click();
		            	 Thread.sleep(2000);
		            	 findElementByXPath(driver,"//ul[@class='dropdown-menu']/li[2]/a").click();
//						-------------Creating New Test Organisation-------------
						  	} 
			 
			 
			 }
			 }
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
//			WebDriverWait Dronelayout = new WebDriverWait(driver,20);
//			Dronelayout.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@click.delegate='usslayout()']")));
//			findElementByXPath(driver,"//a[@click.delegate='usslayout()']").click();
//			
//			Thread.sleep(3000);
//			  System.out.println("Clicking on Serach Icon");
//			  WebElement element = driver.findElement(By.xpath("//span[@class='search_bx']"));
//				 
//		        Actions action = new Actions(driver);
//		 
//		        action.moveToElement(element).build().perform();
//		        
//		        findElementByXPath(driver,"//span[@class='search_bx']/input").sendKeys("Automated Test USS");
//		        
//		        WebElement GetUSS_Area = findElementByXPath(driver,"//tbody[@class='op_data']");
//		    	String USS = GetUSS_Area.getText().toLowerCase();
//			
//		    	System.out.println(">>>>>>> USS  is "+USS);
//		    
//		    	if (USS.contains("automated test uss")) {
//		    		
//		    		
//		    		System.out.println("Uss already Created");
//		    		
//		    		WebDriverWait Uss_listing = new WebDriverWait(driver,20);
//		    		Uss_listing.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//tbody[@class='op_data']")));
//		    		  
//		    		WebElement GetUSS_Area_Name = findElementByXPath(driver,"//tbody[@class='op_data']");
//				         USS_Name = GetUSS_Area_Name.getText();
//				       
//				       System.out.println("USS NAME IS>>>> "+ USS_Name);
//				       
//				       WebElement GetUSS_InstanceId = findElementByXPath(driver,"//div[@class='op_details']/div[3]/span[2]");
//				          USS_InstanceId = GetUSS_InstanceId.getText();
//				       
//				       System.out.println("USS Instance Id is >>>> "+ USS_InstanceId);
//		    		
//				   
//		    		
//		    	}
//		    	else {
//		    		
//		    		System.out.println("Need to Create USS Geography.");
//		    		
//		    		findElementByXPath(driver,"//button[@click.delegate='addUss()']").click();
//					System.out.println("clicked on USS icon");
//		    		
//		    		Thread.sleep(1000);
//		    		findElementByXPath(driver,"//input[@placeholder='USS Name']").sendKeys("Automated Test USS");
//		    		findElementByXPath(driver,"//ui-input[@value.bind='utm.uss_informational_url & validate']/div/input").sendKeys("https://uss-dev.flyanra.net/services/uss/uss");
//		    		findElementByXPath(driver,"//ui-input[@value.bind='utm.uss_openapi_url & validate']/div/input").sendKeys("https://uss-dev.flyanra.net/services/uss/uss");
//                    findElementByXPath(driver,"//ui-input[@value.bind='utm.uss_registration_url & validate']/div/input").sendKeys("https://uss-dev.flyanra.net/services/uss/uss");
//
//		    		
//		    		findElementByXPath(driver,"//ui-input[@value.bind='utm.coverage_area | geography & validate']/div/input").sendKeys("{\r\n" + 
//		    				"        \"type\": \"Polygon\",\r\n" + 
//		    				"        \"coordinates\": [\r\n" + 
//		    				"          [\r\n" + 
//		    				"            [\r\n" + 
//		    				"              -96.48193359375,\r\n" + 
//		    				"              28.275358281817105\r\n" + 
//		    				"            ],\r\n" + 
//		    				"            [\r\n" + 
//		    				"              -94.625244140625,\r\n" + 
//		    				"              28.275358281817105\r\n" + 
//		    				"            ],\r\n" + 
//		    				"            [\r\n" + 
//		    				"              -94.625244140625,\r\n" + 
//		    				"              29.401319510041485\r\n" + 
//		    				"            ],\r\n" + 
//		    				"            [\r\n" + 
//		    				"              -96.48193359375,\r\n" + 
//		    				"              29.401319510041485\r\n" + 
//		    				"            ],\r\n" + 
//		    				"            [\r\n" + 
//		    				"              -96.48193359375,\r\n" + 
//		    				"              28.275358281817105\r\n" + 
//		    				"            ]\r\n" + 
//		    				"          ]\r\n" + 
//		    				"        ]\r\n" + 
//		    				"      }");
//			    		
//		    		
////-------------------------- Implementation for clicking date // able to click date-------------------------------------------------		    		
//		    		
//		    		Thread.sleep(1000);
//		    		findElementByXPath(driver,"//ui-date[@date.bind='utm.time_available_end & validate']/div/input").click();
//		    		Thread.sleep(1000);
//		    		findElementByXPath(driver,"//ui-date-view[@class='floating au-target ui-date-view ui-tether-top ui-tether-left']/div/div/a[3]").click();
//		    		System.out.println("clicked on next month! Now waiting for Date");
//
//		    		Thread.sleep(1000);
//		    		findElementByXPath(driver,"//ui-date[@date.bind='utm.time_available_end & validate']/ui-date-view/div[2]/div[1]/div[2]/a").click();
////		    		findElementByXPath(driver,"//div[@class='ui-dv-container']/div[6]/span[5]").click();
//		    		
//		    		System.out.println("Clicked on Next month Date");
//		    		
//		    		System.out.println("Date Has Been Set To Next Month");
//		    		
//		    		findElementByXPath(driver,"//ui-button[@click.trigger='save()']").click();
//		    		
//		    		System.out.println("Clicked on save Volume Button");
//		    		
//		    		 WebElement loaderClass = findElementByXPath(driver,"//ui-loader[@busy.bind='isWaiting']");
//		    		 String getloaderclass = loaderClass.getAttribute("class");
//		    		 System.out.println("class is "+getloaderclass);
//		    		 
//		    		 
//		    		 
//		    		 if(getloaderclass.equalsIgnoreCase("au-target ui-loader")) {
//		    		
//		    		WebDriverWait Toast_Message = new WebDriverWait(driver,20);
//		    		Toast_Message.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ui-toast[@class='ui-info au-target ui-toast ui-open']/div/span/p")));
//
//		    		
////		    ---------  The Below Xpath for Ui Toast is for Warning when We cannot make Two Uss With Similar Boundary that is why it is Commented ----------	
//		    		
////		    	String Toast_msg = findElementByXPath(driver,"//ui-toast[@class='ui-danger au-target ui-toast ui-open']/div/span/p").getTagName();
////		    	System.out.println("Hence the Toast Message Is" + Toast_msg);
//		    	
//		    	String Toast_msg1 = findElementByXPath(driver,"//ui-toast[@class='ui-info au-target ui-toast ui-open']/div/span/p").getText();
//		    	
//		    	System.out.println("Hence the Toast Message Is" + Toast_msg1);
//		    	
////		    	Toast_msg1.contains("Uss instance already exists in grid(237/424/10)") Another Condition to be implemented
//		    	
////-------------------------------Contingency Form  ---------------------------------------	
//		    	if ( Toast_msg1.contains("Uss Instance created.")) {
//		    		
//		    		//ui-info au-target ui-toast ui-open
//		    		
//		  
//		    		   Thread.sleep(3000);
//					   System.out.println("Clicking on Serach Icon");
//					   WebElement Search_element = driver.findElement(By.xpath("//span[@class='search_bx']"));
//						 
//				       Actions Todo_action = new Actions(driver);
//				 
//				       Todo_action.moveToElement(Search_element).build().perform();
//				        
//				       findElementByXPath(driver,"//span[@class='search_bx']/input").sendKeys("Automated Test USS");
//		    		
//		    		   System.out.println(" Uss Created Succesfully USE Case Passed ");
//		    		   
//		    		   
//		    		WebDriverWait Uvrlayout = new WebDriverWait(driver,20);
//		   			Uvrlayout.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@click.delegate='constraintLayout()']")));
//		    		findElementByXPath(driver,"//a[@click.delegate='constraintLayout()']").click();
//		    		  
//		    		 Thread.sleep(2000); 
//		    		 
//		    		 
//		    		 WebDriverWait Usslayout = new WebDriverWait(driver,20);
//		    		 Usslayout.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@click.delegate='usslayout()']")));
//		 			findElementByXPath(driver,"//a[@click.delegate='usslayout()']").click();
//		    		 
//		    		WebDriverWait Uss_listing = new WebDriverWait(driver,20);
//		    		Uss_listing.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//tbody[@class='op_data']")));
//		    		
//
//		    		  
//		    		WebElement GetUSS_Area_Name = findElementByXPath(driver,"//tbody[@class='op_data']");
//				         USS_Name = GetUSS_Area_Name.getText();
//				       
//				       System.out.println("USS NAME IS>>>> "+ USS_Name);
//				       
//				       WebElement GetUSS_InstanceId = findElementByXPath(driver,"//div[@class='op_details']/div[3]/span[2]");
//				          USS_InstanceId = GetUSS_InstanceId.getText();
//				       
//				       System.out.println("USS Instance Id is >>>> "+ USS_InstanceId);
//				     
//		    		
//		    	}	
//		    		
//		    		
//		    		 }
//		    		
		    		
  		
		    		
		    		
		    		
//	------------------------------------------------------------------------------------------	    		
		    
//	----------------   	    		----------
		    		
//		    		LocalDate futureDate = LocalDate.now().plusMonths(1);
//		    		
//		    		System.out.println("New date Is"+futureDate);
		    	

		    		
//---------------------- Date Time Set by JsExecutor Commenting Working Snippet------------------		    		
		    		
		    		
//----------------------- Selecting Uss Expiration Date Time ------------------------		    		
		    		
//		    		DateFormat dateFormat2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
//		    		Date date = new Date(); 
		    		
//------------------------- Comenting the Code For changing the Current Calender time To UTC Js Executor Public Function   --------		    		
		    		

		    		
//		    		Calendar calndr = Calendar.getInstance();
//		    		
//		    		calndr.add(Calendar.MONTH,1);
//		    		SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000'Z'");
//		    		dateformat.setTimeZone(TimeZone.getTimeZone("UTC"));	
//		    		
//		    		System.out.println(" changed date of the mission >>>>>>" + calndr.getTime());
//		    		System.out.println(" changed date of the mission >>>>>><<<<<" + dateformat.format(calndr.getTime()));
		    		
//		    		Thread.sleep(1000);		
//		            WebElement enddate = findElementByXPath(driver,"//ui-date[@date.bind='utm.time_available_end & validate']");				    		
//		    		String dateVal = dateformat.format(calndr.getTime());		    		
//		    		selectUssEnddate(driver,enddate,dateVal);	
		    		
		    		
		    		
		    		
//----------------------------------------------------------------------------------------------------------------------		    	
    
//		    	}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static  WebElement findElementByXPath(WebDriver driver,String xpath) {
		return driver.findElement(By.xpath(xpath));
	}	
	
//	public static void selectUssEnddate(WebDriver driver,WebElement element ,String dateVal) {
//		
//		JavascriptExecutor js = ((JavascriptExecutor) driver);
//			js.executeScript("arguments[0].setAttribute('date.bind','"+dateVal+"');", element);
//		
//		
//	}
	
	
	
	
}
