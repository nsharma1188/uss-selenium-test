package Create_USS;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Create_ATC.Add_ATC;
import Generate_Token.Token;

public class login_Uss_Atc {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
   
		
		
		
	}

	public static void login_atc(WebDriver driver) {
		
		if(driver!=null){
			System.out.println("Driver is not Null");
			}else {
				System.out.println("Driver is Null");
			}
			
			driver.get("https://uss-staging.flyanra.net");
			driver.manage().window().maximize();
			
			WebDriverWait wait = new WebDriverWait (driver,180);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='Email']")));
			
			
			findElementByXPath(driver,"//input[@placeholder='Email']").sendKeys(Add_ATC.Atc_Email);
			
			findElementByXPath(driver,"//input[@type='password']").sendKeys("Password1");
			
			findElementByXPath(driver,"//a[@role='button']").click();
			
			Token.ATC_token();
			
			 System.out.println("ANSP OTP is>>>"+ Token.Atc_OTP_Token);
			
			 findElementByXPath(driver,"//ui-input[@value.bind='VerificationCode']/div/input").sendKeys(Token.Atc_OTP_Token);
			    
			    findElementByXPath(driver,"//ui-button[@click.trigger='OTPverification()']").click();
			
			String CurrentUrl = driver.getCurrentUrl();
			
			
			
			if(CurrentUrl.equals("https://uss-staging.flyanra.net")) {
				
				
				System.out.println("logged in Successfully ");
				
			}
		
		
	}
	
	public static  WebElement findElementByXPath(WebDriver driver, String xpath) {
		return driver.findElement(By.xpath(xpath));
	}
}
