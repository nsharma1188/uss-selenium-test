package Create_ATC;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Create_ANSP.Add_ANSP;
import Generate_Token.Token;

public class Add_ATC {

	public static String Atc_Email;
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	
	public static void Create_ATC_User(WebDriver driver) {
	
	
	
		 try {
			
			 System.out.println(">>>>>>> trying to login to ANSP user");
			 
			 WebDriverWait wait = new WebDriverWait (driver,180);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='Email']"))); 
			 
				findElementByXPath(driver,"//input[@placeholder='Email']").sendKeys(Add_ANSP.Ansp_Email);
				
				findElementByXPath(driver,"//input[@type='password']").sendKeys("Password1");
				
				findElementByXPath(driver,"//a[@role='button']").click();
			 
				Token.Ansp_token();
			    System.out.println("ANSP OTP is>>>"+ Token.Ansp_OTP_Token);
			    
			    findElementByXPath(driver,"//ui-input[@value.bind='VerificationCode']/div/input").sendKeys(Token.Ansp_OTP_Token);
			    
			    findElementByXPath(driver,"//ui-button[@click.trigger='OTPverification()']").click();
			
			 WebElement loaderClass = findElementByXPath(driver,"//ui-loader[@busy.bind='isWaiting']");
		       String getloaderclass = loaderClass.getAttribute("class");
		       System.out.println("class is "+getloaderclass);

		// -------loader class when loader shows ---->>>-----au-target ui-loader aurelia-hide ------------------------
				 if(getloaderclass.equalsIgnoreCase("au-target ui-loader") || getloaderclass.equalsIgnoreCase("au-target ui-loader aurelia-hide") ) {
			
//						-------------Creating New Test Organisation-------------	 			 
					 
					 System.out.println(" clicking on ATC Icon");
					 Thread.sleep(2000);
					 
					 WebDriverWait Wait_Organisation_Elemenet = new WebDriverWait(driver,38);     
					 Wait_Organisation_Elemenet.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),\"USERS \")]//parent::a")));  //a[@click.delegate='organizationLayout()'],//a[@click.delegate=\"goto('anspPage', true)\"]
					 System.out.println(" located"); 
					 Thread.sleep(2000);
					 findElementByXPath(driver,"//span[contains(text(),\"USERS \")]//parent::a").click();	 
					 System.out.println("clicked on ATC icon");
					 
					 Thread.sleep(3000);
					  System.out.println("clicking  search  icon");
					  WebElement element = driver.findElement(By.xpath("//span[@class='search_bx']")); 
					  
					  Actions action = new Actions(driver);
					  action.moveToElement(element).build().perform();			  
				      findElementByXPath(driver,"//span[@class='search_bx']/input").sendKeys("Nik_");
				      
				      Thread.sleep(1000);
				        WebElement Get_ATC = findElementByXPath(driver,"//tbody[@class='op_data']");
				        String ATC = Get_ATC.getText();
				        
				        System.out.println(">>>>>> serach result"+ATC);
//	      -------------Creating New Test Organisation-------------			        
				        
				        
                 if (ATC.contains("Nik_ ATC"))	{
				  
				  
				        	System.out.println(" ATC alreday created logOut.");
				        	Thread.sleep(1000);
				        	System.out.print("clicking on the logout button");
			                  
				        	findElementByXPath(driver,"//i[@class='au-target fa fa-info-circle actve']/parent::a").click();
				        	Thread.sleep(1000);
				        	findElementByXPath(driver,"//i[@class='au-target fa fa-info-circle']/parent::a").click();
				        	
				        	Atc_Email =	findElementByXPath(driver,"//span[contains(text(), '@')]").getText();
								
								System.out.println(">>>>>> Email Address of ANSP is <<<<<<<<<<"+Atc_Email);
				        	
			                  
			            	Thread.sleep(2000);
			            	WebDriverWait ProfileImage = new WebDriverWait(driver,60);
			            	ProfileImage.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@title='Profile']/img")));
			            	System.out.println("clicked on the profile");
			            	
			            	WebElement logelement = driver.findElement(By.xpath("//a[@title='Profile']/img"));		            	
			            	 logelement.click();
			            	 
			            	 findElementByXPath(driver,"//ul[@class='dropdown-menu']/li[3]/a").click();
					  
					 
				 }else {

					 System.out.println(" Create New ATC  ");
				 
//					 -------------Creating New Test Organisation-------------
				 
					 WebDriverWait Wait_Element = new WebDriverWait(driver,180);     
					 Wait_Element.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@click.delegate='getUser()']")));
				 
					 findElementByXPath(driver,"//button[@click.delegate='getUser()']").click();	 
					  System.out.println("clicked on USER icon to Create ATC");
				 
				 
					  
					  WebDriverWait Wait_Add_icon = new WebDriverWait(driver,30); 
				    	 Wait_Add_icon.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[@for='ui-switch-1']")));
				    	 
				    	 findElementByXPath(driver,"//label[@for='ui-switch-1']").click();
				    	 System.out.println("Clicked on the switch to active state");
				    	 
				    	 findElementByXPath(driver,"//input[@placeholder='Zipcode']").sendKeys("201301");
				    	 findElementByXPath(driver,"//input[@placeholder='State']").sendKeys("Alaska");
				    	 findElementByXPath(driver,"//input[@placeholder='City']").sendKeys("Norway");
				    	 findElementByXPath(driver,"//input[@placeholder='Address Line 1']").sendKeys("thum tower");
				    	 findElementByXPath(driver,"//input[@placeholder='Password']").sendKeys("Password1");
				    	 findElementByXPath(driver,"//input[@placeholder='Email']").sendKeys("auto_atc1@yopmail.com");
				    	 findElementByXPath(driver,"//input[@placeholder='Phone']").sendKeys("7503663518");
//				    	 findElementByXPath(driver,"//input[@placeholder='Identifier']").sendKeys("Automated_ATC");
				    	 findElementByXPath(driver,"//ui-input[@value.bind='user.FirstName & validate']/div/input").sendKeys("Nik_");
				    	 findElementByXPath(driver,"//ui-input[@value.bind='user.LastName & validate']/div/input").sendKeys("ATC");
				    	 
//				    	 findElementByXPath(driver,"//ui-input[@placeholder='${currentLanguage.ansp_page_addpage_fieldanspname}']/div/input").sendKeys("Automated_ATC");
				        
				    	 
				        
				    	 
				 
				    
				         findElementByXPath(driver,"//ui-button[@click.trigger='save()']").click();
				    	 
				    	 Thread.sleep(1000);
				    	 WebElement Toast_Message = findElementByXPath(driver,"//span[@class='ui-message']/p");
				    	String Toastmessage = Toast_Message.getText();
				    	
				    	
				    	
				    	 if (Toastmessage.contains("User Created")) {
								
								
								System.out.println(" ANSP User Loggin OUT.");
						
							findElementByXPath(driver,"//i[@class='au-target fa fa-info-circle actve']/parent::a").click();
							findElementByXPath(driver,"//i[@class='au-target fa fa-info-circle']/parent::a").click();
							 Atc_Email =	findElementByXPath(driver,"//span[contains(text(), '@')]").getText();
								
								System.out.println(">>>>>> Email Address of ANSP is "+Atc_Email);
				                  
				            	Thread.sleep(2000);
				            	WebElement logelement = driver.findElement(By.xpath("//a[@title='Profile']/img"));
				            	
				            	 logelement.click();
				            	 
				            	 findElementByXPath(driver,"//ul[@class='dropdown-menu']/li[3]/a").click();
//								-------------Creating New Test Organisation-------------
								  	} 
				    	 
				    	
				         
				 }
	      
				 
				 
				 
				 }
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	
	
	
	}
	
	
	public static  WebElement findElementByXPath(WebDriver driver, String xpath) {
		return driver.findElement(By.xpath(xpath));
	}	
	
	
	public static void selectUser_role(WebDriver driver,WebElement element ,String Required_role_class1) {
	
	JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("arguments[0].setAttribute('class','"+Required_role_class1+"');", element);
	
	
}	
	
	
	
}
