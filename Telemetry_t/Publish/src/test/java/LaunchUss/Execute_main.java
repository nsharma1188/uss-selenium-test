package LaunchUss;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

import Conformance_Monitoring.Conformance_Monitor;
import Create_ANSP.Add_ANSP;
import Create_ATC.Add_ATC;
import Create_Drone.Add_TestDrone;
import Create_Organisation.Add_TestOrganisation;
import Create_USS.Add_USS;
import Create_USS.login_Uss_Atc;
import Create_Users.Add_Test_USER;
//import Create_Users.Add_TestUser;
import Create_operation.Add_operation;
import Create_telemetry.AddTelemetry_Json;

import launch_Uss.login_Uss_dashboard;
public class Execute_main {

	static WebDriver driver;
	
 public static void CreateDriver() {
		
	 ChromeOptions options = new ChromeOptions();
		options.addArguments("incognito");
		System.setProperty("webdriver.chrome.driver", "C:\\selenium driver\\chromedriver.exe");
		driver = new ChromeDriver(options);
	
		
	}


@Test
public static void LaunchUssFunction () {
	
	CreateDriver();
    login_Uss_dashboard.loginFunctionality(driver);
	
	
}
	

@Test(priority=2,dependsOnMethods= {"LaunchUssFunction"})
public static void Create_ANSP() {
	
	Add_ANSP.Create_ANSP_User(driver);
	
}



@Test(priority=3,dependsOnMethods= {"Create_ANSP"})
public static void Create_ATC() {
	
	Add_ATC.Create_ATC_User(driver);
	
}




@Test (priority=4,dependsOnMethods= {"Create_ATC"})
public static void Create_TestOrg () {
	Add_TestOrganisation.add_org(driver);
	
	
}



@Test (priority=5,dependsOnMethods= {"Create_TestOrg"})
public static void Create_User () {
	
	Add_Test_USER.add_pilot(driver);	
}



@Test (priority=6,dependsOnMethods= {"Create_User"})
public static void Create_Drone() {
	
	Add_TestDrone.Create_TestDrone(driver);
	
}


@Test (priority=7,dependsOnMethods= {"Create_Drone"})
public static void Create_Uss() {
	
	Add_USS.Add_Uss_geography(driver);
	
}


@Test (priority=8,dependsOnMethods= {"Create_Uss"})
public static void Create_Operation() {
	
	Add_operation.add_Mission(driver);
	
}


@Test (priority=9,dependsOnMethods= {"Create_Operation"})
public static void Send_Telemetry() throws InterruptedException {
	 
	for (int i = 0; i<150; i++) {
		
		Thread.sleep(1000);
	AddTelemetry_Json.Telemetry(driver);
	}
}


@Test (priority=10,dependsOnMethods= {"Send_Telemetry"})
public static void conformance_Alert() {
	 
  
	Conformance_Monitor.conformanceNonconformance_check(driver);
	
}


@Test (priority=8,dependsOnMethods= {"conformance_Alert"})
public static void conformanceActivation_Alert() {
	 
  
	Conformance_Monitor.conformanceActivation_check(driver);;
	
}




// ------------ Open Atc in Other Chrome Browser for working parallely---------------------
//@Test (priority=5,dependsOnMethods= {"Create_TestOrg"})
//public static void Create_browser () {
//	CreateDriver();
//	login_Uss_Atc.login_atc(driver);
//	
//}

//-----------------------------------------



//
//@Test (priority=3,dependsOnMethods= {"Create_TestOrg"})
//public static void Create_User() {
//	
//	Add_TestUser.Create_UserPilot(driver);	
//	
//}
//

//
//

//

//
//
//@Test (priority=7,dependsOnMethods= {"Create_Operation"})
//public static void conformance_Alert() {
//	 
//    
//	Conformance_Monitor.conformanceNonconformance_check(driver);
//	
//}
//
//
//@Test (priority=8,dependsOnMethods= {"conformance_Alert"})
//public static void conformanceActivation_Alert() {
//	 
//    
//	Conformance_Monitor.conformanceActivation_check(driver);;
//	
//}

//@Test (priority=7,dependsOnMethods= {"Create_Operation"})
//public static void Send_Telemetry() {
//	 
//    
//	AddTelemetry_Json.Telemetry(driver);
//	
//}
//
//@Test (priority=8,dependsOnMethods= {"Send_Telemetry"})
//public static void Conformance_nonConforming() {
//	 
//    
//	AddTelemetry_Json.NonConforming_telemetry(driver);
//	
//}

	
}
